﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GPGSOLASPNET
{
    public partial class Select_comp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!User.Identity.IsAuthenticated) // if the user is already logged in
                {
                    Response.Redirect(FormsAuthentication.LoginUrl);
                }
                Session.LCID = 2057;

                if (!Page.IsPostBack)
                {
                 
                    String strquery = "EXEC ASP_SPMODULE  @NMODE='CompFill'";
                    modMain.FillInCombo(strquery, ddlCompany, "COBR_NAME", "COBR_ID", SessionManager.g_strConnString);
                    ddlCompany.Items.Insert(0, "Select");
                    
                }
            }
            catch (Exception ex)
            {
                modMain.ShowMessage(ex.Message,this);
            }
        }

        protected void Continue(object sender, EventArgs e)
        {
            if (ddlCompany.Text.ToUpper() == "SELECT")
            {
                modMain.ShowMessage("Select Company", this);
                ddlCompany.Focus();
            }
            else
            {
                if (ddlCompany.Text.Length != 0)
                {
                    SessionManager.g_strCompanyName = ddlCompany.SelectedItem.Text;
                    SessionManager.g_strcobr_id = ddlCompany.SelectedValue;
                    String strcompid = ddlCompany.SelectedItem.Text;
                    if (strcompid != "")
                    {
                        strcompid = strcompid.Substring(strcompid.Length - 2, 2);
                    }
                    SessionManager.g_strCo_id = strcompid;
                    Response.Redirect("Contact.aspx");
                }
            }
        }

        
    }
}