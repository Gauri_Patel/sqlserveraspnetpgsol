﻿using Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GPGSOLASPNET
{
    public partial class forgot_password : System.Web.UI.Page
    {
        string strFCYR_FULLYR;
        protected void Page_Load(object sender, EventArgs e)
        {

            strFCYR_FULLYR = Request.QueryString["FCYR_FULLYR"];
            if (!Page.IsPostBack)
            {
                //  Page.Session.Add("db_name", "m2c");
                SessionManager.g_strConnString = "Data Source = " + SessionManager.g_strServerName + ";Initial Catalog = " + SessionManager.g_strDatabase + strFCYR_FULLYR + ";Integrated Security=SSPI";
                SessionManager.g_strConnStringACC = "Data Source = " + SessionManager.g_strServerName + ";Initial Catalog = " + SessionManager.g_strDatabaseAcc + strFCYR_FULLYR + ";Integrated Security=SSPI";

            }

        }

        protected void Continue(object sender, EventArgs e)
        {
            if (IsValid)
            {
                
                run_proc();
            }

        }

        public void run_proc()
        {
            DataTable worktable = new DataTable();

            worktable = modMain.NewDatatable("SELECT * FROM DBO.USERS WHERE USER_NAME ='" + username.Text + "' ", SessionManager.g_strConnStringACC, null);


            if (worktable.Rows.Count != 0)
            {
                string email_id = Convert.ToString(worktable.Rows[0]["EMAIL_ID"]);
                string f_password =Convert.ToString( worktable.Rows[0]["USER_PWD"]);
                if (email_id != "")
                {
                    sendemail(email_id, f_password);
                }
                else
                {
                   modMain.ShowMessage("Email ID is not Correct.Contact your system administrator for Password.",this);
                }
            }
            
            else
            {
            }
            worktable.Dispose();
        }

        public void sendemail(string email_id, string pwd)
        {
            var host = System.Configuration.ConfigurationManager.AppSettings["smtp_server"];
            string from_mail_id = System.Configuration.ConfigurationManager.AppSettings["from_mail_id"];
            string password = System.Configuration.ConfigurationManager.AppSettings["Epassword"];
            string strmsg = "";

            string result = SMSEMAIL.sendMailMessage(host, from_mail_id, password, email_id, "", "", "Password : Online Order Booking", gen_string(pwd),ref strmsg, "");
            //modMain.ShowMessage(result,this);
            if (strmsg.Length>0)
            {
                modMain.ShowMessage(strmsg,this);
            }
            if (result == "SUCCESS || Mail send Successfully.")
                modMain.ShowMessage("Email has been send to your ID.",this);
            else
            {
                modMain.ShowMessage("Email sending failure.Contact your system administrator.",this);
                return;
            }

            Page.Server.Transfer("Index.aspx");
        }
        public string gen_string(string strPassword)
        {
          
            StringBuilder mymessage = new StringBuilder();
            StringBuilder mymessage_partlist = new StringBuilder();
            strPassword = modMain.EncryptDecryptValue(strPassword, "0");
            mymessage.Append("<strong>Hi...</strong>");
            mymessage.Append("<br />");
            mymessage.Append("<br />");
            mymessage.Append("<strong>User Name:</strong> " + username.Text + " <br />");
            mymessage.Append("<strong>Password :</strong>  " + strPassword + " <br /><br /><br />");
            mymessage.Append("<strong>Online Order Booking</strong> <br />");
            return (mymessage.ToString());
        }
    }
}