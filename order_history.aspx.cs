﻿using Common;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GPGSOLASPNET
{
    public partial class order_history : System.Web.UI.Page
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                if (!User.Identity.IsAuthenticated) // if the user is already logged in
                {
                    Response.Redirect(FormsAuthentication.LoginUrl);
                }
                if (!Page.IsPostBack)
                {
                    Grid_fill();
                }

               
            }
            catch (Exception ex)
            {
                modMain.ShowMessage(ex.Message, this);
            }


        }

        void Grid_fill()
        {
            try
            {
              string  strquery = "EXEC ASP_SPMODULE  @NMODE='OrdHistory',@CCOBR_ID='" +  SessionManager.g_strcobr_id + "',@CWhere='" + SessionManager.g_strUserId + "',@CUSERPREFIX='" + modMain.g_strpartyfill + "'";

                modMain.gridviewbind(strquery, SessionManager.g_strConnString, grdviewnew);
               
                DataTable dt = new DataTable();
                dt = ((DataSet)grdviewnew.DataSource).Tables[0];
                dt.TableName = "table1";
                grdviewnew.HeaderRow.TableSection = TableRowSection.TableHeader;

            }
            catch (SqlException ex)
            {
                modMain.ShowMessage(ex.Message, this);
            }
           
        }
      


    }
}