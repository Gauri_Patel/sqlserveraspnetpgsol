﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GPGSOLASPNET
{
    public partial class Contact : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (! User.Identity.IsAuthenticated) // if the user is already logged in
            {
                Response.Redirect(FormsAuthentication.LoginUrl);
            }


        }

        protected void imgordbook_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("OrderBooking.aspx");
        }

        protected void imgordcat_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Catalogue.aspx"); 
        }

        protected void imgoutstanding_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Outstanding.aspx");
        }

        protected void imgordhistory_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("order_history.aspx");
        }

        protected void imgordbook_Click(object sender, EventArgs e)
        {

        }
    }
}