﻿using Common;
using Core;
using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;

namespace GPGSOLASPNET
{
    public partial class Index : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["logout"] == "Y")
            {
                // ''''isc_class.log_create("logged In", Session.Item("UserID"))
                SessionManager.ClearALL();
                Response.Redirect("Index.aspx");
            }
            // SessionContext.ClearALL();
            var db_name = System.Configuration.ConfigurationManager.AppSettings["dbname"];
          //  Compcode.Text = System.Configuration.ConfigurationManager.AppSettings["ccode"];
            //   Session.Add("db_name", db_name);
      //      SessionManager.g_strDatabase = db_name;
            
            
            FormsAuthentication.SignOut();
            SessionManager.g_strUserId = "0";
            SessionManager.g_strCompanyName = "";
            SessionManager.g_strCo_id = "";
            if (!IsPostBack)
            {
                //fyyearfill();
                // drpcomp_code.SelectedIndex = drpcomp_code.Items.Count - 2;
                // if (Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy")) < Convert.ToDateTime(("01/04/2020")))
                if (Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy"), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat) < Convert.ToDateTime("01/04/2020", System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat))
                    drpYear.SelectedIndex = drpYear.Items.Count - 2;
                else
                    drpYear.SelectedIndex = drpYear.Items.Count - 1;
            }
            //    Button1.Attributes.Add("onclick", "window.close()");
            drpYear.Focus();
        }

        protected void ForgotPwd(object sender, EventArgs e)
        {
            Response.Redirect("forgot_password.aspx?FCYR_FULLYR=" + drpYear.SelectedValue + "");
        }

        protected void LogIn(object sender, EventArgs e)
        {
            SessionManager.g_strConnString = "Data Source = " + SessionManager.g_strServerName + ";Initial Catalog = " + SessionManager.g_strDatabase + drpYear.SelectedValue + ";Integrated Security=SSPI";
            SessionManager.g_strConnStringACC = "Data Source = " + SessionManager.g_strServerName + ";Initial Catalog = " + SessionManager.g_strDatabaseAcc + drpYear.SelectedValue + ";Integrated Security=SSPI";
            SessionManager.g_strfcyr = drpYear.SelectedValue;
            SessionManager.g_strfcyrID = SessionManager.g_strfcyr.Substring(0, 2);
            if (IsValid)
            {
                DataTable worktable = new DataTable();

                try
                {
                    if (drpYear.Text.ToString().Length == 0)
                    {
                     
                        lblMessage.Text = "Cannot Be Blank Year!"; 
                        return;
                    }
                    if (username.Text.ToString().Length == 0)
                    {
                      
                        lblMessage.Text = "Cannot Be Blank User Name!";
                        return;
                    }
                    if (Password.Text.ToString().Length == 0)
                    {
                        
                        lblMessage.Text = "Cannot Be Blank Password!";
                        return;
                    }

                    if (InvalidDatabase())
                    {
                        lblMessage.Text = "Inventory/Accounts data not found for selected year !";
                       
                        return;
                    }
            

                    using (UserService u = new UserService())
                    {
                        string custfix = "";
                        string custType = "";
                        if (rdUser.Checked)
                        {
                            custfix = "U";
                            custType = "USER";
                            string strMessage = "";
                            if (u.IsValidUser(username.Text, Password.Text, out strMessage))
                            {
                                SignIn(username.Text, true);
//SessionManager.UserName = SessionManager.g_strUsername;
                               
                            }
                            else {
                               
                                lblMessage.Text = strMessage;
                                return;

                            }

                        }
                        else

                        {
                            if (rdcust.Checked)
                            {
                                custfix = "C";
                                custType = "Customer";
                            }
                            else
                            {
                                custfix = "S";
                                custType = "Salesperson";
                            }
                            worktable = u.getLoginData(username.Text, Password.Text, custfix, custType);

                            if (worktable.Rows.Count != 0)
                            {
                                DataRow myrow = worktable.Rows[0];

                                SessionManager.UserType = Convert.ToString(myrow["user_type"]);
                                if (rdcust.Checked)
                                {
                                    SessionManager.g_strUsername = Convert.ToString(myrow["PARTY_NAME"]);
                                    SessionManager.g_strUserId = Convert.ToString(myrow["PARTY_KEY"]);
                                }
                                else
                                {
                                    SessionManager.g_strUsername = Convert.ToString(myrow["SALEPERSON_NAME"]);
                                    SessionManager.g_strUserId = Convert.ToString(myrow["SALEPERSON_KEY"]);
                                }
                                SessionManager.AccountId = Convert.ToString(myrow["account_id"]);
                               
                                modMain.g_strpartyfill = Convert.ToString(myrow["PARTY_STATUS"]);
                                if (rdcust.Checked)
                                {
                                    SignIn(Convert.ToString(myrow["PARTY_NAME"]), false);
                                }
                                else
                                {
                                    SignIn(Convert.ToString(myrow["SALEPERSON_NAME"]), false);
                                }
                             
                            }
                            else
                            {
                               
                                lblMessage.Text = "Invalid Username or Password";
                                return;
                            }

                           
                        }
                        SessionManager.g_strYearText = drpYear.Text.ToString();
                        SessionManager.g_strCompanyName = Convert.ToString("");
                        SessionManager.g_strCo_id = Convert.ToString("");
                        SessionManager.g_strcobr_id = Convert.ToString("");
                        Response.Redirect("Select_comp.aspx", false);
                        Context.ApplicationInstance.CompleteRequest();
                    }


                }
                catch (Exception ex)
                {


                    modMain.ShowMessage(ex.Message,this);
                }
                finally
                {
                    worktable.Dispose();
                    //  CmdExecutor = null/* TODO Change to default(_) if this is not a reference type */;
                }
            }

        }
        private void SignIn(string struserName, bool createPersistentCookie)
        {
            var now = DateTime.UtcNow.ToLocalTime();

            var ticket = new FormsAuthenticationTicket(
                1 /*version*/,
                 struserName,
                now,
                now.Add(FormsAuthentication.Timeout),
                createPersistentCookie,
              struserName,
                FormsAuthentication.FormsCookiePath);

            var encryptedTicket = FormsAuthentication.Encrypt(ticket);

            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            cookie.HttpOnly = true;
            if (ticket.IsPersistent)
            {
                cookie.Expires = ticket.Expiration;
            }
            cookie.Secure = FormsAuthentication.RequireSSL;
            cookie.Path = FormsAuthentication.FormsCookiePath;
            if (FormsAuthentication.CookieDomain != null)
            {
                cookie.Domain = FormsAuthentication.CookieDomain;
            }

            Response.Cookies.Add(cookie);

        }
        protected void generateotp(object sender, EventArgs e)
        {
            try
            {
                if (InvalidDatabase())
                {
                    lblMessage.Text = "Inventory/Accounts data not found for selected year !";
                 
                    return;
                }

                if (txtmobileno.Text.ToString().Length == 0)
                {
                   
                    lblMessage.Text = "Cannot Be Blank Mobile No!";
                    return;
                }

                //For generating OTP
                Random r = new Random();
                string OTP = r.Next(1000, 9999).ToString();


                SendSMS sms = new SendSMS(SendSMS.TransType.Manual);
                modMain.g_paramSMSExpired = false;
                modMain.g_ParamSMSUser = "prasyst";
                modMain.g_ParamSMSPassword = "gupta33";
                sms.MobileNo = txtmobileno.Text;
                sms.SMSMessage = "Online Order Booking OTP For Login " + OTP;
                SessionManager.LOGINOTP = OTP;
                sms.SendSMSNow();
                txtOTP.Visible = true;
                btnverfyotp.Visible = true;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
            }
        }
        protected void Verifyotp(object sender, EventArgs e)
        {
            if (InvalidDatabase())
            {
                lblMessage.Text = "Inventory/Accounts data not found for selected year !";
               
                return;
            }


            if (SessionManager.LOGINOTP == txtOTP.Text)
            {
                lblMessage.Text = "You have enter correct OTP.";
                SessionManager.LOGINOTP = null;

                SessionManager.g_strConnString = "Data Source = " + SessionManager.g_strServerName + ";Initial Catalog = " + SessionManager.g_strDatabase + drpYear.SelectedValue + ";Integrated Security=SSPI";
                String strfcyr = drpYear.SelectedValue;
                SessionManager.g_strfcyrID = strfcyr.Substring(0,2);
                if (IsValid)
                {
                    DataTable worktable = new DataTable();

                    try
                    {

                        using (UserService u = new UserService())
                        {
                            worktable = u.getLoginID(txtmobileno.Text, rdcust.Checked ? "C" : "S", rdcust.Checked ? "Customer" : "Salesperson");

                            if (worktable.Rows.Count != 0)
                            {
                                DataRow myrow = worktable.Rows[0];

                                SessionManager.UserType = Convert.ToString(myrow["user_type"]);
                                if (rdcust.Checked)
                                {
                                    SessionManager.g_strUsername = Convert.ToString(myrow["PARTY_NAME"]);
                                    SessionManager.g_strUserId = Convert.ToString(myrow["PARTY_KEY"]);
                                }
                                else
                                {
                                    SessionManager.g_strUsername = Convert.ToString(myrow["SALEPERSON_NAME"]);
                                    SessionManager.g_strUserId = Convert.ToString(myrow["SALEPERSON_KEY"]);
                                }
                                SessionManager.AccountId = Convert.ToString(myrow["account_id"]);
                                SessionManager.g_strYearText = drpYear.Text.ToString();
                                SessionManager.g_strCompanyName = Convert.ToString("");
                                SessionManager.g_strCo_id = Convert.ToString("");
                                SessionManager.g_strcobr_id = Convert.ToString("");
                                modMain.g_strpartyfill = Convert.ToString(myrow["PARTY_STATUS"]);
                                if (rdcust.Checked)
                                {
                                    SignIn(Convert.ToString(myrow["PARTY_NAME"]), false);
                                }
                                else
                                {
                                    SignIn(Convert.ToString(myrow["SALEPERSON_NAME"]), false);
                                }
                                Response.Redirect("Select_comp.aspx", false);
                                Context.ApplicationInstance.CompleteRequest();
                            }
                            else
                            {
                               
                                lblMessage.Text = "Invalid Username";
                            }
                        }


                    }
                    catch (Exception ex)
                    {

                        modMain.ShowMessage(ex.Message,this);
                    }
                    finally
                    {
                        worktable.Dispose();
                        //  CmdExecutor = null/* TODO Change to default(_) if this is not a reference type */;
                    }
                }
            }
            else
            {
                lblMessage.Text = "Pleae enter correct OTP.";
            }
        }

        protected void username_TextChanged(object sender, EventArgs e)
        {

            SessionManager.g_strConnString = "Data Source = " + SessionManager.g_strServerName + ";Initial Catalog = " + SessionManager.g_strDatabase + drpYear.SelectedValue + ";Integrated Security=SSPI";
            DataTable worktable = new DataTable();

            try
            {
                if (drpYear.Text.ToString().Length == 0)
                {
                    
                    lblMessage.Text = "Cannot Be Blank Year!";
                    return;
                }
                //if (txtOTP.Text.ToString().Length == 0)
                //{
                //    FailureText.Text = "Cannot Be Blank OTP!";
                //    lblMessage.Text = "Cannot Be Blank OTP!";
                //    return;
                //}

                using (UserService u = new UserService())
                {
                    worktable = u.getLoginID(username.Text, rdcust.Checked ? "C" : "S", rdcust.Checked ? "Customer" : "Salesperson");

                    if (worktable.Rows.Count != 0)
                    {

                    }
                    else
                    {
                       
                        lblMessage.Text = "Invalid Username";
                    }
                }


            }
            catch (Exception ex)
            {

                modMain.ShowMessage(ex.Message,this);
            }
            finally
            {
                worktable.Dispose();
                //  CmdExecutor = null/* TODO Change to default(_) if this is not a reference type */;
            }

        }

        private bool InvalidDatabase()
        {

            String g_strQuery = "Select DbId From sysDatabases "
               + " Where Name = '" + SessionManager.g_strDatabase + drpYear.SelectedValue + "' Or Name = '" + SessionManager.g_strDatabaseAcc + drpYear.SelectedValue + "'";
            DataTable dtDBCount = modMain.NewDatatable(g_strQuery, modMain.g_strMastConnString);

            return (dtDBCount.Rows.Count <= 1);
        }
        [WebMethod]
        public static string KeepAliveSession()
        {
            return "";
        }
    }

}
