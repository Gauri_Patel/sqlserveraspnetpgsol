﻿<%@ Page Title="Order Item Details" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Order_history_Summary.aspx.cs" Inherits="GPGSOLASPNET.Order_history_Summary" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <link href="Content/DataTables/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <link href="Content/DataTables/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <%-- <script src="Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>--%>
    <script src="Scripts/DataTables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="Scripts/DataTables/dataTables.responsive.min.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=GridviewSize.ClientID%>').DataTable({ "paging": true, "ordering": true, "searching": true });
        });
    </script>
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-primary " id="list-panel">
                <div class="panel-heading ">
                    <h1 class="panel-title ">Order Item Details</h1>
                </div>
                <br />
                <div class="panel-body">

                    <asp:Label runat="server" ID="lblorderid" Visible="False"></asp:Label>
                    <asp:GridView ID="GridviewSize" runat="server" CellPadding="5" CellSpacing="0" CssClass="dt-responsive"
                        AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" Width="100%">
                        <Columns>

                            <asp:TemplateField HeaderText="" HeaderStyle-Width="100px" Visible="false">

                                <ItemTemplate>
                                    <%--<asp:Hyperlink ID="LinkButton1" runat="server" Text="Show" NavigateUrl='<%#Eval("ORDER_ID","Orderhistorydetails.aspx?ORDER_ID={0}&FGSTYLE_CODE=" + Eval("FGSTYLE_CODE") +"&FGTYPE_KEY=" + Eval("FGTYPE_KEY")+"&FGSHADE_KEY=" + Eval("FGSHADE_KEY"))%>' ></asp:Hyperlink>--%>
                                    <asp:Label ID="lnblORDER_ID" runat="server" Text='<%# Bind("ORDER_ID") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Order No" HeaderStyle-Width="100px">

                                <ItemTemplate>
                                    <asp:Label ID="lnblORDER_IDno" runat="server" Text='<%# Bind("ORDBK_NO") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="COBR_ID" SortExpression="COBR_ID" Visible="false">

                                <ItemTemplate>
                                    <asp:Label ID="COBR_ID" runat="server" Text='<%# Bind("COBR_ID") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Style" SortExpression="FGSTYLE_CODE" HeaderStyle-Width="100px">

                                <ItemTemplate>
                                    <asp:Label ID="txtqty" runat="server" Text='<%# Bind("FGSTYLE_CODE") %>' Width="80px"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="FGSTYLE_ID" SortExpression="FGSTYLE_ID" Visible="False">

                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("FGSTYLE_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type" SortExpression="FGTYPE_KEY" HeaderStyle-Width="100px">

                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("FGTYPE_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Shade" SortExpression="FGSHADE_KEY" HeaderStyle-Width="100px">

                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("FGSHADE_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Size" SortExpression="STYSIZE_NAME" HeaderStyle-Width="100px">

                                <ItemTemplate>
                                    <asp:Label ID="COBR_ID" runat="server" Text='<%# Bind("STYSIZE_NAME") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty" SortExpression="Qty" HeaderStyle-Width="100px">

                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Qty") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Rate" SortExpression="WSP" HeaderStyle-Width="100px">

                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("WSP") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" SortExpression="AMT" HeaderStyle-Width="100px">

                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("AMT") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <FooterStyle BorderStyle="Solid" BorderWidth="1px" />
                        <HeaderStyle BorderStyle="Solid" BorderWidth="1px" />
                    </asp:GridView>



                    <asp:HyperLink runat="server" CssClass=" btn-link btn" ID="Loginpage" ViewStateMode="Disabled" NavigateUrl="~/order_history.aspx">Go Back</asp:HyperLink>


                </div>
            </div>



        </div>



    </div>

</asp:Content>
