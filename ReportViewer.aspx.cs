﻿using Common;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GPGSOLASPNET
{
    public partial class ReportViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

            
            ReportDocument cryRpt = new ReportDocument();
            cryRpt.Load(Server.MapPath("Report\\OsBills.rpt"));
                lblpath.Text = Server.MapPath("Report\\OsBills.rpt");
            cryRpt.DataSourceConnections[0].IntegratedSecurity = false;
            cryRpt.DataSourceConnections[0].SetConnection("PGSACC", SessionManager.g_strDatabaseAcc + SessionManager.g_strYearText, "", "");
               
                //cryRpt.Refresh();
                if (cryRpt.IsLoaded)
                 { 
            
            }
            CrystalReportViewer1.ReportSource=cryRpt;
                // cryRpt.Load(Server.MapPath("Report\\OsBills.rpt"));
            }
            catch (Exception ex)
            {

                modMain.ShowMessage(ex.Message,this);
            }
        }
    }
}