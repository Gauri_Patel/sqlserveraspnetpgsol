﻿using Common;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;

namespace GPGSOLASPNET
{
    public partial class newCatalogue : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!User.Identity.IsAuthenticated) // if the user is already logged in
                {
                    Response.Redirect(FormsAuthentication.LoginUrl);
                }
                if (!Page.IsPostBack)
                {



                    HttpContext.Current.Session["dtcatelogdtl"] = null;
                    HttpContext.Current.Session["dtdetails"] = null;

                }


            }
            catch (Exception ex)
            {
                modMain.ShowMessage(ex.Message, this);
            }
        }

        protected void btnloadimg_Click(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string retrivestyles(int intpage,string products,string strStyle="")
        {

            String g_strquery = "exec SP_GETCATELOGUEINFO " + intpage + ",0,'" + products + "','" + strStyle + "'";
            int count = Convert.ToInt32(modMain.ExecuteScalar(g_strquery, null));
            int pagecount = 2;
            count = (count / pagecount + (count % pagecount > 0 ? 1 : 0));
            g_strquery = "exec SP_GETCATELOGUEINFO " + intpage + ","+ pagecount + ",'" + products + "','" + strStyle + "'";

            DataTable dt = modMain.NewDatatable(g_strquery, SessionManager.g_strConnString, null);
            dt.TableName = "Style";

            string s = JsonConvert.SerializeObject(new
            {
                totalcount = 5,//count-1,
                d = dt,

            }); ;

            return s;
        }

        [WebMethod]
        public static string bindShadeData(string StyleId)
        {

            try
            {
                SessionManager.g_strQuery = "EXEC SP_GETAlloctedShade  '" + StyleId + "'";
                DataTable dt = modMain.NewDatatable(SessionManager.g_strQuery, SessionManager.g_strConnString, null);
                //dt.Rows.Clear();
                //dt.Rows.Add();
                dt.TableName = "ShadeData";
                return JsonConvert.SerializeObject(dt);

            }
            catch (Exception ex)
            {
                throw;
            }

        }
        [WebMethod]
        public static string bindStyleData(string StyleId, string FgshadeId, string Strtype)
        {

            string strquery = "";
            DataTable worktable = new DataTable();
            DataSet ds = new DataSet();
            if ((DataTable)HttpContext.Current.Session["dtcatelogdtl"] != null)
            {
                worktable = ((DataTable)HttpContext.Current.Session["dtcatelogdtl"]).Copy();
                string filter = "FGSTYLE_ID='" + StyleId + "' and FGTYPE_KEY='" + Strtype + "' and FGSHADE_KEY='" + FgshadeId + "'";
                DataRow[] arr = worktable.Copy().Select(filter);


                if (arr.Length > 0)
                {
                    worktable.Rows.Clear();
                    worktable = arr.CopyToDataTable();
                    worktable.TableName = "Size";
                    ds.Tables.Add(worktable);
                }
            }


                strquery = "EXEC asp_catelog_getitmdtl  @PARTY_KEY='',@PARTYDTL_ID=0,@COBR_ID='" + SessionManager.g_strcobr_id + "',@FGSTYLE_ID=0,@FGSHADE_KEY='" + FgshadeId + "',@QTY=0";

            
            ds = modMain.NewDataset(strquery, SessionManager.g_strConnString, null);
            ds.Tables[0].TableName = "Size";
            if (0 == 0)
            {
                if (!ds.Tables[0].Columns.Contains("Error"))
                {


                    if (HttpContext.Current.Session["dtdetails"] != null)
                    {
                        DataTable dt = (DataTable)HttpContext.Current.Session["dtdetails"];
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {

                            DataRow[] arr = dt.Select("STYSIZE_NAME='" + item["STYSIZE_NAME"] + "'");
                            if (arr.Length > 0)
                            {
                                item["Qty"] = arr[0]["Qty"];
                            }
                        }
                    }
                    HttpContext.Current.Session["dtdetails"] = (DataTable)ds.Tables[0];
                }
            }
            return ds.GetXml();
        }
        [WebMethod]
        public static string Fillcategories()
        {

            try
            {
                SessionManager.g_strQuery = "EXEC SP_GETAlloctedShade  @strfgstyleid='',@mode='FGCAT'";
                DataTable dt = modMain.NewDatatable(SessionManager.g_strQuery, SessionManager.g_strConnString, null);
                //dt.Rows.Clear();
                //dt.Rows.Add();
                dt.TableName = "ShadeData";
                return JsonConvert.SerializeObject(dt);

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [WebMethod]
        public static string FillProducts(string categories)
        {

            try
            {
                SessionManager.g_strQuery = "EXEC SP_GETAlloctedShade  @strfgstyleid='"+ categories  + "',@mode='FGPRD'";
                DataTable dt = modMain.NewDatatable(SessionManager.g_strQuery, SessionManager.g_strConnString, null);
               
                return JsonConvert.SerializeObject(dt);

            }
            catch (Exception ex)
            {
                throw;
            }
        }


        [WebMethod]
        public static string fillsizedata(string strdt)
        {



            DataTable dt = (DataTable)JsonConvert.DeserializeObject(strdt, (typeof(DataTable)));
            try
            {
                DataTable dtsize = null;
                foreach (DataRow item in dt.Rows)
                {
                    SessionManager.g_strQuery = "EXEC asp_catelog_getitmdtl  @PARTY_KEY='',@PARTYDTL_ID=0,@COBR_ID='" + SessionManager.g_strcobr_id + "',@FGSTYLE_ID=" + item["FGSTYLE_ID"] + ",@FGSHADE_KEY='" + item["FGSHADE_KEY"] + "',@QTY=0";
                    if (dtsize == null)
                    {
                        DataTable dttemp = modMain.NewDatatable(SessionManager.g_strQuery, SessionManager.g_strConnString, null);
                        dtsize = dttemp.Copy();
                        dtsize.PrimaryKey = new DataColumn[] { dtsize.Columns["FGSTYLE_ID"], dtsize.Columns["FGSHADE_KEY"], dtsize.Columns["STYSIZE_ID"] };
                    }
                    else
                    {
                        DataTable dttemp = modMain.NewDatatable(SessionManager.g_strQuery, SessionManager.g_strConnString, null);

                        dtsize.Merge(dttemp, true, MissingSchemaAction.AddWithKey);

                    }





                }

                return JsonConvert.SerializeObject(dtsize);

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [WebMethod]
        public static string binddata(string strdt)
        {
            int i = 0;
            DataTable dt = (DataTable)JsonConvert.DeserializeObject(strdt, (typeof(DataTable)));
            DataRow drRow;

            if (SessionManager.dtcatelogdtl.Rows.Count > 0)
            {

                i = Convert.ToInt32(SessionManager.dtcatelogdtl.Compute("MAX(SRNO)", ""));
            }
            else
            {
                i = 0;
            }



            foreach (DataRow dr in dt.Rows)
            {
                i = i + 1;
                drRow = SessionManager.dtcatelogdtl.NewRow();
                drRow["SRNO"] = i;
                drRow["ALT_BARCODE"] = "";
                drRow["FGSTYLE_ID"] = dr["FGSTYLE_ID"];
                drRow["FGSTYLE_CODE"] = dr["Style"];
                drRow["FGTYPE_KEY"] = dr["fgtype_key"];
                drRow["FGTYPE_NAME"] = dr["Type"];
                drRow["FGSHADE_KEY"] = dr["FGSHADE_KEY"];
                drRow["FGSHADE_NAME"] = dr["Color"];
                drRow["STYSIZE_NAME"] = dr["Size"];
                drRow["STYSIZE_ID"] = dr["STYSIZE_ID"];
                //drRow["QTY"] = (Convert.ToString(drQty["QTY"]).Trim().Length == 0 ? 0 : drQty["QTY"]);
                drRow["Qty"] = dr["QTY"];//to add default qty next new record if same stysize_name
                drRow["MRP"] = dr["MRP"];
                drRow["WSP"] = dr["WSP"];
                drRow["AMOUNT"] = Convert.ToDouble(dr["WSP"]) * Convert.ToDouble(drRow["QTY"]);
                drRow["PENDING_ORD_QTY"] = dr["Pend Qty"];
                drRow["COBR_ID"] = SessionManager.g_strcobr_id;
                dr.AcceptChanges();
                SessionManager.dtcatelogdtl.Rows.Add(drRow);
                
            }
            return "1";

        }

    }
}