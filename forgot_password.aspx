﻿<%@ Page Title="Forgot Password" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="forgot_password.aspx.cs" Inherits="GPGSOLASPNET.forgot_password" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            var x = document.getElementById("Navigation");
            x.style.display = "none";
        });
            ]
    </script>
    <div class="row">
        <div class="col-md-8">
            <br />
            <div class="panel panel-primary list-panel" id="list-panel">
                <div class="panel-heading list-panel-heading">
                    <h1 class="panel-title list-panel-title">Forgot Password</h1>
                </div>
                <br />
                <div class="container">
                   
                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="username" CssClass="col-md-2 control-label">User Name</asp:Label>
                        <div class="col-md-8">
                            <asp:TextBox runat="server" ID="username" CssClass="form-control" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="username"
                                CssClass="text-danger" ErrorMessage="The User Name field is required." />
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                            <asp:Button runat="server" OnClick="Continue" Text="Continue" CssClass="btn btn-default" />
                            <%-- Enable this once you have account confirmation enabled for password reset functionality --%>
                            <asp:HyperLink runat="server" CssClass=" btn-link btn" ID="Loginpage" ViewStateMode="Disabled" NavigateUrl="~/Index.aspx">Go Back</asp:HyperLink>

                        </div>

                    </div>



                </div>
            </div>

        </div>


    </div>




</asp:Content>
