﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ContactAbout.aspx.cs" Inherits="GPGSOLASPNET.ContactAbout" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
   <h3>Information</h3>

<address>

    316 Hariom Plaza, Behind Omkareshwar Temple,
    </br>
    M.G Road, Landmark:Opposite to National Park, Mumbai, India.
    </br>
    Phone: (022) 28953371 - 76,
    </br>
    Mobile: 093210 20014
  
</address>

<address>
    <strong>Support:</strong>   <a href="mailto:support@prathamglobal.com">support@prathamglobal.com</a><br />
    <strong>Marketing:</strong> <a href="mailto:sales@prathamglobal.com">sales@prathamglobal.com</a>
</address>
</asp:Content>
