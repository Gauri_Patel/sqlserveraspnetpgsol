﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace Common
{
    public  class SendSMS
    {
        public enum TransType
        {
            SalesOrder = 1,
            Packing = 2,
            SalesBill = 3,
            SupplierBill = 4,
            DebitNote = 5,
            CreditNote = 6,
            CustomerReceipt = 7,
            SupplierPayment = 8,
            Manual = 9,
            StockTransfer = 10,
            SchemeTransfer = 11
        };
        public enum SendSMSType
        {
            None,
            Instant,
            Batch
        };
        public enum Notification
        {
            Yes,
            No
        };
        public enum PartyType
        {
            Customer,
            Supplier,
            Franchisee,
            SalesPerson,
            Broker,
            Others,
            Manual
        };
        TransType bytTransType;
        Byte vTransactionType;
        string vPartyMobileNo = "";
        string vSMSMessage = "";
        string vPartyNm = "";
        string vDocumentKey = "";
        string vPartyKey = "";
        string vTransNo = "";
        DateTime vTransDt;
        double vTransAmt;
        Notification vNotify;
        PartyType vPartyType;
        Notification vSMSStatus;
        Notification vContinueLoop;
       // string strPartyType = "";

        public SendSMS(TransType bytTrans)
        {
            bytTransType = bytTrans;
            if (bytTrans == TransType.SalesOrder)
            {
                vTransactionType = Convert.ToByte(TransType.SalesOrder);
            }
            else if (bytTrans == TransType.Packing)
            {
                vTransactionType = Convert.ToByte(TransType.Packing);
            }
            else if (bytTrans == TransType.SalesBill)
            {
                vTransactionType = Convert.ToByte(TransType.SalesBill);
            }
            else if (bytTrans == TransType.SupplierBill)
            {
                vTransactionType = Convert.ToByte(TransType.SupplierBill);
            }
            else if (bytTrans == TransType.DebitNote)
            {
                vTransactionType = Convert.ToByte(TransType.DebitNote);
            }
            else if (bytTrans == TransType.CreditNote)
            {
                vTransactionType = Convert.ToByte(TransType.CreditNote);
            }
            else if (bytTrans == TransType.CustomerReceipt)
            {
                vTransactionType = Convert.ToByte(TransType.CustomerReceipt);
            }
            else if (bytTrans == TransType.SupplierPayment)
            {
                vTransactionType = Convert.ToByte(TransType.SupplierPayment);
            }
            else if (bytTrans == TransType.StockTransfer)
            {
                vTransactionType = Convert.ToByte(TransType.StockTransfer);
            }
            else if (bytTrans == TransType.Manual)
            {
                vTransactionType = Convert.ToByte(TransType.Manual);
            }
            else if (bytTrans == TransType.SchemeTransfer)
            {
                vTransactionType = Convert.ToByte(TransType.SchemeTransfer);
            }
           

        }
        public void CreateNewFields()
        {
            SessionManager.g_strQuery = "Select Column_Name From Information_Schema.Columns  Where Table_Name = 'SMSBATCH' And Upper(Column_Name) = 'PARTYTYPE'";
            DataTable dtTable = modMain.NewDatatable(SessionManager.g_strQuery, SessionManager.g_strConnString, null);

            if (dtTable.Rows.Count == 0)
            {
                SessionManager.g_strQuery = "Alter Table SMSBATCH Add PartyType Varchar(1), PartyName Varchar(100)";
                modMain.ExecuteQuery(SessionManager.g_strQuery, null);
            }

            SessionManager.g_strQuery = "Select Column_Name From Information_Schema.Columns  Where Table_Name = 'SMSLOG' And Upper(Column_Name) = 'PARTYTYPE'";
            dtTable = modMain.NewDatatable(SessionManager.g_strQuery, SessionManager.g_strConnString, null);

            if (dtTable.Rows.Count == 0)
            {
                SessionManager.g_strQuery = "Alter Table SMSLOG Add PartyType Varchar(1), PartyName Varchar(100)";
                modMain.ExecuteQuery(SessionManager.g_strQuery, null);
            }
            dtTable = null;
        }

        public string MobileNo
        {
            set
            {
                vPartyMobileNo = value;
            }
        }
        public string PartyName
        {
            set
            {
                vPartyNm = value;
            }
        }
        public string DocumentKey
        {
            set
            {
                vDocumentKey = value;
            }
        }
        public string PartyKey
        {
            set
            {
                vPartyKey = value;
            }
        }
        public string TransNo
        {
            set
            {
                vTransNo = value;
            }
        }
        public string TransDate
        {
            set
            {
                vTransDt = Convert.ToDateTime(value);
            }
        }
        public string TransAmount
        {
            set
            {
                vTransAmt = Convert.ToDouble(value);
            }
        }
        public string SMSMessage
        {
            set
            {
                vSMSMessage = value;
            }
        }
        public Notification DisplayAlert
        {
            set
            {
                vNotify = value;
            }
        }
        public Notification ContinueAhead
        {
            get
            {
                return vContinueLoop;
            }
            set
            {
                vContinueLoop = value;
            }
        }
        public PartyType PartyTypeIs
        {
            set
            {
                vPartyType = value;
            }
        }
        public Notification SMSAllow
        {
            set
            {
                vSMSStatus = value;
            }
            get
            {
                return vSMSStatus;
            }
        }

        public void SendSMSNow()
        {
            try
            {
                //0-Message In Queue
                //1 - Submitted To Carrier
                //2 - Un Delivered
                //3 – Delivered
                //4 – Expired
                //8 – Rejected
                //9 – Message Sent
                //10 – Opted Out Mobile Number
                //11 – Invalid Mobile Number

                // HTTP_API_COM_Object.clsHTTP_API_COM_ObjClass objSMS = new HTTP_API_COM_Object.clsHTTP_API_COM_ObjClass();

                OBJSMS.Service objSMS = new OBJSMS.Service();


                String SuccessMsg = objSMS.SendTextSMS(modMain.g_ParamSMSUser, modMain.g_ParamSMSPassword, "91" + vPartyMobileNo, vSMSMessage, modMain.g_ParamSMSUser);

                
                //SessionManager.g_strQuery = "Insert Into SMSLog Values (" + SMSId + ",'" + Format$(Now, "MM/dd/yyyy hh:mm:ss tt") + "'," + vTransactionType + "," + vDocumentKey + ",'" + vPartyKey + "','" + vPartyMobileNo + "','" + vSMSMessage + "', '" + strSendStatus + "','" + modMain.g_strCoBranchID + "'," + vTransNo + ",'" + Convert.ToDateTime(vTransDt) + "'," + vTransAmt + ",'" + strPartyType + "','" + vPartyNm + "'," + Convert.ToInt32(SuccessMsg) + ")";
                //modMain.ExecuteQuery(SessionManager.g_strQuery,null);
            }
            catch (Exception ex)
            {
                modMain.ShowMessage(ex.Message,null);
            }
        }
        public void SMSLogDetails(string strSMSStatus, string strSendStatus)
        {
            String[] strSMS = null;
            char[] splitchar = { '-' };
            strSMS = strSMSStatus.Split(splitchar);
            if (strSMS.Length > 0)
            {
                strSendStatus = strSMS[1];
            }
            else
            {
                strSendStatus = strSMS[0];
            }
        }


        internal void SendSMSMessage()
        {
            throw new NotImplementedException();
        }
    }
}
