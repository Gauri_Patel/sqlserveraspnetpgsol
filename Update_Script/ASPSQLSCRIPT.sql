IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_NAME='ORDBK' AND COLUMN_NAME='ONLINE_ORDBK')
BEGIN
	ALTER TABLE ORDBK ADD ONLINE_ORDBK CHAR(1) NOT NULL DEFAULT ''
END
GO---PRASYSTSQL---
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_NAME='ORDBK' AND COLUMN_NAME='ONLINE_ORDBK')
BEGIN
	UPDATE ORDBK SET ONLINE_ORDBK='' WHERE ONLINE_ORDBK IS NULL
END
GO---PRASYSTSQL---
IF OBJECT_ID('ASP_SPMODULE','P') IS NOT NULL
	DROP PROCEDURE ASP_SPMODULE
GO---PRASYSTSQL---
CREATE  PROCEDURE dbo.ASP_SPMODULE                                          
(                                          
@NMODE      nvarchar(50)='',   
@userName   T_DESC_25='',
@USER_PWD   varchar(20)='',
@CFCYR      VARCHAR(5)='',                                          
@NUSERID     INT=0,                                          
@CUSERTY     VARCHAR(50)='CUSTOMER', 
@CUSERPREFIX     VARCHAR(20)='S', 
@CCOBR_ID     VARCHAR(5)='',                                          
@CCO_ID      VARCHAR(5) ='',
@cdoc_key    varchar(20)='',
@CWhere nVarchar(500)='',
@nnum varchar(10)='0',
@FGSTYLE_ID BIGINT=0,
@FGSHADE_KEY VARCHAR(5)='',
@FGTYPE_KEY VARCHAR(5)='',
@ALT_BARCODE varchar(100)='',
@QTY Bigint=0
                              
)                                          
AS                                           
BEGIN   
DECLARE @CCMD NVARCHAR(MAX),@CERR_MSG VARCHAR(2000)                                          
--BEGIN TRY     

	if (@NMODE='getLoginID')
	   BEGIN 
			if (@CUSERPREFIX='C')
			BEGIN
				Set @CCMD='select a.*, '''+@CUSERPREFIX+''' as PARTY_STATUS,0 account_id,'''+@CUSERTY+''' as user_type from party a where party_cat=''PC'' and a.SMS_MOBILENO='''+ @userName +''''
				print @CCMD
				EXECUTE SP_EXECUTESQL @CCMD   
			END
			ELSE
			BEGIN
				Set @CCMD='select a.*, '''+@CUSERPREFIX+''' as PARTY_STATUS,0 account_id,'''+@CUSERTY+''' as user_type from SALEPERSON a where  a.SMSMOBILE_NO='''+ @userName +''''
				
				print @CCMD
				EXECUTE SP_EXECUTESQL @CCMD   
			END
	   END

   if (@NMODE='getLoginData')
	   BEGIN 
			print @CUSERPREFIX
			if (@CUSERPREFIX='C')
			BEGIN
				Set @CCMD='Select a.*, '''+@CUSERPREFIX+''' as PARTY_STATUS,0 account_id,'''+@CUSERTY+''' as user_type from party a where party_cat=''PC'' and a.SMS_MOBILENO='''+ @userName +''' and a.SMS_MOBILENO ='''+ @USER_PWD +''''
				print @CCMD
				EXECUTE SP_EXECUTESQL @CCMD   
			END
			ELSE
			BEGIN
				Set @CCMD='select a.*, '''+@CUSERPREFIX+''' as PARTY_STATUS,0 account_id,'''+@CUSERTY+''' as user_type from SALEPERSON a where  a.SMSMOBILE_NO='''+ @userName +''' and a.SMSMOBILE_NO ='''+ @USER_PWD +''''
				print @CCMD
				EXECUTE SP_EXECUTESQL @CCMD   
			END
	   END
	
	if (@NMODE='CompFill')
	   BEGIN 
			Set @CCMD='SELECT a.COBR_ID,b.CO_NAME+''-''+a.COBR_NAME+''-''+a.CO_ID  as COBR_NAME  FROM cobr a join Company b on a.co_id=b.co_id where a.COBR_ID<>'''''
			print @CCMD
			EXECUTE SP_EXECUTESQL @CCMD   
	   END

	if (@NMODE='CatlogueFill')
	   BEGIN 
			Set @CCMD='Select DISTINCT top ('+ @nnum +')  STYLE_IMG as Image,b.FGSTYLE_CODE as fgstyle_key from STYDTL a left join fgstyle b on a.FGSTYLE_ID=b.FGSTYLE_ID where STYLE_IMG<>''''  and b.PRODUCT_TYPE='''+ @CWhere +''''
			print @CCMD
			EXECUTE SP_EXECUTESQL @CCMD   
	   END

	if (@NMODE='OrdHistory')
	   BEGIN 
			if (@CUSERPREFIX='C')
			BEGIN
				Set @CCMD='SELECT ORDBK_KEY AS order_id, COBR_ID,ORDBK_NO,ORDBK_DT,a.PARTY_KEY,DLV_DT,a.BROKER_KEY,BROKER_NAME,a.REMK,CASE WHEN (d.DISTBTR_NAME IS NULL OR d.DISTBTR_NAME='''') THEN ''NA'' ELSE d.DISTBTR_NAME END 
						AS CONSIGNEE,c.PARTY_NAME FROM ORDBK a 
						left join party c on c.PARTY_KEY = a.PARTY_KEY  
						left join CLIENTTERMS e on e.PARTY_KEY=c.PARTY_KEY
						left join broker b on e.broker_key = b.broker_key  
						left join DISTBTR d on a.DISTBTR_KEY=d.DISTBTR_KEY
						where COBR_ID='''+ @CCOBR_ID +''' and a.party_key='''+@CWhere+''' and a.online_ordbk=''S'''
			
			END
			ELSE if (@CUSERPREFIX='S')
			BEGIN
				Set @CCMD='SELECT ORDBK_KEY AS order_id, COBR_ID,ORDBK_NO,ORDBK_DT,a.PARTY_KEY,DLV_DT,a.BROKER_KEY,BROKER_NAME,a.REMK,CASE WHEN (d.DISTBTR_NAME IS NULL OR d.DISTBTR_NAME='''') THEN ''NA'' ELSE d.DISTBTR_NAME END 
						AS CONSIGNEE,c.PARTY_NAME FROM ORDBK a 
						left join party c on c.PARTY_KEY = a.PARTY_KEY  
						left join CLIENTTERMS e on e.PARTY_KEY=c.PARTY_KEY
						left join broker b on e.broker_key = b.broker_key  
						left join DISTBTR d on a.DISTBTR_KEY=d.DISTBTR_KEY
						where COBR_ID='''+ @CCOBR_ID +''' and e.SALEPERSON1_KEY='''+@CWhere+''' and a.online_ordbk=''S'''
			
			END
			BEGIN
				Set @CCMD='SELECT ORDBK_KEY AS order_id, COBR_ID,ORDBK_NO,ORDBK_DT,a.PARTY_KEY,DLV_DT,a.BROKER_KEY,BROKER_NAME,a.REMK,CASE WHEN (d.DISTBTR_NAME IS NULL OR d.DISTBTR_NAME='''') THEN ''NA'' ELSE d.DISTBTR_NAME END 
						AS CONSIGNEE,c.PARTY_NAME FROM ORDBK a 
						left join party c on c.PARTY_KEY = a.PARTY_KEY  
						left join CLIENTTERMS e on e.PARTY_KEY=c.PARTY_KEY
						left join broker b on e.broker_key = b.broker_key  
						left join DISTBTR d on a.DISTBTR_KEY=d.DISTBTR_KEY
						where COBR_ID='''+ @CCOBR_ID +'''  and a.online_ordbk=''S'''
			
			END
			print @CCMD
			EXECUTE SP_EXECUTESQL @CCMD     
	   END

	if (@NMODE='OrdHistoryFill')
	   BEGIN 
			Set @CCMD='SELECT  b.cobr_id,a.ORDBK_KEY ORDER_ID,b.ORDBK_NO,FGSTYLE_CODE,a.FGSTYLE_ID,D.FGTYPE_NAME,E.FGSHADE_NAME ,STYSIZE_NAME,Sum(c.QTY) AS QTY,MAX(WSP) WSP,caST(ROund(Sum(C.wsp*C.QTY),2) as numeric(15,2)) AS AMT 
						FROM ORDBKSTY a left join ORDBK b on a.ORDBK_KEY=b.ORDBK_KEY 
						left join ORDBKSTYSZ c on a.ORDBK_KEY=b.ORDBK_KEY and a.ORDBKSTY_ID=c.ORDBKSTY_ID
						left join FGTYPE d on d.FGTYPE_KEY=a.FGTYPE_KEY
						left join FGshade e on e.FGSHADE_KEY=a.FGSHADE_KEY
						where b.ORDBK_KEY='''+ @cdoc_key +'''  GROUP by a.FGSTYLE_ID,b.cobr_id,a.ORDBK_KEY,b.ORDBK_NO,FGSTYLE_CODE,D.FGTYPE_NAME,E.FGSHADE_NAME,STYSIZE_NAME'
			print @CCMD
			EXECUTE SP_EXECUTESQL @CCMD   
	   END

	   if (@NMODE='stylefill')
	   BEGIN 
			Set @CCMD='Select  FGSTYLE_ID ,fgstyle_code FGSTYLE_NAME from fgstyle where fgstyle_code like ''%'+ @CWhere +'%'' Order BY fgstyle_code'
			print @CCMD
			EXECUTE SP_EXECUTESQL @CCMD   
	   END

	   if (@NMODE='stylefillwebservices')
	   BEGIN 
			Set @CCMD='Select fgstyle_code FGSTYLE_NAME from fgstyle where fgstyle_code like ''%'+ @CWhere +'%''  Order BY fgstyle_code'
			print @CCMD
			EXECUTE SP_EXECUTESQL @CCMD   
	   END

	    if (@NMODE='styletypefill')
	   BEGIN 
			Set @CCMD='Select DISTINCT  a.FGTYPE_KEY, FGTYPE_NAME from FGSTYLE a left Join FGtype b 
			on a.FGTYPE_KEY=b.FGTYPE_KEY where FGSTYLE_ID ='+@CWhere+' Order BY FGTYPE_NAME 
'
			print @CCMD
			EXECUTE SP_EXECUTESQL @CCMD   
	   END

	    if (@NMODE='styleshadefill')
	   BEGIN 
	   if(@CUSERPREFIX='ALL')
			Set @CCMD='Select DISTINCT FGSHADE_KEY,CASE when len(FGSHADE_NAME)=0 then ''(none)'' else FGSHADE_NAME end   AS FGSHADE_NAME  from FGSHADE b 
			 where status=''1'' Order BY FGSHADE_NAME '
			else
				Set @CCMD='Select DISTINCT a.FGSHADE_KEY,FGSHADE_NAME AS FGSHADE_NAME  from StyShade a left join FGSHADE b 
			on a.FGSHADE_KEY=b.FGSHADE_KEY where a.FGSTYLE_ID ='+@CWhere+' Order BY FGSHADE_NAME '

			print @CCMD
			EXECUTE SP_EXECUTESQL @CCMD   
	   END

	    if (@NMODE='PARTYPLACEFILL')
	   BEGIN 
			Set @CCMD='SELECT PARTYDTL_ID, PLACE FROM PARTYDTL WHERE PARTY_KEY = '''+@CWhere+''' ORDER BY MAIN_BRANCH DESC,PLACE '
			print @CCMD
			EXECUTE SP_EXECUTESQL @CCMD   
	   END

	    if (@NMODE='styleimgfill')
	   BEGIN 
			Set @CCMD='Select a.STYLE_IMG as IMAGE from STYDTL a  where a.FGSTYLE_ID ='+@CWhere+''
			print @CCMD
			EXECUTE SP_EXECUTESQL @CCMD   
	   END

	    if (@NMODE='Partyfill')
	   BEGIN 
			if (@CUSERPREFIX='C')
			BEGIN
				Set @CCMD='SELECT a.PARTY_KEY,a.PARTY_NAME FROM party a where a.party_key='''+@CWhere+'''  Order BY PARTY_NAME'
				print @CCMD
				EXECUTE SP_EXECUTESQL @CCMD   
			END
			ELSE if (@CUSERPREFIX='S')
			BEGIN
				Set @CCMD='select distinct b.PARTY_KEY,b.PARTY_NAME from CLIENTTERMS a left join party b on a.PARTY_KEY=b.PARTY_KEY
						 left join SALEPERSON c on c.SALEPERSON_KEY=a.SALEPERSON1_KEY where  C.SALEPERSON_KEY='''+ @CWhere +''''
				print @CCMD
				EXECUTE SP_EXECUTESQL @CCMD   
			END
			ELSE
			BEGIN
				Set @CCMD='SELECT a.PARTY_KEY,a.PARTY_NAME FROM party a where Status=''1'' Order BY PARTY_NAME'
						
				print @CCMD
				EXECUTE SP_EXECUTESQL @CCMD   
			END
	   END

	   

	    if (@NMODE='Partydetailsfill')
	   BEGIN 
			Set @CCMD='SELECT A.ADDR,CASE WHEN (C.DISTBTR_NAME IS NULL OR C.DISTBTR_NAME='''') THEN ''NA'' ELSE C.DISTBTR_NAME END  AS CONSIGNEE_NAME 
			FROM PARTY A LEFT JOIN CLIENTTERMS B ON A.PARTY_KEY=B.PARTY_KEY LEFT JOIN DISTBTR C ON C.DISTBTR_KEY=B.DISTBTR_KEY WHERE A.PARTY_KEY='''+ @CWhere +''''
			print @CCMD
			EXECUTE SP_EXECUTESQL @CCMD   
	   END

	   if (@NMODE='PartyDtldetailsfill')
	   BEGIN 
			Set @CCMD='SELECT A.ADDR,CASE WHEN (C.DISTBTR_NAME IS NULL OR C.DISTBTR_NAME='''') THEN ''NA'' ELSE C.DISTBTR_NAME END  AS CONSIGNEE_NAME 
			FROM PARTYDTL A LEFT JOIN CLIENTTERMS B ON A.PARTY_KEY=B.PARTY_KEY LEFT JOIN DISTBTR C ON C.DISTBTR_KEY=B.DISTBTR_KEY WHERE  A.PARTYDTL_ID='+ @CWhere +''
			print @CCMD
			EXECUTE SP_EXECUTESQL @CCMD   
	   END

	   if (@NMODE='GenerateCode')
	   BEGIN
		 GENERATE_CODE:    
		 DECLARE @cSRCODE VARCHAR(2000)   
		 DECLARE @cfcyr_key VARCHAR(2)  
		 
		 select  @cfcyr_key=fcyr_key from fcyr where currn_fcyr='1' 

		 EXEC SPCOMMON_GETNEXTKEY 'Sales Order','ORDBK','ORDBK_NO',6, @cSRCODE OUTPUT,'', @CCOBR_ID,@cfcyr_key
		 SELECT @cSRCODE as [New_Value]    
		 print @cSRCODE
	   END 

	    if (@NMODE='fillgridSize')
	   BEGIN 
			Set @CCMD='SELECT 0  SRNO,ALT_BARCODE,B.FGSTYLE_ID,FGSTYLE_CODE,B.FGTYPE_KEY,'''' AS FGTYPE_NAME,'''' FGSHADE_NAME,FGSHADE_KEY,STYSIZE_NAME,ISNULL(A.CL_QTY ,0) 
			AS QTY,MRP,0 AS PENDING_ORD_QTY FROM STYSTKDTL A JOIN STYSTK B ON A.STYSTK_KEY=B.STYSTK_KEY
			LEFT JOIN FGSTYLE C ON B.FGSTYLE_ID=C.FGSTYLE_ID WHERE A.STYSTK_KEY=''AA1''
'
			print @CCMD
			EXECUTE SP_EXECUTESQL @CCMD   
	   END

	    if (@NMODE='fillgridSizedata')
	   BEGIN 
			Set @CCMD='SELECT 0  SRNO,ALT_BARCODE,B.FGSTYLE_ID,FGSTYLE_CODE,B.FGTYPE_KEY,'''' AS FGTYPE_NAME,'''' FGSHADE_NAME,FGSHADE_KEY,STYSIZE_NAME,ISNULL(A.CL_QTY ,0) 
			AS QTY,MRP,0 AS PENDING_ORD_QTY,'''' as cobr_id FROM STYSTKDTL A JOIN STYSTK B ON A.STYSTK_KEY=B.STYSTK_KEY
			LEFT JOIN FGSTYLE C ON B.FGSTYLE_ID=C.FGSTYLE_ID WHERE A.STYSTK_KEY=''AA1''
'
			print @CCMD
			EXECUTE SP_EXECUTESQL @CCMD   
	   END


	   if (@NMODE='fillgrid')
	   begin

	   SET @CCMD= 'SELECT  0 SRNO,A.ALT_BARCODE,B.FGSTYLE_ID,FGSTYLE_CODE,C.FGTYPE_KEY,FGTYPE_NAME, E.FGSHADE_NAME,B.FGSHADE_KEY,A.STYSIZE_NAME
				,'+ @QTY +' AS QTY,MRP,round(((ISNULL(A.CL_QTY,0)+ISNULL(A.PROC_STK,0))-ISNULL(A.ORD_BAL,0)) ,2) AS PENDING_ORD_QTY 
				FROM STYSTKDTL A JOIN STYSTK B ON A.STYSTK_KEY=B.STYSTK_KEY
				LEFT JOIN FGSTYLE C ON B.FGSTYLE_ID=C.FGSTYLE_ID 
				LEFT JOIN FGTYPE D ON B.FGTYPE_KEY=D.FGTYPE_KEY 
				LEFT JOIN FGSHADE E ON B.FGSHADE_KEY=E.FGSHADE_KEY 
				LEFT JOIN STYSIZE I ON A.STYSIZE_ID=I.STYSIZE_ID
				LEFT JOIN FGSIZE J ON J.FGSIZE_ID = I.FGSIZE_ID 
				WHERE c.FGSTYLE_ID ='+CONVERT(VARCHAR(10),@FGSTYLE_ID)+' AND  B.FGSHADE_KEY =''' + @FGSHADE_KEY + ''' AND  B.COBR_ID='''+@CCOBR_ID+'''
				Order by J.FGSIZE_ABRV'
				--SET @CCMD= 'SELECT distinct 0 SRNO,A.ALT_BARCODE,B.FGSTYLE_ID,FGSTYLE_CODE,C.FGTYPE_KEY,FGTYPE_NAME, E.FGSHADE_NAME,B.FGSHADE_KEY,A.STYSIZE_NAME
				--,ISNULL(A.CL_QTY ,0) AS QTY,MRP,round(((ISNULL(A.CL_QTY,0)+ISNULL(PROSTK.PENDING_PROSTK_QTY,0))-ISNULL(ORD.PENDING_ORD_QTY,0)) ,2) AS PENDING_ORD_QTY 
				--FROM STYSTKDTL A JOIN STYSTK B ON A.STYSTK_KEY=B.STYSTK_KEY
				--LEFT JOIN FGSTYLE C ON B.FGSTYLE_ID=C.FGSTYLE_ID 
				--LEFT JOIN FGTYPE D ON B.FGTYPE_KEY=D.FGTYPE_KEY 
				--LEFT JOIN FGSHADE E ON B.FGSHADE_KEY=E.FGSHADE_KEY 
				--LEFT JOIN STYSIZE I ON A.STYSIZE_ID=I.STYSIZE_ID
				--LEFT JOIN FGSIZE J ON J.FGSIZE_ID = I.FGSIZE_ID 
				--LEFT JOIN   
				--  (  
				--   SELECT SUM(A.BAL_QTY) AS PENDING_ORD_QTY ,C.FCYR_KEY+C.COBR_ID+B.FGITM_KEY AS ORD_KEY,STYSIZE_ID,STYSIZE_NAME  
				--   FROM ORDBKSTYSZ A LEFT JOIN ORDBKSTY B ON A.ORDBKSTY_ID=B.ORDBKSTY_ID  
				--   LEFT JOIN ORDBK C ON C.ORDBK_KEY=B.ORDBK_KEY  
				--   WHERE A.BAL_QTY>0  
				--   GROUP BY C.FCYR_KEY,C.COBR_ID,B.FGITM_KEY ,STYSIZE_ID,STYSIZE_NAME  
				--  ) ORD ON ORD.ORD_KEY=A.STYSTK_KEY AND ORD.STYSIZE_ID=A.STYSIZE_ID  
				--  LEFT JOIN   
				--  (  
				--   SELECT SUM(A.ISU_BAL_QTY) AS PENDING_PROSTK_QTY ,C.FCYR_KEY+C.COBR_ID+B.FGITM_KEY AS PROSTK_KEY,STYSIZE_ID,STYSIZE_NAME  
				--   FROM PROISUSTYSZ A LEFT JOIN PROISUDTL B ON A.PROISUDTL_ID=B.PROISUDTL_ID  
				--   LEFT JOIN PROISU C ON C.PROISU_KEY=B.PROISU_KEY  
				--   WHERE A.ISU_BAL_QTY>0 AND C.ISU_TYPE=''I''  
				--   GROUP BY C.FCYR_KEY,C.COBR_ID,B.FGITM_KEY ,STYSIZE_ID,STYSIZE_NAME  
				--  ) PROSTK ON PROSTK.PROSTK_KEY=A.STYSTK_KEY AND PROSTK.STYSIZE_ID=A.STYSIZE_ID  
				--WHERE c.FGSTYLE_ID ='+CONVERT(VARCHAR(10),@FGSTYLE_ID)+' AND  B.FGSHADE_KEY =''' + @FGSHADE_KEY + ''' AND  B.COBR_ID='''+@CCOBR_ID+'''
				--Order by J.FGSIZE_ABRV'
				print @CCMD
				EXECUTE SP_EXECUTESQL @CCMD  
		END

		if (@NMODE='fillgridStyle')
	   begin

				SET @CCMD= 'SELECT  0 SRNO,'''' ALT_BARCODE,C.FGSTYLE_ID,FGSTYLE_CODE,C.FGTYPE_KEY,FGTYPE_NAME, '''' FGSHADE_NAME,'''' FGSHADE_KEY,I.STYSIZE_NAME,
					0 QTY ,MRP,round(((ISNULL(STY.CL_QTY,0)+ISNULL(STY.PROC_STK,0))-ISNULL(STY.ORD_BAL,0)) ,2) AS  PENDING_ORD_QTY 
					FROM  FGSTYLE C 
					LEFT JOIN FGTYPE D ON C.FGTYPE_KEY=D.FGTYPE_KEY 
					LEFT JOIN STYSIZE I ON C.FGSTYLE_ID=I.FGSTYLE_ID
					LEFT JOIN FGSIZE J ON J.FGSIZE_ID = I.FGSIZE_ID 
					LEFT JOIN   
					  (  
					   SELECT SUM(A.Cl_QTY) AS CL_QTY,SUM(A.PROC_STK) AS PROC_STK,SUM(A.ORD_BAL) AS ORD_BAL ,C.FCYR_KEY,C.COBR_ID,C.FGSTYLE_ID,C.FGSHADE_KEY,A.STYSIZE_ID,A.STYSIZE_NAME  
					   FROM STYSTKDTL A LEFT JOIN  STYSTK C ON C.STYSTK_KEY=A.STYSTK_KEY  
					   GROUP BY C.FCYR_KEY,C.COBR_ID,C.FGSTYLE_ID,C.FGSHADE_KEY,A.STYSIZE_ID,A.STYSIZE_NAME  
					  ) STY ON STY.FGSTYLE_ID='+CONVERT(VARCHAR(10),@FGSTYLE_ID)+' AND  STY.FGSHADE_KEY =''' + @FGSHADE_KEY + ''' AND STY.STYSIZE_ID=I.STYSIZE_ID 
					  AND STY.COBR_ID='''+@CCOBR_ID+''' AND STY.FCYR_KEY='''+@CFCYR+'''
					  WHERE C.FGSTYLE_ID ='+CONVERT(VARCHAR(10),@FGSTYLE_ID)+'
					  Order by J.FGSIZE_ABRV'
				--SET @CCMD= 'SELECT distinct 0 SRNO,'''' ALT_BARCODE,C.FGSTYLE_ID,FGSTYLE_CODE,C.FGTYPE_KEY,FGTYPE_NAME, '''' FGSHADE_NAME,'''' FGSHADE_KEY,I.STYSIZE_NAME,
				--	0 QTY ,MRP,round(((ISNULL(STY.CL_QTY,0)+ISNULL(PROSTK.PENDING_PROSTK_QTY,0))-ISNULL(ORD.PENDING_ORD_QTY,0)) ,2) AS  PENDING_ORD_QTY 
				--	FROM  FGSTYLE C 
				--	LEFT JOIN FGTYPE D ON C.FGTYPE_KEY=D.FGTYPE_KEY 
				--	LEFT JOIN STYSIZE I ON C.FGSTYLE_ID=I.FGSTYLE_ID
				--	LEFT JOIN FGSIZE J ON J.FGSIZE_ID = I.FGSIZE_ID 
				--	LEFT JOIN   
				--  (  
				--   SELECT SUM(A.BAL_QTY) AS PENDING_ORD_QTY ,C.FCYR_KEY,C.COBR_ID,B.FGSTYLE_ID,B.FGSHADE_KEY,STYSIZE_ID,STYSIZE_NAME  
				--   FROM ORDBKSTYSZ A LEFT JOIN ORDBKSTY B ON A.ORDBKSTY_ID=B.ORDBKSTY_ID  
				--   LEFT JOIN ORDBK C ON C.ORDBK_KEY=B.ORDBK_KEY  
				--   WHERE A.BAL_QTY>0  
				--   GROUP BY C.FCYR_KEY,C.COBR_ID,B.FGSTYLE_ID,B.FGSHADE_KEY ,STYSIZE_ID,STYSIZE_NAME  
				--  ) ORD ON ORD.FGSTYLE_ID='+CONVERT(VARCHAR(10),@FGSTYLE_ID)+' AND  ORD.FGSHADE_KEY =''' + @FGSHADE_KEY + ''' AND ORD.STYSIZE_ID=I.STYSIZE_ID  
				--   AND ORD.COBR_ID='''+@CCOBR_ID+''' AND ORD.FCYR_KEY='''+@CFCYR+'''
				--  LEFT JOIN   
				--  (  
				--   SELECT SUM(A.ISU_BAL_QTY) AS PENDING_PROSTK_QTY ,C.FCYR_KEY,C.COBR_ID,B.FGSTYLE_ID,B.FGSHADE_KEY,STYSIZE_ID,STYSIZE_NAME  
				--   FROM PROISUSTYSZ A LEFT JOIN PROISUDTL B ON A.PROISUDTL_ID=B.PROISUDTL_ID  
				--   LEFT JOIN PROISU C ON C.PROISU_KEY=B.PROISU_KEY  
				--   WHERE A.ISU_BAL_QTY>0 AND C.ISU_TYPE=''I''  
				--   GROUP BY C.FCYR_KEY,C.COBR_ID,B.FGSTYLE_ID,B.FGSHADE_KEY ,STYSIZE_ID,STYSIZE_NAME  
				--  ) PROSTK ON PROSTK.FGSTYLE_ID='+CONVERT(VARCHAR(10),@FGSTYLE_ID)+' AND  PROSTK.FGSHADE_KEY =''' + @FGSHADE_KEY + ''' AND PROSTK.STYSIZE_ID=I.STYSIZE_ID 
				--  AND PROSTK.COBR_ID='''+@CCOBR_ID+''' AND PROSTK.FCYR_KEY='''+@CFCYR+'''
				--  LEFT JOIN   
				--  (  
				--   SELECT SUM(A.Cl_QTY) AS CL_QTY ,C.FCYR_KEY,C.COBR_ID,C.FGSTYLE_ID,C.FGSHADE_KEY,A.STYSIZE_ID,A.STYSIZE_NAME  
				--   FROM STYSTKDTL A LEFT JOIN  STYSTK C ON C.STYSTK_KEY=A.STYSTK_KEY  
				--   GROUP BY C.FCYR_KEY,C.COBR_ID,C.FGSTYLE_ID,C.FGSHADE_KEY,A.STYSIZE_ID,A.STYSIZE_NAME  
				--  ) STY ON STY.FGSTYLE_ID='+CONVERT(VARCHAR(10),@FGSTYLE_ID)+' AND  STY.FGSHADE_KEY =''' + @FGSHADE_KEY + ''' AND STY.STYSIZE_ID=I.STYSIZE_ID 
				--  AND STY.COBR_ID='''+@CCOBR_ID+''' AND STY.FCYR_KEY='''+@CFCYR+'''
				--  WHERE C.FGSTYLE_ID ='+CONVERT(VARCHAR(10),@FGSTYLE_ID)+'
				--  Order by J.FGSIZE_ABRV'
				print @CCMD
				EXECUTE SP_EXECUTESQL @CCMD  
		END

		if (@NMODE='fillgridBarcode')
	   begin

				set @CCMD='SELECT '''' SRNO,A.ALT_BARCODE,B.FGSTYLE_ID,FGSTYLE_CODE,C.FGTYPE_KEY,FGTYPE_NAME, E.FGSHADE_NAME,B.FGSHADE_KEY,A.STYSIZE_NAME,'+ @QTY +'
				AS QTY,MRP,(SELECT ISNULL(SUM(ORDBKSTYSZ.BAL_QTY),0) FROM ORDBKSTYSZ  LEFT JOIN ORDBKSTY ON ORDBKSTY.ORDBKSTY_ID =ORDBKSTYSZ.ORDBKSTY_ID 
				LEFT JOIN ORDBK ON ORDBK.ORDBK_KEY =ORDBKSTY.ORDBK_KEY  
				LEFT JOIN STYSIZE F ON F.STYSIZE_ID=ORDBKSTYSZ.STYSIZE_ID
				LEFT JOIN FGSIZE G ON F.FGSIZE_ID = G.FGSIZE_ID 
				WHERE ORDBKSTY.FGSTYLE_ID =B.FGSTYLE_ID AND  ORDBKSTY.FGSHADE_KEY =B.FGSHADE_KEY  AND ORDBK.COBR_ID='''+@CCOBR_ID+'''
				AND ORDBKSTYSZ.STYSIZE_ID=F.STYSIZE_ID AND ORDBK.STATUS = ''1'' AND ORDBKSTYSZ.BAL_QTY > 0) AS PENDING_ORD_QTY 
				FROM STYSTKDTL A JOIN STYSTK B ON A.STYSTK_KEY=B.STYSTK_KEY
				LEFT JOIN FGSTYLE C ON B.FGSTYLE_ID=C.FGSTYLE_ID 
				LEFT JOIN FGTYPE D ON B.FGTYPE_KEY=D.FGTYPE_KEY 
				LEFT JOIN FGSHADE E ON B.FGSHADE_KEY=E.FGSHADE_KEY 
				WHERE A.STYSTKDTL_KEY=''' + @ALT_BARCODE + ''' AND  B.COBR_ID='''+@CCOBR_ID+''''
				print @CCMD
				EXECUTE SP_EXECUTESQL @CCMD  
		END

			if (@NMODE='fillgridAltBarcode')
	   begin

				set @CCMD='SELECT 0 SRNO,A.ALT_BARCODE,B.FGSTYLE_ID,FGSTYLE_CODE,C.FGTYPE_KEY,FGTYPE_NAME, E.FGSHADE_NAME,B.FGSHADE_KEY,A.STYSIZE_NAME,'+ @QTY +'
				AS QTY,MRP,(SELECT ISNULL(SUM(ORDBKSTYSZ.BAL_QTY),0) FROM ORDBKSTYSZ  LEFT JOIN ORDBKSTY ON ORDBKSTY.ORDBKSTY_ID =ORDBKSTYSZ.ORDBKSTY_ID 
				LEFT JOIN ORDBK ON ORDBK.ORDBK_KEY =ORDBKSTY.ORDBK_KEY  
				LEFT JOIN STYSIZE F ON F.STYSIZE_ID=ORDBKSTYSZ.STYSIZE_ID
				LEFT JOIN FGSIZE G ON F.FGSIZE_ID = G.FGSIZE_ID 
				WHERE ORDBKSTY.FGSTYLE_ID =B.FGSTYLE_ID AND  ORDBKSTY.FGSHADE_KEY =B.FGSHADE_KEY  AND ORDBK.COBR_ID='''+@CCOBR_ID+'''
				AND ORDBKSTYSZ.STYSIZE_ID=F.STYSIZE_ID AND ORDBK.STATUS = ''1'' AND ORDBKSTYSZ.BAL_QTY > 0) AS PENDING_ORD_QTY 
				FROM STYSTKDTL A JOIN STYSTK B ON A.STYSTK_KEY=B.STYSTK_KEY
				LEFT JOIN FGSTYLE C ON B.FGSTYLE_ID=C.FGSTYLE_ID 
				LEFT JOIN FGTYPE D ON B.FGTYPE_KEY=D.FGTYPE_KEY 
				LEFT JOIN FGSHADE E ON B.FGSHADE_KEY=E.FGSHADE_KEY 
				WHERE A.ALT_BARCODE=''' + @ALT_BARCODE + ''' AND  B.COBR_ID='''+@CCOBR_ID+''''
				print @CCMD
				EXECUTE SP_EXECUTESQL @CCMD  
		END

		

--END TRY                                          
--BEGIN CATCH                                          
--SET @CERR_MSG='ERROR:  PROCEDURE: ASP_SPMODULE  ERROR MESSAGE: '+RTRIM(LTRIM(ERROR_MESSAGE()))+'  ERROR LINE : '+RTRIM(LTRIM(ERROR_LINE()))                                          
--GOTO ENDREC                                  
--END CATCH                                          
                              
--ENDREC:                                          
--IF ISNULL(@CERR_MSG,'')<>''                                          
--ROLLBACK TRANSACTION                                          
--ELSE                                          
--COMMIT TRANSACTION    

END

GO---PRASYSTSQL---
IF OBJECT_ID('ASP_ORDER','P') IS NOT NULL
	DROP PROCEDURE ASP_ORDER
GO---PRASYSTSQL---
CREATE PROCEDURE ASP_ORDER 
(  
	 @ORDER_ID varchar(100)='',
	 @USER_ID BIGINT=0
)as  
  

BEGIN  
  
declare @cCMD Nvarchar(max)  

------------------------------INSERT ORDBK  

		print 'INSERT ORDBK  '
		set @cCMD='INSERT INTO ORDBK(FCYR_KEY,CO_ID,COBR_ID,ORDBK_NO,ORDBK_KEY,ORDBK_DT,PORD_REF, PORD_DT,PARTY_KEY,PARTYDTL_ID,BROKER_KEY,DLV_DT,REMK,STATUS,CREATED_BY,CREATED_DT,DISTBTR_KEY,SALEPERSON1_KEY,ROUND_OFF_DESC  
				,GST_APP,GST_TYPE,SHP_PARTY_KEY,SHP_PARTYDTL_ID,aprv1_flg,aprv1_user_id,aprv2_flg,aprv2_user_id,aprv3_flg,aprv3_user_id,COMMON_DLV_DT_FLG,STK_FLG,ORDBK_TYPE,ISWO,LOTWISE,online_ordbk)  
				select  a.FCYR_KEY, b.CO_ID,a.COBR_ID, a.ordbk_no,(a.FCYR_KEY+''''+a.COBR_ID+''''+a.ordbk_no) as ORDBK_KEY
				,convert(datetime,a.ORDBK_DT,100) AS ORDBK_DT,a.ORDBK_NO PORD_REF,convert(datetime,a.ORDBK_DT,100) PORD_DT
				,a.PARTY_KEY,a.PARTYDTL_ID,c.BROKER_KEY, convert(datetime,a.DLV_DT,100) as DLV_DT,a.REMK,a.STATUS,  
				 1 created_by,getdate() CREATED_Dt,ISNULL(e.distbtr_key,'''') DISTBTR_KEY,d.SALEPERSON_KEY SALEPERSON1_KEY,''ROUND_OFF'' ROUND_OFF_DESC  
				,''N'' GST_APP,''S'' GST_TYPE, a.party_key SHP_PARTY_KEY,a.partydtl_id  SHP_PARTYDTL_ID, 2 aprv1_flg,0 aprv1_user_id,2 aprv2_flg, 0 aprv2_user_id,
				2 aprv3_flg, 0 aprv3_user_id,''0'' COMMON_DLV_DT_FLG, ''0'' STK_FLG,ISNULL(a.ORDBK_TYPE,''0'') as  ORDBK_TYPE ,''0'' ISWO,''0'' LOTWISE  ,''S'' as online_ordbk
				from ordbkAsp a   
				left join COBR b on CAST(a.COBR_ID as varchar)=b.COBR_ID  
				left join BROKER c on CAST(a.BROKER_KEY as varchar)=c.BROKER_KEY  
				left join salePerson d on CAST(a.SALEPERSON1_KEY as varchar)=d.SALEPERSON_NAME  
				left join distbtr e on e.distbtr_name=a.CONSIGNEE 
				where  a.ordbk_key='''+ @ORDER_ID +''''  
		 PRINT @cCMD  
		EXECUTE SP_EXECUTESQL @cCMD  


--------------------------------INSERT ORDBKSTY  
  
		print 'INSERT ORDBKSTY  '
		set @cCMD='insert into ordbksty (ORDBKSTY_ID,ORDBK_KEY,FGPRD_KEY,FGSTYLE_ID,FGSTYLE_CODE,FGTYPE_KEY,FGSHADE_KEY,FGITM_KEY,QTY,DLV_DT,BAL_QTY,STATUS)  
		select (SELECT ISNULL(MAX(ORDBKSTY_ID),0)  from ordbksty)+ ROW_NUMBER() over(order by ordbk_key) as ORDBKSTY_ID  ,* from  
		(  
		select   a.ordbk_key,b.fgprd_key, a.fgstyle_id,b.fgstyle_code,b.fgtype_key,ISNULL(d.fgshade_key,'''') fgshade_key  
		, b.FGPRD_KEY+CAST(A.FGSTYLE_ID as varchar)+ISNULL(b.FGTYPE_KEY,'''')+ISNULL(d.fgshade_key,'''') FGITM_KEY,SUM(CAST(a.Qty as numeric(15,3)))  QTY
		, e.DLV_DT ,SUM(CAST(a.Qty as numeric(15,3))) BAL_QTY,''1'' STATUS  
		from ordbkAspdtl a  
		left join fgstyle b on b.fgstyle_id=a.fgstyle_id  
		left join fgtype c on b.fgtype_key=c.fgtype_key  
		left join fgshade d on d.FGSHADE_KEY=a.FGSHADE_KEY  
		join ordbkAsp e on e.ordbk_key=a.ordbk_key  
		where a.USER_ID=1 and a.ordbk_key='''+ @ORDER_ID +'''
		group by a.ordbk_key,b.fgprd_key, a.fgstyle_id,b.fgstyle_code,  
		b.fgtype_key,ISNULL(d.fgshade_key,'''') ,e.DLV_DT
		
		) a   '  
		 PRINT @cCMD  
		EXECUTE SP_EXECUTESQL @cCMD
------------------------------INSERT ORDBKSTYSZ  
  
		print 'INSERT ORDBKSZ  '
		set @cCMD='INSERT INTO ORDBKSTYSZ (ORDBKSTYSZ_ID,ORDBK_KEY,ORDBKSTY_ID,STYSIZE_ID,STYSIZE_NAME,QTY,BAL_QTY,MRP,WSP,ITM_AMT,DISC_AMT)  
		select (SELECT ISNULL(MAX(ORDBKSTYSZ_ID),0)  from ordbkstysz)+ ROW_NUMBER() over(order by a.ORDBKSTY_ID) as ORDBKSTYSZ_ID,a.ordbk_key,a.ordbksty_id,c.stysize_id,c.stysize_name,d.QTY,d.QTY
		,d.mrp,d.wsp ,CAST(d.AMOUNT as numeric(15,2)),0 DISC_AMT
		from ordbksty a  
		join stysize c on c.fgprd_key=a.fgprd_key and c.fgstyle_id=a.fgstyle_id   
		join ordbkAspdtl d on d.stysize_name=c.stysize_name and a.fgstyle_id=d.fgstyle_id and a.FGTYPE_KEY=d.FGTYPE_KEY and a.FGSHADE_KEY=d.FGSHADE_KEY
		left join ordbkstysz b on a.ordbksty_id=b.ordbksty_id where b.ordbksty_id is null and a.ordbk_key in (select ordbk_key from ordbkAspdtl where USER_ID=1 and ordbk_key='''+ @ORDER_ID +''')  
		'

		 PRINT @cCMD  
		EXECUTE SP_EXECUTESQL @cCMD


------------------------------------------------update null values with defaults  

	print 'Insert null Values  '
	set @cCMD='exec SPMODULE_UPDATE_NULL_WITH_DEFAULTS ''ordbkSTYsz'','' ordbk_key in (select ordbk_key from ordbkAsp WHERE ordbk_key='''''+CAST(@ORDER_ID as varchar)+''''')''' 
   
	PRINT @cCMD  
	EXECUTE SP_EXECUTESQL @cCMD  
	set @cCMD='exec SPMODULE_UPDATE_NULL_WITH_DEFAULTS ''ordbkSTY'','' ordbk_key in (select ordbk_key from ordbkAsp WHERE ordbk_key='''''+CAST(@ORDER_ID as varchar)+''''')''' 
  
	PRINT @cCMD  
	EXECUTE SP_EXECUTESQL @cCMD  
	set @cCMD='exec SPMODULE_UPDATE_NULL_WITH_DEFAULTS ''ordbk'','' ordbk_key in (select ordbk_key from ordbkAsp WHERE ordbk_key='''''+CAST(@ORDER_ID as varchar)+''''')'''  
	PRINT @cCMD  
	EXECUTE SP_EXECUTESQL @cCMD  
  
------------------------------------------update qty from ordbkstysz to ordbksty  
	print 'update qty from ordbkstysz to ordbksty  '
	set @cCMD='update a set a.qty=b.qty,a.bal_qty=b.bal_qty , a.Amt=b.ITM_AMT 
	FROM ORDBKSTY a  
	join (select ordbksty_id, SUM(qty) qty,SUM(bal_qty) bal_qty,SUM(ITM_AMT)  ITM_AMT from ordbkstysz group by ordbksty_id) b on a.ordbksty_id=b.ordbksty_id  
	where a.ordbk_key  in (select ordbk_key from ordbkAsp WHERE ordbk_key='''+CAST(@ORDER_ID as varchar)+''')  '
	PRINT @cCMD  
	EXECUTE SP_EXECUTESQL @cCMD 
-------------------MTY MISMATCHED

print 'if exists(select * from  
   (SELECT SUM(qty) qty,ordbk_key   
   from ordbksty   
    where ordbk_key  in (select ordbk_key from ordbkAsp WHERE ordbk_key='''+CAST(@ORDER_ID as varchar)+''')   
   group by ordbk_key) a  
  join   
  (select SUM(CAST(QTY as numeric(15,3))) qty ,ordbk_key   
  from ordbkAspdtl  
   where ordbk_key  in (select ordbk_key from ordbkAsp WHERE ordbk_key='''+CAST(@ORDER_ID as varchar)+''')   
  group by ordbk_key) b on a.ordbk_key=b.ordbk_key  
  where a.qty<>b.qty) '

 if exists(select * from  
   (SELECT SUM(qty) qty,ordbk_key   
   from ordbksty   
    where ordbk_key  in (select ordbk_key from ordbkAsp WHERE ordbk_key=@ORDER_ID)   
   group by ordbk_key) a  
  join   
  (select SUM(CAST(QTY as numeric(15,3))) qty ,ordbk_key   
  from ordbkAspdtl  
   where ordbk_key  in (select ordbk_key from ordbkAsp WHERE ordbk_key=@ORDER_ID)   
  group by ordbk_key) b on a.ordbk_key=b.ordbk_key  
  where a.qty<>b.qty)  
BEGIN  
 SELECT '' MSG,'QTY MISMATCHED' as error  
 return;  
end  
  
  --	EXEC UPDATE_PROC_ORD_STOCK @TYPE='ORD_BAL',@ordbk_key= @ORDER_ID 
	--IN STYSTKDTL
select ordbk_key MSG,'' ERROR from ordbkAsp WHERE ordbk_key=@ORDER_ID  
END

  GO---PRASYSTSQL---
if object_id('Fn_GETRate','fn') is not null
drop Function  dbo.Fn_GETRate
GO---PRASYSTSQL---

Create function dbo.Fn_GETRate(
@intFGstyle_id bigint,
 @strParty_key VARCHAR(10),
@intpartydtl_id bigint,
@g_ParamMarkDownSizeMRP char(1),
@Rate numeric(15,2)

)  

returns NUMERIC(15,2)  

as  

begin  

declare @cmd nvarchar(4000)


--SELECT   @rate=case when   @rate=0 then MRP else   @rate end ,
--	  @rate= case when   @rate=0 then SSP else   @rate end  FROM STYCATRT 
--WHERE FGSTYLE_ID = (SELECT FGSTYLE_ID FROM FGSTYLE WHERE FGSTYLE_ID=@intfgstyle_id  AND STATUS = '1')            
--AND CLIENTCAT_KEY = (SELECT CLIENTCAT_KEY FROM CLIENTTERMS WHERE PARTY_KEY =@strParty_key )
			
Declare @DISC numeric(15,2),@RDOFF int,@RateDiff numeric(15,2)
set @RDOFF=0
SET @DISC=0
		
select @DISC=CASE WHEN isnull(A.MAIN_BRANCH,'0')<>'1' AND A.trade_disc<>0 THEN  a.trade_disc ELSE C.TRADE_DISC END  ,
	@RDOFF=CASE WHEN isnull(A.MAIN_BRANCH,'0')<>'1' AND A.trade_disc<>0 THEN  a.RDOFF ELSE C.RDOFF END  
from partydtl a 
join PARTY b on a.party_key=b.PARTY_key 
join CLIENTTERMS c on b.party_key=c.party_key WHERE A.PARTYDTL_ID=@intpartydtl_id

if	@g_ParamMarkDownSizeMRP='2'
BEGIN

if	@DISC=0
begin

	--set @cmd= 'EXEC SPMODULE_BRANDWISE_MARKDOWN @cParty_key=''' +@strParty_key+''',	@nFGStyle_ID='+CAST(@intFGstyle_id as varchar)+',	@rate=' +CAST(@Rate as varchar) + ',@DISC=@DISC output '
	----print @cmd
	--EXECUTE sp_executesql  @cmd,N'@DISC numeric(15,2) output',@DISC=@DISC output; 
	----print @DISC
		SELECT @disc=(CASE WHEN C.MRP_RD>ISNULL(A.MARKDOWN_UPTO,0) THEN A.MARKDOWN1 ELSE A.MARKDOWN END) --AS MARKDOWN
		FROM PARTYCATBRANDDISC A
		JOIN CLIENTTERMS E ON E.CLIENTCAT_KEY=A.CLIENTCAT_KEY
		JOIN PARTY F ON F.PARTY_KEY=E.PARTY_KEY
		JOIN 
		(
		SELECT BRAND_KEY,MRP,(CASE WHEN @rate =0 THEN MRP ELSE @rate END) AS MRP_RD FROM FGSTYLE WHERE FGSTYLE_ID=@intFGSTYLE_ID

		)C ON C.BRAND_KEY=A.BRAND_KEY
		WHERE F.PARTY_KEY=@strParty_key

end

if @DISC=0
	Select @DISC=  Margin_MRP From ClientCat 
	Where ClientCat_Key = (SELECT ClientCat_Key FROM CLIENTTERMS WHERE PARTY_KEY = @strParty_key)

SET @RateDiff=  @rate * (@Disc / 100)

 If @RateDiff < 0 
   SET   @rate =   @rate - @RateDiff
Else
   SET   @rate =   @rate + @RateDiff


   Select @RDOFF= RdOff From FGPrd Where FGPrd_Key = (SELECT FGPRD_KEY FROM FGSTYLE where FGSTYLE_ID=@intfgstyle_id )
     SET   @rate = [dbo].RDOFF(  @rate, @RDOFF)

END	
else----getwsp from mrp
begin

if	@g_ParamMarkDownSizeMRP='0'
BEGIN
--print   @rate
		if	@DISC> 0
		BEGIN
	   set   @rate = Round(  @rate - (  @rate * (@DISC / 100)),2)
	   
                     SET   @rate =  [dbo].RDOFF(  @rate, @RDOFF)

	 END
	 
END
ELSE-----g_ParamMarkDownSizeMRP='1' 
BEGIN
		if	@DISC=0
		begin

			--set @cmd= 'EXEC SPMODULE_BRANDWISE_MARKDOWN @cParty_key=''' +@strParty_key+''',	@nFGStyle_ID='+CAST(@intFGstyle_id as varchar)+',	@rate=' +CAST(@Rate as varchar) + ',@DISC=@DISC output '
			----print @cmd
			--EXECUTE sp_executesql  @cmd,N'@DISC numeric(15,2) output',@DISC=@DISC output; 
			----print @DISC
				SELECT @disc=(CASE WHEN C.MRP_RD>ISNULL(A.MARKDOWN_UPTO,0) THEN A.MARKDOWN1 ELSE A.MARKDOWN END) --AS MARKDOWN
				FROM PARTYCATBRANDDISC A
				JOIN CLIENTTERMS E ON E.CLIENTCAT_KEY=A.CLIENTCAT_KEY
				JOIN PARTY F ON F.PARTY_KEY=E.PARTY_KEY
				JOIN 
				(
				SELECT BRAND_KEY,MRP,(CASE WHEN @rate =0 THEN MRP ELSE @rate END) AS MRP_RD FROM FGSTYLE WHERE FGSTYLE_ID=@intFGSTYLE_ID

				)C ON C.BRAND_KEY=A.BRAND_KEY
				WHERE F.PARTY_KEY=@strParty_key
		end
		
				if @DISC=0
			Select @DISC=  Margin_SSP From ClientCat 	Where ClientCat_Key = (SELECT ClientCat_Key FROM CLIENTTERMS WHERE PARTY_KEY = @strParty_key)

		if @DISC=0
			Select @DISC=FGMDW_RATE,@RDOFF=RdOff FROM FGPRD WHERE FGPRD_KEY=(SELECT FGPRD_KEY From FGSTYLE where FGSTYLE_ID=@intfgstyle_id)

		SET @RateDiff=  @rate * (@Disc / 100)

		 If @RateDiff < 0 
		   SET @rate = @rate + @RateDiff
		Else
		   SET @rate = @rate - @RateDiff

----print   @rate

		SET @rate =  [dbo].RDOFF(@rate, @RDOFF)

		END

end
return @Rate
end  

GO---PRASYSTSQL---
if object_id('GETRATE','P') is not null
drop procedure GETRATE
GO---PRASYSTSQL---
CREATE PROCEDURE GETRATE
(
@intFGstyle_id bigint=0,
@strParty_key VARCHAR(10)='',
@intpartydtl_id bigint=0,
@dblMRP numeric(15,2)  =0 output ,
@dblWsp numeric(15,2)  =0 output

)
AS
BEGIN
--

declare @accDbname nvarchar(50)
SET @accDbname=REPLACE( CAST(db_name() as nvarchar),'PGS','PGSACC')
--print @accDbname
declare @g_ParamMarkDownSizeMRP nchar(1)
declare @cmd nvarchar(4000)

set @cmd= N'SELECT @g_ParamMarkDownSizeMRP=REMARK  FROM '+@accDbname+'..USERPARAM where USERPM_ID=9'
EXECUTE sp_executesql  @cmd,N'@g_ParamMarkDownSizeMRP nchar(1) output',@g_ParamMarkDownSizeMRP=@g_ParamMarkDownSizeMRP output;  

		 print '@g_ParamMarkDownSizeMRP=' + @g_ParamMarkDownSizeMRP
         


SELECT @dblMRP=case when @dblMRP=0 and @g_ParamMarkDownSizeMRP='2' then dbo.Fn_GETRate( @intFGstyle_id,@strParty_key,@intpartydtl_id,@g_ParamMarkDownSizeMRP,(CASE @dblwsp when 0 then ssp else @dblwsp end ) )  
					else (CASE @dblMRP when 0 then MRP else @dblMRP end ) end ,
		@dblWsp= case when  @g_ParamMarkDownSizeMRP<>'2' then dbo.Fn_GETRate( @intFGstyle_id,@strParty_key,@intpartydtl_id,@g_ParamMarkDownSizeMRP,(CASE @dblMRP when 0 then MRP else @dblMRP end ))  
					else (CASE @dblwsp when 0 then ssp else @dblwsp end ) end  FROM STYCATRT 
WHERE FGSTYLE_ID = (SELECT FGSTYLE_ID FROM FGSTYLE WHERE FGSTYLE_ID=@intfgstyle_id  AND STATUS = '1')            
AND CLIENTCAT_KEY = (SELECT CLIENTCAT_KEY FROM CLIENTTERMS WHERE PARTY_KEY =@strParty_key )
			

END



GO---PRASYSTSQL---
if object_id('ASP_ORD_GETITMDTL','P') is not null
drop procedure ASP_ORD_GETITMDTL
GO---PRASYSTSQL---
CREATE PROCEDURE ASP_ORD_GETITMDTL
(

@NMODE      nvarchar(50)='',  
@PARTY_KEY varchar(10),
@PARTYDTL_ID bigint,
@COBR_ID varchar(2) ,
@FGSTYLE_ID BIGINT=0,
@FGSHADE_KEY VARCHAR(5)='',
@FGTYPE_KEY VARCHAR(5)='',
@FGPTN_KEY VARCHAR(5)='',
@ALT_BARCODE varchar(25)='',
@QTY bigint=0


)
AS
BEGIN
declare @accDbname nvarchar(50)
declare @cmd nvarchar(4000)
SET @accDbname=REPLACE( CAST(db_name() as nvarchar),'PGS','PGSACC')
declare @g_ParamMarkDownSizeMRP nchar(1)
set @cmd= N'SELECT @g_ParamMarkDownSizeMRP=REMARK  FROM '+@accDbname+'..USERPARAM where USERPM_ID=9'
EXECUTE sp_executesql  @cmd,N'@g_ParamMarkDownSizeMRP nchar(1) output',@g_ParamMarkDownSizeMRP=@g_ParamMarkDownSizeMRP output;  

		 print '@g_ParamMarkDownSizeMRP=' + @g_ParamMarkDownSizeMRP


--SELECT  '' SRNO,A.ALT_BARCODE,B.FGSTYLE_ID,FGSTYLE_CODE,C.FGTYPE_KEY,FGTYPE_NAME, E.FGSHADE_NAME,B.FGSHADE_KEY,A.STYSIZE_NAME
--				,ISNULL(A.CL_QTY ,0) AS QTY,
--				case when @g_ParamMarkDownSizeMRP='2' then dbo.Fn_GETRate( C.fgstyle_id,@PARTY_KEY,@PARTYDTL_ID,@g_ParamMarkDownSizeMRP,R.ssp+I.SSPRD) else R.MRP+I.MRPRD end  MRP,
--				case when (@g_ParamMarkDownSizeMRP='1' ) then dbo.Fn_GETRate(  C.fgstyle_id,@PARTY_KEY,@PARTYDTL_ID,@g_ParamMarkDownSizeMRP,R.MRP+I.MRPRD) else R.SSP+I.SSPRD end WSP,
--				round(((ISNULL(A.CL_QTY,0)+ISNULL(A.PROC_STK,0))-ISNULL(A.ORD_BAL,0)) ,2) AS PENDING_ORD_QTY 
--				FROM STYSTKDTL A 
--				JOIN STYSTK B ON A.STYSTK_KEY=B.STYSTK_KEY
--				LEFT JOIN FGSTYLE C ON B.FGSTYLE_ID=C.FGSTYLE_ID 
--				LEFT JOIN FGTYPE D ON B.FGTYPE_KEY=D.FGTYPE_KEY 
--				LEFT JOIN FGSHADE E ON B.FGSHADE_KEY=E.FGSHADE_KEY 
--				LEFT JOIN STYSIZE I ON A.STYSIZE_ID=I.STYSIZE_ID
--				LEFT JOIN FGSIZE J ON J.FGSIZE_ID = I.FGSIZE_ID 
--				left join (select * from STYCATRT t
--					WHERE t.CLIENTCAT_KEY = (SELECT CLIENTCAT_KEY FROM CLIENTTERMS WHERE PARTY_KEY =@PARTY_KEY )
--					) R on R.fgstyle_id=c.fgstyle_id
--				WHERE c.FGSTYLE_ID =@FGSTYLE_ID AND  B.FGSHADE_KEY =@FGSHADE_KEY AND  B.COBR_ID=@COBR_ID
--				Order by J.FGSIZE_ABRV
IF	len(@ALT_BARCODE)=0


SELECT  0 SRNO,ISNULL(STY.ALT_BARCODE,'') ALT_BARCODE,C.FGSTYLE_ID,FGSTYLE_CODE,C.FGTYPE_KEY,D.FGTYPE_NAME, ISNULL(E.FGSHADE_NAME,'') FGSHADE_NAME,ISNULL(E.FGSHADE_KEY,'') FGSHADE_KEY,I.STYSIZE_NAME
,@QTY AS QTY,ISNULL(case when @g_ParamMarkDownSizeMRP='2' then dbo.Fn_GETRate( C.fgstyle_id,@PARTY_KEY,@PARTYDTL_ID,@g_ParamMarkDownSizeMRP,R.ssp+I.SSPRD) else R.MRP+I.MRPRD end ,0) MRP,
ISNULL(case when (@g_ParamMarkDownSizeMRP='1' ) then dbo.Fn_GETRate(  C.fgstyle_id,@PARTY_KEY,@PARTYDTL_ID,@g_ParamMarkDownSizeMRP,R.MRP+I.MRPRD) else R.SSP+I.SSPRD end,0)WSP,
--round(((ISNULL(STY.CL_QTY,0)+ISNULL(STY.PROC_STK,0))-ISNULL(STY.ORD_BAL,0)) ,2) AS PENDING_ORD_QTY ,0.00 AMOUNT,@COBR_ID COBR_ID
(SELECT     ISNULL(SUM(dbo.STYSTKDTL.CL_QTY), 0) AS Expr1
FROM        dbo.STYSTKDTL LEFT OUTER JOIN
            dbo.STYSTK ON dbo.STYSTK.STYSTK_KEY = dbo.STYSTKDTL.STYSTK_KEY
WHERE      (dbo.STYSTKDTL.STYSIZE_ID = I.STYSIZE_ID)) +
(SELECT     ISNULL(SUM(dbo.PROISUSTYSZ.ISU_BAL_QTY), 0) AS Expr1
FROM        dbo.PROISUSTYSZ LEFT OUTER JOIN
            dbo.PROISUDTL ON dbo.PROISUDTL.PROISUDTL_ID = dbo.PROISUSTYSZ.PROISUDTL_ID LEFT OUTER JOIN
            dbo.PROISU ON dbo.PROISU.PROISU_KEY = dbo.PROISUDTL.PROISU_KEY
WHERE         (dbo.PROISU.COBR_ID = @COBR_ID) AND (dbo.PROISUSTYSZ.STYSIZE_ID = I.STYSIZE_ID) AND (dbo.PROISUDTL.FGSTYLE_ID = @FGSTYLE_ID) and (dbo.PROISUDTL.FGSHADE_KEY =@FGSHADE_KEY))
+
(SELECT     ISNULL(SUM(dbo.PRORECSTYSZ.BAL_QTY), 0) AS Expr1
FROM        dbo.PRORECSTYSZ LEFT OUTER JOIN
            dbo.PRORECDTL ON dbo.PRORECDTL.PRORECDTL_ID = dbo.PRORECSTYSZ.PRORECDTL_ID LEFT OUTER JOIN
            dbo.PROREC ON dbo.PROREC.PROREC_KEY = dbo.PRORECDTL.PROREC_KEY
WHERE        (dbo.PROREC.COBR_ID = @COBR_ID) AND (dbo.PRORECSTYSZ.STYSIZE_ID =I.STYSIZE_ID) AND (dbo.PRORECDTL.FGSTYLE_ID = @FGSTYLE_ID) and (dbo.PRORECDTL.FGSHADE_KEY =@FGSHADE_KEY)) 
-

(SELECT     ISNULL(SUM(dbo.ORDBKSTYSZ.BAL_QTY), 0) AS Expr1
FROM        dbo.ORDBKSTYSZ LEFT OUTER JOIN
            dbo.ORDBKSTY ON dbo.ORDBKSTY.ORDBKSTY_ID = dbo.ORDBKSTYSZ.ORDBKSTY_ID LEFT OUTER JOIN
            dbo.ORDBK ON dbo.ORDBK.ORDBK_KEY = dbo.ORDBKSTY.ORDBK_KEY
WHERE      (dbo.ORDBK.COBR_ID = @COBR_ID) AND (dbo.ORDBKSTYSZ.STYSIZE_ID = I.STYSIZE_ID) AND (dbo.ORDBK.STATUS = '1') AND 
            (dbo.ORDBKSTYSZ.BAL_QTY > 0) AND (dbo.ORDBK.ORDBK_TYPE <> '1') and (dbo.ORDBKSTY.FGSHADE_KEY =@FGSHADE_KEY)) 
AS PENDING_ORD_QTY,0.00 AMOUNT,@COBR_ID COBR_ID
FROM  FGSTYLE C ---ON B.FGSTYLE_ID=C.FGSTYLE_ID 
LEFT JOIN FGTYPE D ON c.FGTYPE_KEY=D.FGTYPE_KEY 
LEFT JOIN STYSIZE I ON C.FGSTYLE_ID=I.FGSTYLE_ID 
LEFT JOIN FGSIZE J ON J.FGSIZE_ID = I.FGSIZE_ID 
LEFT JOIN FGPRD K ON K.FGPRD_KEY = C.FGPRD_KEY 
left join (select * from STYCATRT t
	WHERE t.CLIENTCAT_KEY = (SELECT CLIENTCAT_KEY FROM CLIENTTERMS WHERE PARTY_KEY =@PARTY_KEY )
	) R on R.fgstyle_id=c.fgstyle_id
left JOIN   
		(  
		SELECT SUM(A.Cl_QTY) AS CL_QTY,SUM(A.PROC_STK) AS PROC_STK,SUM(A.ORD_BAL) AS ORD_BAL 
		,ST.FCYR_KEY,ST.COBR_ID,ST.FGSTYLE_ID,ST.FGSHADE_KEY,A.STYSIZE_ID,A.STYSIZE_NAME ,A.ALT_BARCODE 
		FROM STYSTKDTL A LEFT JOIN  STYSTK ST ON ST.STYSTK_KEY=A.STYSTK_KEY  
		WHERE  ST.FGSTYLE_ID=@FGSTYLE_ID AND  ST.FGSHADE_KEY =@FGSHADE_KEY and ST.COBR_ID=@COBR_ID  
		GROUP BY ST.FCYR_KEY,ST.COBR_ID,ST.FGSTYLE_ID,ST.FGSHADE_KEY,A.STYSIZE_ID,A.STYSIZE_NAME,A.ALT_BARCODE 
					   
		) STY ON  STY.STYSIZE_ID=I.STYSIZE_ID  
	LEFT JOIN FGSHADE E ON STY.FGSHADE_KEY=E.FGSHADE_KEY   
WHERE c.FGSTYLE_ID =@FGSTYLE_ID 
Order by J.FGSIZE_ABRV
ELSE
BEGIN

Declare @stystk_key varchAR(50),@stystkDTL_Id bigint
SET @stystk_key=''
SET @stystkDTL_Id=0

SELECT @stystkDTL_Id=A.STYSTKDTL_ID,@stystk_key=ST.STYSTK_KEY
FROM STYSTKDTL A 
LEFT JOIN  STYSTK ST ON ST.STYSTK_KEY=A.STYSTK_KEY  
WHERE   ST.COBR_ID=@COBR_ID and a.ALT_BARCODE=@ALT_BARCODE
		
		print @stystkDTL_Id
if @stystkDTL_Id=0
BEGIN
		SELECT @stystkDTL_Id=A.STYSTKDTL_ID,@stystk_key=ST.STYSTK_KEY
		FROM STYSTKDTL A 
		LEFT JOIN  STYSTK ST ON ST.STYSTK_KEY=A.STYSTK_KEY  
		WHERE   ST.COBR_ID=@COBR_ID and a.STYSTKDTL_KEY=@ALT_BARCODE
	
END 
print @stystkDTL_Id
if @stystkDTL_Id>0
BEGIN
SELECT  0 SRNO,ISNULL(STY.ALT_BARCODE,'') ALT_BARCODE,C.FGSTYLE_ID,FGSTYLE_CODE,C.FGTYPE_KEY,D.FGTYPE_NAME, E.FGSHADE_NAME,STY.FGSHADE_KEY,I.STYSIZE_NAME
,@QTY AS QTY,
ISNUll(case when @g_ParamMarkDownSizeMRP='2' then dbo.Fn_GETRate( C.fgstyle_id,@PARTY_KEY,@PARTYDTL_ID,@g_ParamMarkDownSizeMRP,R.ssp+I.SSPRD) else R.MRP+I.MRPRD end,0) MRP,
ISNUll(case when (@g_ParamMarkDownSizeMRP='1' ) then dbo.Fn_GETRate(  C.fgstyle_id,@PARTY_KEY,@PARTYDTL_ID,@g_ParamMarkDownSizeMRP,R.MRP+I.MRPRD) else R.SSP+I.SSPRD end,0) WSP,
--round(((ISNULL(STY.CL_QTY,0)+ISNULL(STY.PROC_STK,0))-ISNULL(STY.ORD_BAL,0)) ,2) AS PENDING_ORD_QTY ,0.00 AMOUNT,@COBR_ID COBR_ID
(SELECT     ISNULL(SUM(dbo.STYSTKDTL.CL_QTY), 0) AS Expr1
FROM        dbo.STYSTKDTL LEFT OUTER JOIN
            dbo.STYSTK ON dbo.STYSTK.STYSTK_KEY = dbo.STYSTKDTL.STYSTK_KEY
WHERE      (dbo.STYSTKDTL.STYSIZE_ID = I.STYSIZE_ID)) +
(SELECT     ISNULL(SUM(dbo.PROISUSTYSZ.ISU_BAL_QTY), 0) AS Expr1
FROM        dbo.PROISUSTYSZ LEFT OUTER JOIN
            dbo.PROISUDTL ON dbo.PROISUDTL.PROISUDTL_ID = dbo.PROISUSTYSZ.PROISUDTL_ID LEFT OUTER JOIN
            dbo.PROISU ON dbo.PROISU.PROISU_KEY = dbo.PROISUDTL.PROISU_KEY
WHERE         (dbo.PROISU.COBR_ID = @COBR_ID) AND (dbo.PROISUSTYSZ.STYSIZE_ID = I.STYSIZE_ID) AND (dbo.PROISUDTL.FGSTYLE_ID = C.FGSTYLE_ID) and (dbo.PROISUDTL.FGSHADE_KEY =STY.FGSHADE_KEY))
+
(SELECT     ISNULL(SUM(dbo.PRORECSTYSZ.BAL_QTY), 0) AS Expr1
FROM        dbo.PRORECSTYSZ LEFT OUTER JOIN
            dbo.PRORECDTL ON dbo.PRORECDTL.PRORECDTL_ID = dbo.PRORECSTYSZ.PRORECDTL_ID LEFT OUTER JOIN
            dbo.PROREC ON dbo.PROREC.PROREC_KEY = dbo.PRORECDTL.PROREC_KEY
WHERE        (dbo.PROREC.COBR_ID = @COBR_ID) AND (dbo.PRORECSTYSZ.STYSIZE_ID =I.STYSIZE_ID) AND (dbo.PRORECDTL.FGSTYLE_ID = C.FGSTYLE_ID) and (dbo.PRORECDTL.FGSHADE_KEY =STY.FGSHADE_KEY)) 
-

(SELECT     ISNULL(SUM(dbo.ORDBKSTYSZ.BAL_QTY), 0) AS Expr1
FROM        dbo.ORDBKSTYSZ LEFT OUTER JOIN
            dbo.ORDBKSTY ON dbo.ORDBKSTY.ORDBKSTY_ID = dbo.ORDBKSTYSZ.ORDBKSTY_ID LEFT OUTER JOIN
            dbo.ORDBK ON dbo.ORDBK.ORDBK_KEY = dbo.ORDBKSTY.ORDBK_KEY
WHERE      (dbo.ORDBK.COBR_ID = @COBR_ID) AND (dbo.ORDBKSTYSZ.STYSIZE_ID = I.STYSIZE_ID) AND (dbo.ORDBK.STATUS = '1') AND 
            (dbo.ORDBKSTYSZ.BAL_QTY > 0) AND (dbo.ORDBK.ORDBK_TYPE <> '1') and (dbo.ORDBKSTY.FGSHADE_KEY =STY.FGSHADE_KEY)) 
AS PENDING_ORD_QTY,0.00 AMOUNT,@COBR_ID COBR_ID
FROM  FGSTYLE C ---ON B.FGSTYLE_ID=C.FGSTYLE_ID 
LEFT JOIN FGTYPE D ON c.FGTYPE_KEY=D.FGTYPE_KEY 
LEFT JOIN STYSIZE I ON C.FGSTYLE_ID=I.FGSTYLE_ID 
LEFT JOIN FGSIZE J ON J.FGSIZE_ID = I.FGSIZE_ID 
LEFT JOIN FGPRD K ON K.FGPRD_KEY = C.FGPRD_KEY 
left join (select * from STYCATRT t
	WHERE t.CLIENTCAT_KEY = (SELECT CLIENTCAT_KEY FROM CLIENTTERMS WHERE PARTY_KEY =@PARTY_KEY )
	) R on R.fgstyle_id=c.fgstyle_id
 JOIN   
		(  
		SELECT SUM(A.Cl_QTY) AS CL_QTY,SUM(A.PROC_STK) AS PROC_STK,SUM(A.ORD_BAL) AS ORD_BAL 
		,ST.FCYR_KEY,ST.COBR_ID,ST.FGSTYLE_ID,ST.FGSHADE_KEY,A.STYSIZE_ID,A.STYSIZE_NAME ,A.ALT_BARCODE 
		FROM STYSTKDTL A LEFT JOIN  STYSTK ST ON ST.STYSTK_KEY=A.STYSTK_KEY  
		WHERE  ST.STYSTK_KEY=@stystk_key
		GROUP BY ST.FCYR_KEY,ST.COBR_ID,ST.FGSTYLE_ID,ST.FGSHADE_KEY,A.STYSIZE_ID,A.STYSIZE_NAME,A.ALT_BARCODE 
					   
		) STY ON  STY.STYSIZE_ID=I.STYSIZE_ID  
	LEFT JOIN FGSHADE E ON STY.FGSHADE_KEY=E.FGSHADE_KEY   
--WHERE c.FGSTYLE_ID =@FGSTYLE_ID 
Order by J.FGSIZE_ABRV
END
ELSE
BEGIN
	SELECT 'INVALID BARCODE' Error
END
END




END
GO---PRASYSTSQL---
IF OBJECT_ID('SPCOMMON_GETNEXTKEY','P') IS NOT NULL
	DROP PROCEDURE SPCOMMON_GETNEXTKEY
GO---PRASYSTSQL---   
CREATE PROCEDURE SPCOMMON_GETNEXTKEY        
(        
 @CMODULENAME  VARCHAR(4000),        
 @CTABLENAME   VARCHAR(4000) ,        
 @CCOLNAME   NVARCHAR(4000) ,        
 @NCOLLEN   NUMERIC(5),        
 @CSRCODE   VARCHAR(4000) OUTPUT        
 ,@CPREFIX NVARCHAR(5)=''        
 ,@CCOBR_ID  VARCHAR(5)=''  
  ,@Cfcyr_key  VARCHAR(2)=''        
      
)        
AS        
BEGIN        
 DECLARE @DTVALUE TABLE (CCOLVAL VARCHAR(4000))        
 DECLARE @CCMD NVARCHAR(4000)        
 SET @CSRCODE=''        
 IF ISNULL(@NCOLLEN,0)=0        
  SELECT @NCOLLEN=LENGTH FROM SYSCOLUMNS WHERE ID=OBJECT_ID(@CTABLENAME,'U') AND NAME =@CCOLNAME        
         
 SELECT @CSRCODE=SR_CODE FROM SERIES (NOLOCK) WHERE SR_FOR=@CMODULENAME --AND 1=2        
         
 SET @CPREFIX=(CASE WHEN ISNULL(@CPREFIX,'')='' THEN ISNULL(@CSRCODE,'') ELSE ISNULL(@CPREFIX,'') END)        
 SET @CSRCODE=@CPREFIX        
 DECLARE @CKEY NVARCHAR(20),@NACCESSKEYPREFIX INT        
 SET @NACCESSKEYPREFIX =10        
 IF ISNULL(@CSRCODE,'')<>''        
 BEGIN        
 --DECLARE @CKEY VARCHAR(20)         
 NEXTKEY:        
  DELETE FROM @DTVALUE        
  if exists (select * from INFORMATION_SCHEMA.COLUMNS where table_name=@CTABLENAME and column_name=@CCOLNAME and data_type='bigint')             
      SET @CCMD=N'SELECT @CKEY='''+@CSRCODE+'''+isnull(max('+@CCOLNAME+'),0) FROM '+ @CTABLENAME+ '(NOLOCK) WHERE LEFT('+@CCOLNAME+','+LTRIM(RTRIM(STR(LEN(@CSRCODE))))+')='''+@CSRCODE+'''           
           '+ (CASE WHEN ISNULL(@CCOBR_ID,'') ='' THEN '' ELSE '  AND  COBR_ID=''' + ISNULL(@CCOBR_ID,'') +'''' END)+
           + (CASE WHEN ISNULL(@Cfcyr_key,'') ='' THEN '' ELSE '  AND  fcyr_key=''' + ISNULL(@Cfcyr_key,'') +'''' END)+ '  PRINT @CKEY'                          
  else                
      SET @CCMD=N'SELECT @CKEY='''+@CSRCODE+'''+CONVERT(VARCHAR(100),MAX(CONVERT(NUMERIC,SUBSTRING('+@CCOLNAME+',LEN('''+@CSRCODE+''')+1,'+STR(@NCOLLEN)+')))) FROM           
      '+ @CTABLENAME+ '(NOLOCK) WHERE LEFT('+@CCOLNAME+','+LTRIM(RTRIM(STR(LEN(@CSRCODE))))+')='''+@CSRCODE+'''           
      '+ (CASE WHEN ISNULL(@CCOBR_ID,'') ='' THEN '' ELSE '  AND  COBR_ID=''' + ISNULL(@CCOBR_ID,'') +'''' END)+
       + (CASE WHEN ISNULL(@Cfcyr_key,'') ='' THEN '' ELSE '  AND  fcyr_key=''' + ISNULL(@Cfcyr_key,'') +'''' END)+ '  PRINT @CKEY'                      
                
          
  EXEC SP_EXECUTESQL  @CCMD ,N'@CKEY  VARCHAR(20) OUTPUT',@CKEY=@CKEY OUTPUT         
  PRINT ISNULL(@CKEY,'ROHIT')        
  INSERT @DTVALUE (CCOLVAL) VALUES(ISNULL(@CKEY,''))        
  SELECT @CSRCODE=@CSRCODE+(CASE WHEN CCOLVAL='' THEN '1' ELSE LTRIM(RTRIM(STR(CAST(SUBSTRING(CCOLVAL,LEN(@CSRCODE)+1,LEN(CCOLVAL)) AS NUMERIC(10))+1))) END) FROM @DTVALUE        
  IF LEN(@CSRCODE)<@NCOLLEN        
   SELECT @CSRCODE=@CPREFIX+REPLICATE('0',@NCOLLEN-LEN(@CSRCODE))+SUBSTRING(@CSRCODE,LEN(@CPREFIX)+1,LEN(@CSRCODE))        
  ELSE IF LEN(@CSRCODE)>@NCOLLEN        
  BEGIN        
   SET @CPREFIX=LTRIM(RTRIM(STR(@NACCESSKEYPREFIX)))        
   SET @CSRCODE=@CPREFIX        
   SET @NACCESSKEYPREFIX=@NACCESSKEYPREFIX+1        
   GOTO NEXTKEY        
  END        
 END        
 ELSE        
 BEGIN        
 --DECLARE @CKEY VARCHAR(20)         
 -- SET @CCMD=N'SELECT  @CKEY=CONVERT(VARCHAR(100),MAX('+@CCOLNAME+')) FROM '+ @CTABLENAME+ '(NOLOCK) WHERE ISNUMERIC('+@CCOLNAME+')=1  PRINT @CKEY'        
 -- PRINT @CCMD        
  if exists (select * from INFORMATION_SCHEMA.COLUMNS where table_name=@CTABLENAME and column_name=@CCOLNAME and data_type='bigint')             
      SET @CCMD=N'SELECT  @CKEY=CONVERT(VARCHAR(100),MAX('+@CCOLNAME+')) FROM '+ @CTABLENAME+ '(NOLOCK) '        
           + (CASE WHEN ISNULL(@CCOBR_ID,'') ='' THEN '' ELSE ' WHERE  COBR_ID=''' + ISNULL(@CCOBR_ID,'') +'''' END)                         
  else                
      SET @CCMD=N'SELECT  @CKEY=CONVERT(VARCHAR(100),MAX(CAST(ISNULL('+@CCOLNAME+',0) as BIGINT))) FROM '+ @CTABLENAME+ '(NOLOCK) WHERE ISNUMERIC('+@CCOLNAME+')=1 '        
      + (CASE WHEN ISNULL(@CCOBR_ID,'') ='' THEN '' ELSE '  AND  COBR_ID=''' + ISNULL(@CCOBR_ID,'') +'''' END)         
      --INSERT @DTVALUE (CCOLVAL)       
      EXEC SP_EXECUTESQL  @CCMD ,N'@CKEY  VARCHAR(20) OUTPUT',@CKEY=@CKEY OUTPUT       
      --print @CCMD      
     INSERT @DTVALUE (CCOLVAL) VALUES(ISNULL(@CKEY,0))       
        
     if exists (select * from INFORMATION_SCHEMA.COLUMNS where table_name=@CTABLENAME and column_name=@CCOLNAME and data_type='bigint')          
   SELECT @CSRCODE=LTRIM(RTRIM(STR(ISNULL(CCOLVAL ,0)+1))) FROM @DTVALUE       
      ELSE       
    SELECT @CSRCODE=REPLICATE('0',@NCOLLEN-LEN(LTRIM(RTRIM(STR(ISNULL(CCOLVAL ,0)+1)))))+LTRIM(RTRIM(STR(ISNULL(CCOLVAL ,0)+1))) FROM @DTVALUE        
 END        
 --SELECT CCOLVAL AS [LAST VALUE] FROM  @DTVALUE        
           
END        
GO---PRASYSTSQL---
IF OBJECT_ID('SP_GETIMAGEPATH','P') IS NOT NULL
	DROP PROCEDURE SP_GETIMAGEPATH
GO---PRASYSTSQL---  
CREATE PROCEDURE SP_GETIMAGEPATH    
 (    
 @intFGSTYLE_ID bigint,    
 @strFGSHADE_KEY VARCHAR(8)    
 )as    
 BEGIN    
 declare @imgPath varchar(50)    
 set @imgPath=''    
  select @imgPath=isnull(IMG_PATH,'') from STYSHADE where FGSTYLE_ID= @intFGSTYLE_ID  and fgshade_key=@strFGSHADE_KEY    
 if len(@imgPath)=0    
 begin    
 select @imgPath=ISNULL(STYLE_IMG,'')  from stydtl where FGSTYLE_ID= @intFGSTYLE_ID     
  select @imgPath  STYLE_PATH,'' SHDE_PATH    
 end    
  else    
  begin    
   select ''  STYLE_PATH,@imgPath SHDE_PATH    
      
  end    
END  
GO---PRASYSTSQL---

IF OBJECT_ID('SP_GETCATELOGUEINFO','P') IS NOT NULL
	DROP PROCEDURE SP_GETCATELOGUEINFO
GO---PRASYSTSQL---  
CREATE PROCEDURE SP_GETCATELOGUEINFO  
 (  
 @intStart_id bigint,  
 @intMaxcount bigint,
 @strProducts varchar(100),
 @strStyle varchar(100)=''
  )as  
 BEGIN 
 if exists(select * from tempdb.information_schema.tables where TABLE_NAME='##FGPRDTMP')
 drop table ##FGPRDTMP

 if	len(@strProducts)>0
 exec('select fgPRD_key into ##FGPRDTMP From FGPRD WHERE FGPRD_KEY IN ('+@strProducts+')')
 if	@intMaxcount>0 
 begin
if	len(@strStyle)>0
select  * from 
 (Select row_number() over (order by a.FGSTYLE_NAME) r, ISNULL(e.STYLE_IMG,'') as Image,a.FGSTYLE_ID, a.FGSTYLE_NAME,'' FGSHADE_NAME ,d.FGTYPE_NAME ,'' FGSHADE_KEY,a.FGTYPE_KEY 
  from FGSTYLE a 
   join fgtype d on a.fgtype_key=d.fgtype_key
  left join stydtl e on a.fgstyle_id=e.fgstyle_id
  where a.fgstyle_id<>0 and a.FGSTYLE_CODE like @strStyle+'%'
  ) x
 -- where x.r>((@intStart_id-1)*@intMaxcount) and x.r<=(@intStart_id*@intMaxcount)
    order by x.FGSTYLE_NAME 

else
  select  * from 
 (Select row_number() over (order by a.FGSTYLE_NAME) r, ISNULL(e.STYLE_IMG,'') as Image,a.FGSTYLE_ID, a.FGSTYLE_NAME,'' FGSHADE_NAME ,d.FGTYPE_NAME ,'' FGSHADE_KEY,a.FGTYPE_KEY 
  from FGSTYLE a 
   join fgtype d on a.fgtype_key=d.fgtype_key
  left join stydtl e on a.fgstyle_id=e.fgstyle_id
  where a.fgstyle_id<>0 and a.fgprd_key in (select * from ##FGPRDTMP)
  ) x
 -- where x.r>((@intStart_id-1)*@intMaxcount) and x.r<=(@intStart_id*@intMaxcount)
    order by x.FGSTYLE_NAME 
end
else
Select count(*)
  from FGSTYLE a  
  where a.fgstyle_id<>0 and  a.fgprd_key in (select * from ##FGPRDTMP)

END
GO---PRASYSTSQL---


IF OBJECT_ID('SP_GETAlloctedShade','P') IS NOT NULL
	DROP PROCEDURE SP_GETAlloctedShade
GO---PRASYSTSQL---  
CREATE PROCEDURE SP_GETAlloctedShade  
 (  
 @strfgstyleid varchar(500) ='',
 @mode varchar(10)=''
 )as  
 BEGIN 
 
 DECLARE @cmd nvarchar(1000)
 if @mode='' 
 set @cmd='select * from
 ( 
 select isnull(shade.IMG_PATH,isnull(e.STYLE_IMG,'''')) IMG,a.FGSTYLE_ID, a.FGSTYLE_NAME,isnull(c.FGSHADE_NAME,'''') FGSHADE_NAME,d.FGTYPE_NAME ,isnull(c.FGSHADE_KEY,'''') FGSHADE_KEY,a.FGTYPE_KEY  
	from fgstyle  a	
	left join 
	(select distinct isnull(ss.fgstyle_id,isnull(st.fgstyle_id,0)) fgstyle_id,isnull(ss.fgshade_key,isnull(st.fgshade_key,''''))  fgshade_key  ,ss.IMG_PATH
	from styshade   ss  
	full outer join stystk st on ss.fgstyle_id=st.fgstyle_id and ss.fgshade_key=st.fgshade_key  
	) shade on shade.fgstyle_id=a.fgstyle_id  
	left join fgshade c on shade.fgshade_key=c.FGSHADE_KEY
	left join fgtype d on a.fgtype_key=d.fgtype_key
	left join stydtl e on e.fgstyle_id=a.fgstyle_id

	
	) a ' +

	CASE WHEN LEN(@strfgstyleid)>0 THEN  'where a.fgstyle_id in ('+@strfgstyleid+')' ELSE '' END

	else if @mode='fgcat'
set @cmd='select * from
 ( 
 SELECT '''' FGCAT_KEY,''(Blank)'' FGCAT_NAME
 UNION
select FGCAT_KEY,FGCAT_NAME
FROM FGCAT WHERE STATUS=''1'' AND FGCAT_KEY<>''''
	
	) a ' 
	else if @mode='fgprd'
set @cmd='select * from
 ( 
 SELECT '''' FGPRD_KEY,''(Blank)'' FGPRD_NAME
 UNION
select FGPRD_KEY,FGPRD_NAME
FROM FGPRD WHERE ' + CASE WHEN LEN(@strfgstyleid)>0 THEN  ' FGCAT_key in ('+@strfgstyleid+')' ELSE '' END +
' and STATUS=''1'' AND FGPRD_KEY<>''''
	
	) a '
	



	print @cmd
	exec(@cmd)
 

END
GO---PRASYSTSQL---
GO---PRASYSTSQL---
if object_id('asp_catelog_getitmdtl','P') is not null
drop procedure asp_catelog_getitmdtl
GO---PRASYSTSQL---
CREATE PROCEDURE asp_catelog_getitmdtl    
(    
      
@PARTY_KEY varchar(10),    
@PARTYDTL_ID bigint,    
@COBR_ID varchar(2) ,    
@FGSTYLE_ID BIGINT=0,    
@FGSHADE_KEY VARCHAR(5)='',    
@FGTYPE_KEY VARCHAR(5)='',    
@FGPTN_KEY VARCHAR(5)='',    
 @QTY bigint=0    
    
    
)    
AS    
BEGIN    
declare @accDbname nvarchar(50)    
declare @cmd nvarchar(4000)    
SET @accDbname=REPLACE( CAST(db_name() as nvarchar),'PGS','PGSACC')    
declare @g_ParamMarkDownSizeMRP nchar(1)    
set @cmd= N'SELECT @g_ParamMarkDownSizeMRP=REMARK  FROM '+@accDbname+'..USERPARAM where USERPM_ID=9'    
EXECUTE sp_executesql  @cmd,N'@g_ParamMarkDownSizeMRP nchar(1) output',@g_ParamMarkDownSizeMRP=@g_ParamMarkDownSizeMRP output;      
    
   print '@g_ParamMarkDownSizeMRP=' + @g_ParamMarkDownSizeMRP     
    
SELECT  0 SRNO,ISNULL(STY.ALT_BARCODE,'') ALT_BARCODE,C.FGSTYLE_ID,FGSTYLE_CODE,C.FGTYPE_KEY,D.FGTYPE_NAME, ISNULL(E.FGSHADE_NAME,'') FGSHADE_NAME,ISNULL(E.FGSHADE_KEY,'') FGSHADE_KEY,I.STYSIZE_NAME    
,@QTY AS QTY,ISNULL(case when @g_ParamMarkDownSizeMRP='2' then dbo.Fn_GETRate( C.fgstyle_id,@PARTY_KEY,@PARTYDTL_ID,@g_ParamMarkDownSizeMRP,R.ssp+I.SSPRD) else R.MRP+I.MRPRD end ,0) MRP,    
ISNULL(case when (@g_ParamMarkDownSizeMRP='1' ) then dbo.Fn_GETRate(  C.fgstyle_id,@PARTY_KEY,@PARTYDTL_ID,@g_ParamMarkDownSizeMRP,R.MRP+I.MRPRD) else R.SSP+I.SSPRD end,0)WSP,    
I.STYSIZE_ID,    
--round(((ISNULL(STY.CL_QTY,0)+ISNULL(STY.PROC_STK,0))-ISNULL(STY.ORD_BAL,0)) ,2) AS PENDING_ORD_QTY ,0.00 AMOUNT,@COBR_ID COBR_ID    
(SELECT     ISNULL(SUM(dbo.STYSTKDTL.CL_QTY), 0) AS Expr1    
FROM        dbo.STYSTKDTL LEFT OUTER JOIN    
            dbo.STYSTK ON dbo.STYSTK.STYSTK_KEY = dbo.STYSTKDTL.STYSTK_KEY    
WHERE      (dbo.STYSTKDTL.STYSIZE_ID = I.STYSIZE_ID)) +    
(SELECT     ISNULL(SUM(dbo.PROISUSTYSZ.ISU_BAL_QTY), 0) AS Expr1    
FROM        dbo.PROISUSTYSZ LEFT OUTER JOIN    
            dbo.PROISUDTL ON dbo.PROISUDTL.PROISUDTL_ID = dbo.PROISUSTYSZ.PROISUDTL_ID LEFT OUTER JOIN    
            dbo.PROISU ON dbo.PROISU.PROISU_KEY = dbo.PROISUDTL.PROISU_KEY    
WHERE         (dbo.PROISU.COBR_ID = @COBR_ID) AND (dbo.PROISUSTYSZ.STYSIZE_ID = I.STYSIZE_ID) AND (dbo.PROISUDTL.FGSTYLE_ID = @FGSTYLE_ID) and (dbo.PROISUDTL.FGSHADE_KEY =@FGSHADE_KEY))    
+    
(SELECT     ISNULL(SUM(dbo.PRORECSTYSZ.BAL_QTY), 0) AS Expr1    
FROM        dbo.PRORECSTYSZ LEFT OUTER JOIN    
            dbo.PRORECDTL ON dbo.PRORECDTL.PRORECDTL_ID = dbo.PRORECSTYSZ.PRORECDTL_ID LEFT OUTER JOIN    
            dbo.PROREC ON dbo.PROREC.PROREC_KEY = dbo.PRORECDTL.PROREC_KEY    
WHERE        (dbo.PROREC.COBR_ID = @COBR_ID) AND (dbo.PRORECSTYSZ.STYSIZE_ID =I.STYSIZE_ID) AND (dbo.PRORECDTL.FGSTYLE_ID = @FGSTYLE_ID) and (dbo.PRORECDTL.FGSHADE_KEY =@FGSHADE_KEY))     
-    
    
(SELECT     ISNULL(SUM(dbo.ORDBKSTYSZ.BAL_QTY), 0) AS Expr1    
FROM        dbo.ORDBKSTYSZ LEFT OUTER JOIN    
            dbo.ORDBKSTY ON dbo.ORDBKSTY.ORDBKSTY_ID = dbo.ORDBKSTYSZ.ORDBKSTY_ID LEFT OUTER JOIN    
            dbo.ORDBK ON dbo.ORDBK.ORDBK_KEY = dbo.ORDBKSTY.ORDBK_KEY    
WHERE      (dbo.ORDBK.COBR_ID = @COBR_ID) AND (dbo.ORDBKSTYSZ.STYSIZE_ID = I.STYSIZE_ID) AND (dbo.ORDBK.STATUS = '1') AND     
            (dbo.ORDBKSTYSZ.BAL_QTY > 0) AND (dbo.ORDBK.ORDBK_TYPE <> '1') and (dbo.ORDBKSTY.FGSHADE_KEY =@FGSHADE_KEY))     
AS PENDING_ORD_QTY,0.00 AMOUNT,@COBR_ID COBR_ID    
FROM  FGSTYLE C ---ON B.FGSTYLE_ID=C.FGSTYLE_ID     
LEFT JOIN FGTYPE D ON c.FGTYPE_KEY=D.FGTYPE_KEY     
LEFT JOIN STYSIZE I ON C.FGSTYLE_ID=I.FGSTYLE_ID     
LEFT JOIN FGSIZE J ON J.FGSIZE_ID = I.FGSIZE_ID     
LEFT JOIN FGPRD K ON K.FGPRD_KEY = C.FGPRD_KEY     
left join (select * from STYCATRT t    
 WHERE t.CLIENTCAT_KEY = (SELECT CLIENTCAT_KEY FROM CLIENTTERMS WHERE PARTY_KEY =@PARTY_KEY )    
 ) R on R.fgstyle_id=c.fgstyle_id    
 left join 
 (select distinct isnull(ss.fgstyle_id,isnull(st.fgstyle_id,0)) fgstyle_id,isnull(ss.fgshade_key,isnull(st.fgshade_key,''))  fgshade_key  
	from styshade   ss  
	full outer join stystk st on ss.fgstyle_id=st.fgstyle_id and ss.fgshade_key=st.fgshade_key  
) shade on shade.fgstyle_id=c.fgstyle_id  
left JOIN       
  (      
  SELECT SUM(A.Cl_QTY) AS CL_QTY,SUM(A.PROC_STK) AS PROC_STK,SUM(A.ORD_BAL) AS ORD_BAL     
  ,ST.FCYR_KEY,ST.COBR_ID,ST.FGSTYLE_ID,ST.FGSHADE_KEY,A.STYSIZE_ID,A.STYSIZE_NAME ,A.ALT_BARCODE     
  FROM STYSTKDTL A LEFT JOIN  STYSTK ST ON ST.STYSTK_KEY=A.STYSTK_KEY      
  WHERE  ST.FGSTYLE_ID=@FGSTYLE_ID AND  ST.FGSHADE_KEY =@FGSHADE_KEY and ST.COBR_ID=@COBR_ID      
  GROUP BY ST.FCYR_KEY,ST.COBR_ID,ST.FGSTYLE_ID,ST.FGSHADE_KEY,A.STYSIZE_ID,A.STYSIZE_NAME,A.ALT_BARCODE     
            
  ) STY ON  STY.STYSIZE_ID=I.STYSIZE_ID      
 LEFT JOIN FGSHADE E ON shade.FGSHADE_KEY=E.FGSHADE_KEY       
WHERE c.FGSTYLE_ID =@FGSTYLE_ID  and isnull(shade.fgshade_key,'')  =  @FGSHADE_KEY  
Order by J.FGSIZE_ABRV    
    
    
END 
GO---PRASYSTSQL---
