﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data.SqlClient;
using System.Data;
//using DALC;
//using BusinessObject;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Collections;
using Microsoft.VisualBasic;
using System.Configuration;
using Common;
using System.Web.SessionState;
using System.Threading;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService
{
    
    public WebService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
        
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }


    [WebMethod]
    public ArrayList fgstyle_select(string strfgstyle )
    {
       
        DataTable worktable = new DataTable();
        ArrayList my_array = new ArrayList();


        String strquery = "EXEC ASP_SPMODULE  @NMODE='stylefillwebservices',@CWhere='" + strfgstyle + "'";
        worktable = modMain.NewDatatable(strquery, SessionManager.g_strConnString, null);

        if (worktable.Rows.Count != 0)
        {

            for (int i = 0; i < worktable.Rows.Count; i++)
            {
                my_array.Add((string)worktable.Rows[i]["FGSTYLE_NAME"]);
            }

        }


        return my_array;

    }
}

