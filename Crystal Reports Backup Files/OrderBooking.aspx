﻿<%@ Page Title="Order Booking" Language="C#" MasterPageFile="~/Site.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="OrderBooking.aspx.cs" Inherits="GPGSOLASPNET.OrderBooking" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


    <link href="Content/jquery-ui.css" rel="stylesheet" />
    <script src="Scripts/jquery-ui.js"></script>
      

    <link href="Content/DataTables/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <link href="Content/DataTables/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
    
     <script src="Scripts/JsonDataTables/jquery.tabletojson.min.js"></script>
    <%--<script src="Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>--%>
    <script src="Scripts/DataTables/jquery.dataTables.js" type="text/javascript"></script>
    
      <script src="Scripts/MultiCHK/bootstrap-multiselect.js" type="text/javascript"></script>
     <link href="Scripts/MultiCHK/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />

    <script src="Scripts/DataTables/dataTables.responsive.min.js" type="text/javascript">
    </script>
    

    <script type="text/javascript">

        $(document).ready(function () {
            $('#<%=GridviewSize.ClientID%>').on("keypress", function (event) {
               
                var keyPressed = event.keyCode || event.which;
                if (keyPressed === 13) {
                 
                    event.preventDefault();
                   
                    return false;
                }
            });
            $("#Cancel").focusout(function () {
                Changefocus();
            });

            function KeepSessionAlive() {
                // 1. Make request to server
                $.ajax({
                    type: "POST",
                    url: "Index.aspx/KeepAliveSession",
                    contentType: "application/json; charset=utf-8",
                    data: '',
                    dataType: "json",
                    success: function (result, status, xhr) {
                        //  alert(result.d)
                    },
                    error: function (req, status, error) {

                        alert(req + " " + status + " " + error);
                    }
                })


            };
            // 2. Schedule new request after 60000 miliseconds (1 minute)
            setInterval(KeepSessionAlive, 60000);
            // Initial call of function
            KeepSessionAlive();

          

            $('#<%=gddetails.ClientID%>').prepend($("<thead></thead>").append($(this).find("#<%=gddetails.ClientID%> tr:first")));
           <%-- $("#<%=gddetails.ClientID%>").find("tbody tr, thead tr")
                .children(":nth-child(3)")
                .attr("hidden", true);
            $("#<%=gddetails.ClientID%>").find("tbody tr, thead tr")
                .children(":nth-child(4)")
                .attr("hidden", true);--%>
          <%--  $("#<%=gddetails.ClientID%>").find("tbody tr, thead tr")
                .children(":nth-child(5)")
                .attr("hidden", true);
            $("#<%=gddetails.ClientID%>").find("tbody tr, thead tr")
                .children(":nth-child(6)")
                .attr("hidden", true);--%>
            // Select tab by name
            $('.nav-tabs a[href="#main"]').tab('show')

            $("#myModal").on('shown.bs.modal', function () {
                Changefocus();
            });

            var table = $('#<%=gddetails.ClientID%>').DataTable({
                "paging": false, "searching": true,
                "responsive": true,
                "language": {
                    "infoEmpty": "No data available in table"//Change your default empty table message
                },
                "columnDefs": [

                    { responsivePriority: 1, targets: 0 },
                    { responsivePriority: 2, targets: -1 },
                    { responsivePriority: 3, targets: -6     }
                ]

            });
            $('.nav-tabs a').click(function () {
                $("#MainContent_TabName").val($(this).attr("href").replace("#", ""))

                $(this).tab('show');

                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust()
                    .responsive.recalc();

            });


            $(function () {
                $("[id*=txtdlv_date]").datepicker({

                });
            });

           
          
            $('#<%=drpshade.ClientID%>').multiselect({
                maxHeight: 200,
                includeSelectAllOption: true,
                nonSelectedText: 'Select Shade',
                buttonWidth: '95%',
                inheritClass: true,
                 enableFiltering: true,
             //   buttonContainer: '<div class="btn-group" />'
            });
            
            $('#<%=chkmultishade.ClientID%>').change(function () {

                if ($(this).is(":checked")) {
                   // $("#divmultilst").show();
                    $("#<%=drpshade.ClientID%>").attr('multiple','multiple');
                   
                }
                else {
                  //  $("#divmultilst").hide();
                    $("#<%=drpshade.ClientID%>").removeAttr('multiple');
                    
                }
                $('#<%=drpshade.ClientID%>').multiselect('rebuild');
            });

            $('#<%=chkmultishade.ClientID%>').prop("checked", false).trigger("change");


            $('#<%=rdsetwise.ClientID%>').change(function () {
                $('#<%=txtset.ClientID%>').val('');
                $('#<%=txtConversion.ClientID%>').val('');
                if ($(this).is(":checked")) {
                    $("#divsetconver").show();
                    $("#divsetconver1").show();
                
                }
                else {
                    f
                    $("#divsetconver").hide();
                    $("#divsetconver1").hide();
                   
                }

            });

            $('#<%=rdnormal.ClientID%>').change(function () {
                if ($(this).is(":checked")) {

                    $('#<%=txtset.ClientID%>').val('');
                    $('#<%=txtConversion.ClientID%>').val('');
                    $("#divsetconver").hide();
                    $("#divsetconver1").hide();
                }

            });

            $('#<%=rdnormal.ClientID%>').prop("checked", true).trigger("change");


            $('#<%=rdstyle.ClientID%>').change(function () {
                //  alert("enable/disbled")
                if ($(this).is(":checked")) {
                    $("#<%=txtscan.ClientID%>").prop("disabled", true);
                    $("#<%=txtstyle.ClientID%>").removeAttr("disabled");
                    $("#<%=drpstyle.ClientID%>").removeAttr("disabled");
                    $("#<%=drptype.ClientID%>").removeAttr("disabled");
                    $("#<%=drpshade.ClientID%>").removeAttr("disabled");
                    $('#<%=drpshade.ClientID%>').multiselect('enable');
                }

            });

            $('#<%=rdstyle.ClientID%>').prop("checked", true).trigger("change");

            $('#<%=rdbarcode.ClientID%>').change(function () {
                // alert("enable/disbled")
                if ($(this).is(":checked")) {
                    $("#<%=txtstyle.ClientID%>").prop("disabled", true);
                    $("#<%=drpstyle.ClientID%>").prop("disabled", true);
                    $("#<%=drptype.ClientID%>").prop("disabled", true);
                    $("#<%=drpshade.ClientID%>").prop("disabled", true);
                    $('#<%=drpshade.ClientID%>').multiselect('disable');
                    $("#<%=txtscan.ClientID%>").removeAttr("disabled");

                }


            });

            $("#<%=txtscan.ClientID%>").on('keypress', function (e) {
                if (e.which == 13) {
                    e.preventDefault()

                    var strparty_key = $("#<%=drpparty.ClientID%>").val()
                    if (strparty_key == null) {
                        strparty_key = ""

                    }
                    if (strparty_key == "Select" || strparty_key == "") {
                        $("#MainContent_ctl12").css("visibility", "");
                        $("#<%=drpparty.ClientID%>").focus();
                        return;
                    }

                    var strpartydtl_id = $("#<%=drpplace.ClientID%>").val()
                    if (strpartydtl_id == null) {
                        strpartydtl_id = ""

                    }
                    if (strpartydtl_id == "Select" || strpartydtl_id == "0" || strpartydtl_id == "") {
                        $("#MainContent_ctl12").css("visibility", "");
                        $("#<%=drpplace.ClientID%>").focus();
                        return;
                    }




                    $("#SpanBarcode-loading-progress").show();
                    $.ajax({
                        type: "POST",
                        url: "OrderBooking.aspx/bind",
                        contentType: "application/json; charset=utf-8",
                        data: '{"StyleId":"' + $(this).val() + '","FgshadeId":"","Strtype":"B","strParty_key":"' + strparty_key + '","intPartydtl_id":"' + strpartydtl_id + '","SETQTY":0,"conversion":0}',
                        dataType: "json",
                        success: OnSuccessBarcode,
                        error: function (req, status, error) {
                            $("#SpanBarcode-loading-progress").hide();
                            alert(req + " " + status + " " + error);
                        }
                    })
                }
            });

            function OnSuccessBarcode(response) {
                // alert(response.d)
                var xmlDoc = $.parseXML(response.d);
                var xml = $(xmlDoc);
                var Sizes = xml.find("Size");
                if (Sizes.find("Error").first().text().length > 0) {
                    alert(Sizes.find("Error").first().text())

                } else {


                    $("#<%=drpstyle.ClientID%>")
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="' + Sizes.find("FGSTYLE_ID").first().text() + '">' + Sizes.find("FGSTYLE_CODE").first().text() + '</option>')
                        .val(Sizes.find("FGSTYLE_ID").first().text())
                    $("#<%=txtstyle.ClientID%>").val(Sizes.find("FGSTYLE_CODE").first().text());

                    $("#<%=drptype.ClientID%>")
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="' + Sizes.find("FGTYPE_KEY").first().text() + '">' + Sizes.find("FGTYPE_NAME").first().text() + '</option>')
                        .val(Sizes.find("FGTYPE_KEY").first().text())
                    $("#<%=drpshade.ClientID%>")
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="' + Sizes.find("FGSHADE_KEY").first().text() + '">' + Sizes.find("FGSHADE_NAME").first().text() + '</option>')
                        .val(Sizes.find("FGSHADE_KEY").first().text())
                    $('#<%=drpshade.ClientID%>').multiselect('rebuild');
                    fillimage($("#<%=drpstyle.ClientID%>").val(), $("#<%=drpshade.ClientID%>").val());

                    fillSizeGridView(Sizes)
                    $("#SpanBarcode-loading-progress").hide();
                };




            };

            function fillSizeGridView(Sizes) {

                var row1 = $("#<%=GridviewSize.ClientID%> tbody tr:first-child")
                $("th", row1).eq(4).attr("data-override", "PENDING_ORD_QTY")

                $("#<%=GridviewSize.ClientID%> tbody tr:last-child").show();
                var row = $("#<%=GridviewSize.ClientID%> tbody tr:last-child").clone(true);

                if (Sizes.length > 0) {

                    $("#<%=GridviewSize.ClientID%> tbody tr").not($("#<%=GridviewSize.ClientID%> tbody tr:first-child")).remove();
                    var i = 0;
                    $.each(Sizes, function () {
                        //   alert($(this).find("QTY").text())
                        $("td", row).eq(0).html($(this).find("STYSIZE_NAME").text());
                        $("td", row).eq(1).html($(this).find("MRP").text());
                        $("td", row).eq(2).html($(this).find("WSP").text());
                       
                        $('td:eq(3) input', row).val($(this).find("QTY").text());
                        // $('td:eq(3) input', row).val(0);
                        $('td:eq(3) input', row).attr('min', "0");
                        $('td:eq(3) input', row).attr('type', "number");
                        
                        $("td ", row).eq(4).html($(this).find("PENDING_ORD_QTY").text());

                        $("td", row).eq(5).html($(this).find("Fgstyle_id").text());
                        $("td", row).eq(6).html($(this).find("FGSTYLE_CODE").text());
                        $("td", row).eq(7).html($(this).find("FGTYPE_KEY").text());
                        $("td", row).eq(8).html($(this).find("FGSHADE_KEY").text());
                        $("td", row).eq(9).html($(this).find("Cobr_id").text());
                        $("td", row).eq(10).html($(this).find("ALT_BARCODE").text());
                        $("td", row).eq(11).html($(this).find("SRNO").text());
                        $("td", row).eq(12).html($(this).find("FGTYPE_NAME").text());
                        $("td", row).eq(13).html($(this).find("FGSHADE_NAME").text());

                        $("#<%=GridviewSize.ClientID%>").append(row);
                        row = $("#<%=GridviewSize.ClientID%> tbody tr:last-child").clone(true);
                      
                            i = i + $(this).find("QTY").text();
                        
                       
                    });
                    row = $("#<%=GridviewSize.ClientID%> tbody tr:nth-child(2)").focus();
                    $('td:eq(3) input', row).focus();
                    $('td:eq(3) input', row).select();
                    $("#dvsize").hide()
                } else {
                    $("#<%=GridviewSize.ClientID%> tbody tr:last-child").hide();
                    $("#dvsize").show()
                }

                if ($('#<%=rdbarcode.ClientID%>').is(":checked") && ! $("#<%=chkChangeqty.ClientID%>").is(":checked") && i>0) {
                    $("#<%=btnadddata.ClientID%>").trigger('click');
                }
            };

            $("#<%=txtstyle.ClientID%>").on('keypress', function (e) {
                if (e.keyCode == 13) {
                   
                 //   $('#<%=txtstyle.ClientID%>').trigger("focusout");
                    $('#<%=drptype.ClientID%>').focus(); 
            }
            });
            $("#<%=txtstyle.ClientID%>").focusout(function () {
              //  if (e.keyCode == 9) {
                  // e.preventDefault()
                    $("#dvsizedata").hide()
                    $('#<%=drpstyle.ClientID%>').val('');
                    $('#<%=drptype.ClientID%>').val('');
                    $('#<%=drpshade.ClientID%>').val('');
                    $('#<%=txtscan.ClientID%>').val('');
                    $("#<%=Image2.ClientID%>").removeAttr("src");
                    $("#<%=Image2.ClientID%>").attr("src", "Images/blankimg.png");
                    var row = $("#<%=GridviewSize.ClientID%> tbody tr:last-child").clone(true);
                    $("#<%=GridviewSize.ClientID%> tbody tr").not($("#<%=GridviewSize.ClientID%> tbody tr:first-child")).remove();
                    $("#<%=GridviewSize.ClientID%>").append(row);
                    $("#<%=GridviewSize.ClientID%> tbody tr:last-child").hide();
                    $("#dvsize").show()
                    //alert($(this).val());
                    $("#SpanType-loading-progress").show();
                    $.ajax({
                        type: "POST",
                        url: "OrderBooking.aspx/FILLSTYLEFILL",
                        contentType: "application/json; charset=utf-8",
                        data: '{"StyleId":"' + $(this).val() + '"}',
                        dataType: "json",
                        success: function (result, status, xhr) {
                            $("#<%=drpstyle.ClientID%>").html(result.d);
                            $("#SpanType-loading-progress").hide();
                            var txtcode = $("#<%=txtstyle.ClientID%>").val();
                            $('#<%=drpstyle.ClientID%>').trigger("change");
                            if (txtcode == $('#<%=drpstyle.ClientID%>').text())
                            {
                                $('#<%=drptype.ClientID%>').focus(); 

                            } else {
                                $('#<%=drpstyle.ClientID%>').focus(); 
                            }
                        },
                        error: function (xhr, status, error) {
                            alert("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
                            $("#SpanType-loading-progress").hide();
                        }
                    });
               // }
            });



            $("#<%=drpplace.ClientID%>").change(function () {
                $("#<%=txtbranch.ClientID%>").val('');
                $("#<%=txtconsignee.ClientID%>").val('');

               // if ($(this).val() > 0 && $(this).val() != null) {

               // alert($(this).val())
                   //  alert($("#<%=drpplace.ClientID%> option:selected").text())
                   // $("#<%=drpplace.ClientID%>").val($("#<%=drpplace.ClientID%> option:selected").text());
                $("#SpanType-loading-progress").show();
                $.ajax({
                    type: "POST",
                    url: "OrderBooking.aspx/FillAddrPartydtl",
                    contentType: "application/json; charset=utf-8",
                    data: '{"StrParty_key":"' + $(this).val() + '"}',
                    dataType: "json",
                    success: function (result, status, xhr) {

                        $("#<%=txtbranch.ClientID%>").val(result.d);
                        $("#SpanType-loading-progress").hide();
                    },
                    error: function (xhr, status, error) {
                        alert("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
                        $("#SpanType-loading-progress").hide();
                    }
                });

                $("#SpanType-loading-progress").show();
                $.ajax({
                    type: "POST",
                    url: "OrderBooking.aspx/FillConsiPartydtl",
                    contentType: "application/json; charset=utf-8",
                    data: '{"StrParty_key":"' + $(this).val() + '"}',
                    dataType: "json",
                    success: function (result, status, xhr) {

                        $("#<%=txtconsignee.ClientID%>").val(result.d);
                        $("#SpanType-loading-progress").hide();
                    },
                    error: function (xhr, status, error) {
                        alert("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
                        $("#SpanType-loading-progress").hide();
                    }
                });


            });


            $("#<%=drpparty.ClientID%>").change(function () {
                $("#<%=txtbranch.ClientID%>").val('');
                $("#<%=txtconsignee.ClientID%>").val('');
                $("#<%=drpplace.ClientID%>").val('');

                if ($(this).val() != 'Select' && $(this).val() != null) {
                  //  alert($(this).val())
               // alert($(this).val())
                   //  alert($("#<%=drpparty.ClientID%> option:selected").text())
                   // $("#<%=drpparty.ClientID%>").val($("#<%=drpparty.ClientID%> option:selected").text());

                    $("#SpanType-loading-progress").show();
                    $.ajax({
                        type: "POST",
                        url: "OrderBooking.aspx/Fillplace",
                        contentType: "application/json; charset=utf-8",
                        data: '{"StrParty_key":"' + $(this).val() + '"}',
                        dataType: "json",
                        success: function (result, status, xhr) {

                            $("#<%=drpplace.ClientID%>").html(result.d);
                            $("#SpanType-loading-progress").hide();
                        },
                        error: function (xhr, status, error) {
                            alert("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
                            $("#SpanType-loading-progress").hide();
                        }
                    });

                    $("#SpanType-loading-progress").show();
                    $.ajax({
                        type: "POST",
                        url: "OrderBooking.aspx/FillAddr",
                        contentType: "application/json; charset=utf-8",
                        data: '{"StrParty_key":"' + $(this).val() + '"}',
                        dataType: "json",
                        success: function (result, status, xhr) {

                            $("#<%=txtbranch.ClientID%>").val(result.d);
                            $("#SpanType-loading-progress").hide();
                        },
                        error: function (xhr, status, error) {
                            alert("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
                            $("#SpanType-loading-progress").hide();
                        }
                    });

                    $("#SpanType-loading-progress").show();
                    $.ajax({
                        type: "POST",
                        url: "OrderBooking.aspx/FillConsi",
                        contentType: "application/json; charset=utf-8",
                        data: '{"StrParty_key":"' + $(this).val() + '"}',
                        dataType: "json",
                        success: function (result, status, xhr) {

                            $("#<%=txtconsignee.ClientID%>").val(result.d);
                            $("#SpanType-loading-progress").hide();
                        },
                        error: function (xhr, status, error) {
                            alert("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
                            $("#SpanType-loading-progress").hide();
                        }
                    });


                }
            });

            $('#<%=drpparty.ClientID%>').trigger("change");
            $("#<%=drpstyle.ClientID%>").change(function () {

                if ($(this).val() > 0 && $(this).val() != null) {

                    // alert($("#<%=drpstyle.ClientID%> option:selected").text())
                    $("#<%=txtstyle.ClientID%>").val($("#<%=drpstyle.ClientID%> option:selected").text());
                    $("#SpanType-loading-progress").show();
                    $.ajax({
                        type: "POST",
                        url: "OrderBooking.aspx/FillTypeByStyleId",
                        contentType: "application/json; charset=utf-8",
                        data: '{"StyleId":"' + $(this).val() + '"}',
                        dataType: "json",
                        success: function (result, status, xhr) {
                            $("#<%=drptype.ClientID%>").html(result.d);
                            $("#SpanType-loading-progress").hide();
                        },
                        error: function (xhr, status, error) {
                            alert("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
                            $("#SpanType-loading-progress").hide();
                        }
                    });

                    if (lnkAllclicked) {
                       // fillShade($(this).val(), "all")
                    } else {
                        fillShade($(this).val(), "allocated")

                        var fgshadeimg = $("#<%=drpshade.ClientID%>").val()
                        if (fgshadeimg == null) {
                            fgshadeimg = ''
                        }
                       fillimage($(this).val(), fgshadeimg);
                    }
                   
                    var fgshadeimg = $("#<%=drpshade.ClientID%>").val()
                    if (fgshadeimg == null) {
                        fgshadeimg = ''
                    }
                    fillimage($(this).val(), fgshadeimg);
                }
            });
            var lnkAllclicked = false;
            $("#<%=lnkallocatedshade.ClientID%>").click(function () {
                lnkAllclicked = false;
                var style = $("#<%=drpstyle.ClientID%>").val()
                if (style == null) {
                    style = 0

                }
              
                fillShade(style, "allocated");


                var fgshadeimg = $("#<%=drpshade.ClientID%>").val()
                if (fgshadeimg == null) {
                    fgshadeimg = ''
                }
                fillimage($(this).val(), fgshadeimg);

            });

            $("#<%=lnkallshade.ClientID%>").click(function () {
                lnkAllclicked = true;

                  fillShade(0, "all");

                var fgshadeimg = $("#<%=drpshade.ClientID%>").val()
                if (fgshadeimg == null) {
                    fgshadeimg = ''
                }
                fillimage($(this).val(), fgshadeimg);
            });

            function fillShade(style_id, strflag) {
             //   alert(strflag)
                $("#SpanShade-loading-progress").show();
                $.ajax({
                    type: "POST",
                    url: "OrderBooking.aspx/FillShadeByStyleId",
                    contentType: "application/json; charset=utf-8",
                    data: '{"StyleId":"' + style_id + '","strflag":"'+ strflag+ '"}',
                    dataType: "json",
                    success: function (result, status, xhr) {
                        $("#<%=drpshade.ClientID%>").html(result.d);
                        $('#<%=drpshade.ClientID%>').multiselect('rebuild');
                        $('#<%=drpshade.ClientID%>').trigger("change");

                            $("#SpanShade-loading-progress").hide();
                        },
                        error: function (xhr, status, error) {
                            alert("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
                            $("#SpanShade-loading-progress").hide();
                        }
                    });

            };
            
            function fillimage(style_id, shadekey) {
                $("#SpanImage-loading-progress").show();

                $.ajax({
                    type: "POST",
                    url: "OrderBooking.aspx/FillImage",
                    contentType: "application/json; charset=utf-8",
                    data: '{"StyleId":"' + style_id + '","shadekey":"' + shadekey + '"}',
                    dataType: "json",
                    success: function (result, status, xhr) {
                        // alert(result.d.length)
                        if (result.d != null && result.d.length != 0) {

                            //   "~/StyleImg/{0}"
                            $("#<%=Image2.ClientID%>").attr("src", result.d);


                        } else {
                            $("#<%=Image2.ClientID%>").removeAttr("src");

                            $("#<%=Image2.ClientID%>").attr("src", "Images/blankimg.png");

                        }
                        $("#SpanImage-loading-progress").hide();
                    },
                    error: function (xhr, status, error) {
                        alert("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
                        $("#SpanImage-loading-progress").hide();
                    }
                });


            }
            
            $("#<%=drpshade.ClientID%>").change(function () {
                var fgshadeimg = $("#<%=drpshade.ClientID%>").val()
                if (fgshadeimg == null) {
                    fgshadeimg = ''
                }
                fillimage($("#<%=drpstyle.ClientID%>").val(), fgshadeimg);
               
            });

            $("#<%=btnstysize.ClientID%>").click(function (e) {
            //    alert('abc')

                e.preventDefault();

                var strparty_key = $("#<%=drpparty.ClientID%>").val()
                if (strparty_key == null) {
                    strparty_key = ""

                }
                if (strparty_key == "Select" || strparty_key == "") {
                    $("#MainContent_ctl12").css("visibility", "");
                    $("#<%=drpparty.ClientID%>").focus();
                    return;
                }

                var strpartydtl_id = $("#<%=drpplace.ClientID%>").val()
                if (strpartydtl_id == null) {
                    strpartydtl_id = ""

                }
                if (strpartydtl_id == "Select" || strpartydtl_id == "0" || strpartydtl_id == "") {
                    $("#MainContent_ctl12").css("visibility", "");
                    $("#<%=drpplace.ClientID%>").focus();
                    return;
                }



                var style = $("#<%=drpstyle.ClientID%>").val()
                if (style == null) {
                    style = ""

                }

                var type = $("#<%=drptype.ClientID%>").val()
                if (type == null) {
                    type = ""

                }

                var shde = $("#<%=drpshade.ClientID%>").val()
                if (shde == null) {
                    shde = ""

                }

                var conversion = $("#<%=txtConversion.ClientID%>").val()
                if (conversion == null) {
                    conversion = 0

                }
                if (conversion == "") {
                    conversion = 0

                }

                var SETQTY = $("#<%=txtset.ClientID%>").val()
                if (SETQTY == null) {
                    SETQTY = 0

                }

                if (SETQTY == "") {
                    SETQTY = 0

                }

                if (style == "Select" || style == "0" || style == "") {
                    $("#MainContent_ctl12").css("visibility", "");
                    $("#<%=drpstyle.ClientID%>").focus();
                    return;
                } else {
                    $("#MainContent_ctl12").css("visibility", "hidden");
                    $("#<%=btnstysize.ClientID%>").prop("disabled", true);
                    $.ajax({
                        type: "POST",
                        url: "OrderBooking.aspx/bind",
                        contentType: "application/json; charset=utf-8",
                        data: '{"StyleId":"' + style + '","FgshadeId":"' + shde + '","Strtype":"' + type + '","strParty_key":"' + strparty_key + '","intPartydtl_id":"' + strpartydtl_id + '","SETQTY":' + SETQTY + ',"conversion":' + conversion + '}',
                        dataType: "json",
                        success: OnSuccess,
                        error: function (req, status, error) {
                            alert(req + " " + status + " " + error);
                        }
                    })
                }
            });

            
            $("#<%=GridviewSize.ClientID%> tbody tr:last-child").hide();
            $("#<%=gddetails.ClientID%> tbody tr:last-child").hide();

            $("#dvsizedata").hide()

            function OnSuccess(response) {
                // alert(response.d)
                var xmlDoc = $.parseXML(response.d);
                var xml = $(xmlDoc);
                var Sizes = xml.find("Size");

                fillSizeGridView(Sizes)

                $("#<%=btnstysize.ClientID%>").prop("disabled", false);
            };



            $("#<%=btnadddata.ClientID%>").click(function (e) {
                // alert("äbc");
                e.preventDefault();
                $("#Spanconfirm-loading-progress").show();
                var style = $("#<%=drpstyle.ClientID%>").val()
                if (style == null) {
                    style = ""

                }
                if (style == "Select" || style == "0" || style == "") {
                    $("#MainContent_ctl12").css("visibility", "");
                    $("#<%=drpstyle.ClientID%>").focus();
                    return;
                } else {
                    $("#MainContent_ctl12").css("visibility", "hidden");
                }
                $.each($("#<%=GridviewSize.ClientID%> tbody tr"), function () {
                    //  alert($('td:eq(3) input', this).val())
                    $("td", $(this)).eq(3).attr("data-override", $('td:eq(3) input', this).val())
                });
                var table = $("#<%=GridviewSize.ClientID%>").tableToJSON();
                if (table.length == 0) {
                    alert("Size details not found!")
                    $("#<%=btnstysize.ClientID%>").focus();
                    return;
                }

              //  alert($("#<%=GridviewSize.ClientID%>").tableToJSON());
             //   alert($("#<%=drpstyle.ClientID%> option:selected").text());

                var stylecode = $("#<%=drpstyle.ClientID%> option:selected").text()
                if (stylecode == null) {
                    stylecode = ""

                }
                var type = $("#<%=drptype.ClientID%>").val()
                if (type == null) {
                    type = ""

                }

                var typename = $("#<%=drptype.ClientID%> option:selected").text()
                if (typename == null) {
                    typename = ""

                }
          
                var multishdename = '';
            
                $('#<%=drpshade.ClientID%> option:selected').each(function () {

                    if (multishdename == '') {
                        multishdename += $(this).val() + '-' + $(this).text();
                    }
                    else
                    {
                    multishdename += ',' + $(this).val() + '-' + $(this).text();
                    }
                });
                                   
              
                if (multishdename == null) {
                    multishdename = ""

                }

                

                var table = "'" + JSON.stringify(table) + "'"

                // alert(table)
                $.ajax({
                    type: "POST",
                    url: "OrderBooking.aspx/binddata",
                    contentType: "application/json; charset=utf-8",

                    data: '{"strdt":' + table + ',"strsrno":1,"fgstyle_id":' + style + ',"fgtype":"' + type + '","fgtypename":"' + typename + '","Fgstylecode":"' + stylecode + '","multishdename":"' + multishdename + '"}',
                    dataType: "json",
                    success: OnSuccessSize,
                    error: function (req, status, error) {
                        alert(req + " " + status + " " + error);
                        $("#Spanconfirm-loading-progress").hide();
                    }
                })
            });



         
            function OnSuccessSize(response) {
                // alert(response.d)
                var xmlDoc = $.parseXML(response.d);
                var xml = $(xmlDoc);
                var Sizes = xml.find("Size");
                $("#<%=gddetails.ClientID%> tbody tr:last-child").show();
                var row = $("#<%=gddetails.ClientID%>  tbody tr:last-child").clone(true);

                $("#<%=gddetails.ClientID%> tbody tr").not($("#<%=gddetails.ClientID%> tbody tr:first-child")).remove();

                table.clear().draw();

                if (Sizes.length > 0) {

                    var i = 0;
                    $.each(Sizes, function () {


                        $("td span", row).eq(0).html($(this).find("SRNO").text());
                        $("td span", row).eq(1).html($(this).find("FGSTYLE_CODE").text());

                        $("td span", row).eq(2).html($(this).find("FGTYPE_NAME").text());
                        $("td span", row).eq(3).html($(this).find("FGSHADE_NAME").text());
                        $("td span", row).eq(4).html($(this).find("STYSIZE_NAME").text());
                        $("td span", row).eq(5).html($(this).find("QTY").text());
                        $("td span", row).eq(6).html($(this).find("MRP").text());
                        $("td span", row).eq(7).html($(this).find("WSP").text());
                        $("td span", row).eq(8).html($(this).find("AMOUNT").text());
                        $("td span", row).eq(9).html($(this).find("PENDING_ORD_QTY").text());
                      


                        table.rows.add(row); // Add new data
                        table.columns.adjust().draw(); // Redraw the DataTable
                        row = $("#<%=gddetails.ClientID%> tbody tr:last-child").clone(true);

                        i = i + 1;

                    });
                   // $("#dvsizedata").hide()
                 //   $("#<%=gddetails.ClientID%> tbody tr:first-child").hide();
                    $("#dvsizedata").hide()
                    $('#<%=drpstyle.ClientID%>').val('');
                    $('#<%=drptype.ClientID%>').val('');
                    $('#<%=drpshade.ClientID%>').val('');
                    $('#<%=txtscan.ClientID%>').val('');
                    $("#<%=Image2.ClientID%>").removeAttr("src");
                    $("#<%=Image2.ClientID%>").attr("src", "Images/blankimg.png");
                    var row = $("#<%=GridviewSize.ClientID%> tbody tr:last-child").clone(true);
                    $("#<%=GridviewSize.ClientID%> tbody tr").not($("#<%=GridviewSize.ClientID%> tbody tr:first-child")).remove();
                    $("#<%=GridviewSize.ClientID%>").append(row);
                    $("#<%=GridviewSize.ClientID%> tbody tr:last-child").hide();
                    $('#<%=txtstyle.ClientID%>').val('');
                    $('#<%=txtset.ClientID%>').val('');
                    $('#<%=txtConversion.ClientID%>').val('');
                   

                 
                  
                    $('option', $('#<%=drpshade.ClientID%>')).each(function (element) {
                        $(this).removeAttr('selected').prop('selected', false);
                    });

                    $('#<%=drpshade.ClientID%>').multiselect('refresh');
                    $("#<%=drpshade.ClientID%>").removeAttr('multiple');
                    $('#<%=drpshade.ClientID%>').multiselect('rebuild');
                    $('#<%=chkmultishade.ClientID%>').prop("checked", false);
                    Changefocus();
                    $("#dvsize").show()

                } else {
                    $("#dvsize").show()
                }

                $("#Spanconfirm-loading-progress").hide();

            };
            
            function Changefocus() {

                if ($('#<%=rdstyle.ClientID%>').is(':checked'))
                {
                    $('#<%=txtstyle.ClientID%>').focus();
                    $('#<%=txtstyle.ClientID%>').select();
                }
                else {
                    $('#<%=txtscan.ClientID%>').focus();
                    $('#<%=txtscan.ClientID%>').select();
                }
            };

            //Edit event handler.
            $("[id*=gddetails] [id*=lnkedit]").click(function () {
                var row = $(this).closest("tbody tr");

                row.find(".txtqtyzise1").show();
                row.find(".txtqtyzise1").val(row.find(".SpanQty").text());

                row.find(".txtqtyzise1").focus();
                row.find(".SpanQty").hide();
                row.find(".Update").show();
                row.find(".Cancel").show();
                row.find(".Delete").hide();
                $(this).hide();
                return false;
            });

            //Delete event handler.
            $("[id*=gddetails] [id*=lnkdelete]").click(function () {
                if (confirm("Do you want to delete this row?")) {
                    var row = $(this).closest("tr");

                    row.find(".Update").hide();
                    row.find(".Cancel").hide();
                    var intsrno = row.find("[id*=lblsrno]").text();

                    $.ajax({
                        type: "POST",
                        url: "OrderBooking.aspx/DeleteSize",
                        data: '{Srno: ' + intsrno + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            row.remove();
                        },
                        error: function (xhr, status, error) {
                            alert("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)

                        }
                    });
                }

                return false;
            });



            //Cancel event handler.
            $("[id*=gddetails] [id*=lnkcancel]").click(function () {
                var row = $(this).closest("tr");

                row.find(".Edit").show();
                row.find(".Delete").show();
                row.find(".Update").hide();
                row.find(".txtqtyzise1").hide();
                row.find(".SpanQty").show();
                $(this).hide();
                return false;
            });

            //Update event handler.
            $("[id*=gddetails] [id*=lnkupdate]").click(function () {
                var row = $(this).closest("tr");
                var input = row.find(".txtqtyzise1");
                row.find(".Edit").show();
                row.find(".Delete").show();
                row.find(".Cancel").hide();
                $(this).hide();

                var intsrno = row.find("[id*=lblsrno]").text();

                $.ajax({
                    type: "POST",
                    url: "OrderBooking.aspx/UpdateCustomer",
                    data: '{Srno: ' + intsrno + ',Qty: ' + input.val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (input.val().length > 0) {
                            var span = row.find(".SpanQty");

                            span.html(input.val());
                            span.show();
                            input.hide();
                        }
                    },
                    error: function (xhr, status, error) {
                        alert("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)

                    }
                });

                return false;
            });

        });



    </script>


    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-primary" id="list-panel">
                <div class="panel-heading">
                    <h1 class="panel-title">Order Booking</h1>
                </div>

                <div class="panel-body">

                    <%--  <div class="panel panel-default" style="width: 95%; padding: 10px; margin: 10px">--%>
                    <div id="Tabs" role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li><a href="#main" aria-controls="main" role="tab" data-toggle="tab">Main  </a></li>
                            <li><a href="#detail" aria-controls="detail" role="tab" data-toggle="tab">Detail</a></li>

                        </ul>
                    </div>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="main">
                            <br />
                            <div class="form-group row">
                                <asp:Label runat="server" AssociatedControlID="lblorder_no" CssClass="control-label col-sm-1">Order No</asp:Label>
                                <div class="col-sm-11">
                                    <asp:TextBox runat="server" ID="lblorder_no" CssClass="form-control" ReadOnly="true" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="lblorder_no"
                                        CssClass="text-danger" ErrorMessage="The Order No field is required." />

                                </div>
                            </div>

                             <div class="form-group row">
                                <asp:Label runat="server" AssociatedControlID="drpordtype" CssClass="control-label col-sm-1">Order type</asp:Label>
                                <div class="col-sm-10">
                                    <asp:DropDownList ID="drpordtype" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="0">Only Sales</asp:ListItem>
                                        <asp:ListItem Value="1">Only Work-Order</asp:ListItem>
                                        <asp:ListItem Value="2">Sales And Work-Order</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="drpordtype"
                                        CssClass="text-danger" ErrorMessage="The Order Type field is required." />
                                    <br />
                                </div>
                            </div>

                            <div class="form-group row">
                                <asp:Label runat="server" AssociatedControlID="txtdlv_date" CssClass="control-label col-sm-1">Dlv Date</asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtdlv_date" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtdlv_date"
                                        CssClass="text-danger" ErrorMessage="The Dlv Date field is required." />
                                    <br />
                                </div>

                            </div>
                            <div class="form-group row">
                                <asp:Label runat="server" AssociatedControlID="drpparty" CssClass="control-label col-sm-1">Party</asp:Label>
                                <div class="col-sm-10">
                                    <%--  <asp:DropDownList ID="drpparty" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="drpparty_SelectedIndexChanged">
                                    </asp:DropDownList>--%>
                                    <asp:DropDownList ID="drpparty" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="drpparty"
                                        CssClass="text-danger" ErrorMessage="The Party field is required." />
                                    <br />
                                </div>
                            </div>
                            <div class="form-group row">
                                <asp:Label runat="server" AssociatedControlID="drpplace" CssClass="control-label col-sm-1">Place</asp:Label>
                                <div class="col-sm-10">
                                    <asp:DropDownList ID="drpplace" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="drpplace"
                                        CssClass="text-danger" ErrorMessage="The Place is required." />
                                    <br />

                                </div>
                            </div>

                            <div class="form-group row">
                                <asp:Label runat="server" AssociatedControlID="txtbranch" CssClass="control-label col-sm-1">Address</asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtbranch" runat="server" TextMode="MultiLine" CssClass="form-control" ReadOnly="True" Enabled="False"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtbranch"
                                        CssClass="text-danger" ErrorMessage="The Branch is required." />
                                    <br />

                                </div>
                            </div>
                            <div class="form-group row">
                                <asp:Label runat="server" AssociatedControlID="txtconsignee" CssClass="control-label col-sm-1">Consignee</asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtconsignee" runat="server" ReadOnly="True" CssClass="form-control" Enabled="False"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtconsignee"
                                        CssClass="text-danger" ErrorMessage="The Consignee is required." />
                                    <br />

                                </div>
                            </div>
                            <div class="form-group row">
                                <asp:Label runat="server" AssociatedControlID="txtremark" CssClass="control-label col-sm-1">Remark</asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtremark" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                    <asp:Label runat="server" ID="lblsrno" Visible="False"></asp:Label>
                                    <br />

                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="radio-inline">
                                    <asp:RadioButton ID="rdbarcode" runat="server" Text="Barcode" GroupName="type_bar_style" /></label>
                                <label class="radio-inline">
                                    <asp:RadioButton ID="rdstyle" runat="server" Text="Style" GroupName="type_bar_style" /></label>
                                <br />

                            </div>
                            <div class="form-group ">
                                <label class="radio-inline">
                                    <asp:RadioButton ID="rdnormal" runat="server" Text="Normal" GroupName="normal_setwise" /></label>
                                <label class="radio-inline">
                                    <asp:RadioButton ID="rdsetwise" runat="server" Text="setwise" GroupName="normal_setwise" /></label>

                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="detail">
                            <div class="row">
                                <div class="col-md-12 ">
                                    <br />
                                    <%-- <button type="button" id="btnaddord" class="btn btn-primary" data-toggle="modal" data-target="#myModal" data-backdrop="static">
                                        Add Order
                                    </button>--%>
                                    <div class="div-withBottomBorder">
                                        <button type="button" class="btn btn-default btn-md" data-toggle="modal" data-target="#myModal" id="btnCreateORDBKSTY"  tabindex=-1 data-backdrop="static">
                                            +  Add Item(s)</button>

                                    </div>
                                    <br />
                                </div>

                                <div class="col-md-12">
                                    <%--OnRowDeleting="OnRowDeleting" OnRowEditing="OnRowEditing"--%>
                                    <asp:GridView ID="gddetails" runat="server" DataKeyNames="Srno" CssClass="dt-responsive" Width="100%" CellPadding="5" CellSpacing="0"
                                        AutoGenerateColumns="False" Visible="true" EnableModelValidation="False" EnablePersistedSelection="True">
                                        <Columns>

                                            <asp:TemplateField HeaderText="SrNo" SortExpression="Srno">

                                                <ItemTemplate>

                                                    <asp:Label ID="lblsrno" runat="server" Text='<%# Bind("Srno") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Style" SortExpression="FGSTYLE_CODE">

                                                <ItemTemplate>
                                                    <asp:Label ID="lblFGSTYLE_CODE" runat="server" Text='<%# Bind("FGSTYLE_CODE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           

                                            <asp:TemplateField HeaderText="Type" SortExpression="FGTYPE_NAME">

                                                <ItemTemplate>
                                                    <asp:Label ID="lFGTYPE_NAME" runat="server" Text='<%# Bind("FGTYPE_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Shade" SortExpression="FGSHADE_NAME">

                                                <ItemTemplate>
                                                    <asp:Label ID="lFGSHADE_NAME" runat="server" Text='<%# Bind("FGSHADE_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <%--  <asp:TemplateField HeaderText="STYSIZE_ID" SortExpression="STYSIZE_ID" Visible="False">

                                                <ItemTemplate>
                                                    <asp:Label ID="STYSIZE_IDnn" runat="server" Text='<%# Bind("STYSIZE_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Size" SortExpression="STYSIZE_NAME">

                                                <ItemTemplate>
                                                    <asp:Label ID="STYSIZE_NAMEg" runat="server" Text='<%# Bind("STYSIZE_NAME") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="QTY" SortExpression="QTY">

                                                <ItemTemplate>
                                                    <asp:Label ID="lblPRODUCT_TYPQTYE" runat="server" CssClass="SpanQty" Text='<%# Bind("QTY") %>'></asp:Label>

                                                    <asp:TextBox Text='<%# Eval("QTY") %>' runat="server" Style="display: none; width: 100%" CssClass="txtqtyzise1" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="MRP" SortExpression="MRP">

                                                <ItemTemplate>
                                                    <asp:Label ID="mkkMRP" runat="server" Text='<%# Bind("MRP") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="WSP" SortExpression="WSP">

                                                <ItemTemplate>
                                                    <asp:Label ID="mkkWSP" runat="server" Text='<%# Bind("WSP") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Amount" SortExpression="AMOUNT">

                                                <ItemTemplate>
                                                    <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("AMOUNT") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="50px" />
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Avl. Qty" SortExpression="PENDING_ORD_QTY">

                                                <ItemTemplate>
                                                    <asp:Label ID="lblavlqtyde" runat="server" Text='<%# Bind("PENDING_ORD_QTY") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="50px" />
                                            </asp:TemplateField>



                                          
                                            <asp:TemplateField HeaderText="Action">

                                                <ItemTemplate>
                                                    <%-- <asp:linkbutton text="Edit" runat="server" CssClass="Cancel" Style="display: none" data-toggle="myModal" href="#EditModal" runat="server" cssclass="btn  icon-edit" xmlns:asp="#unknown" id="lnkedit" />--%>
                                                    <asp:LinkButton Text="Edit" runat="server" CssClass="RightBorder"  ID="lnkedit" />
                                                    <asp:LinkButton Text="Update" runat="server" CssClass="Update" Style="display: none; border-right-style: groove; padding-right: inherit;" ID="lnkupdate" />
                                                    <asp:LinkButton Text="Cancel" runat="server" CssClass="Cancel" Style="display: none" ID="lnkcancel" />
                                                    <asp:LinkButton Text="Delete" runat="server" CssClass="Delete" ID="lnkdelete" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>


                                    </asp:GridView>

                                    <div id="dvsizedata" style="width: 100%; align-content: center; border: solid; border-color: darkgrey; border-width: thin;"><span>No records has been added.</span></div>


                                </div>


                            </div>
                        </div>
                    </div>
                    <!-- The Modal -->

                    <div id="myModal" class="modal fade" role="dialog" aria-labelledby="Edit" aria-hidden="true">


                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">

                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Add Order Item(s)</h4>
                                </div>

                                <!-- Modal body -->
                                <div class="modal-body">


                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtscan" CssClass="form-control input-sm" runat="server" AutoPostBack="false" placeholder="barcode"></asp:TextBox>
                                            <span id="SpanBarcode-loading-progress" style="display: none;" class="please-wait">Please wait..</span>
                                        </div>
                                        <div class="col-md-8">
                                            <label class="checkbox-inline">
                                                <asp:CheckBox ID="chkChangeqty" runat="server" Text="Chg Qty" /></label>
                                            <label class="checkbox-inline">
                                                <asp:CheckBox ID="chkmultishade" runat="server" Text="Multi Shade" /></label>

                                        </div>


                                    </div>

                                    

                                    <div class="row">
                                        <div class="col-md-8">

                                            <div class="form-group row">
                                                <asp:Label runat="server" ID="lbldetails" Font-Bold="True" Font-Names="Arial" Font-Size="12pt" ForeColor="#990000"></asp:Label>

                                            </div>
                                            <div class="form-group row">
                                                <asp:Label runat="server" AssociatedControlID="drpstyle" CssClass="control-label col-sm-2">Style</asp:Label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox ID="txtstyle" CssClass="form-control input-sm" runat="server" AutoPostBack="false" placeholder="Style Code"></asp:TextBox>

                                                    <asp:DropDownList ID="drpstyle" runat="server" ValidationGroup="savestylesz" CssClass="form-control input-sm">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="drpstyle" CssClass="text-danger" ErrorMessage="The Style is required." />


                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <asp:Label runat="server" AssociatedControlID="drptype" CssClass="control-label col-sm-2">Type</asp:Label>
                                                <div class="col-sm-10">
                                                    <asp:DropDownList ID="drptype" runat="server" CssClass="form-control input-sm">
                                                    </asp:DropDownList>
                                                    <span id="SpanType-loading-progress" style="display: none;" class="please-wait">Please wait..</span>

                                                    <%-- <asp:RequiredFieldValidator runat="server" ControlToValidate="drptype" CssClass="text-danger" ErrorMessage="The Type is required." />--%>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <asp:Label runat="server" AssociatedControlID="drpshade" CssClass="control-label col-sm-2">Shade</asp:Label>
                                                <div class="col-sm-10">
                                                    <asp:DropDownList ID="drpshade" runat="server" CssClass="form-control input-sm">
                                                    </asp:DropDownList>
                                                    
                                                    <span id="SpanShade-loading-progress" style="display: none;" class="please-wait">Please wait..</span>
                                                    <%-- <asp:RequiredFieldValidator runat="server" ControlToValidate="drpshade" CssClass="text-danger" ErrorMessage="The Shade is required." />--%>
                                                    <asp:LinkButton ID="lnkallocatedshade" runat="server" CssClass="RightBorder" Text="Allocated"><span>Allocated</span></asp:LinkButton>
                                                     <asp:LinkButton ID="lnkallshade"  runat="server"  Style="padding: inherit;" Text="All">All</asp:LinkButton>
                                                    </div>
                                            </div>
                                          
                                            <div class="form-group row" id="divsetconver">

                                                <asp:Label runat="server" AssociatedControlID="txtset" CssClass="control-label col-sm-2">Set</asp:Label>

                                                <div class="col-sm-5">
                                                    <asp:TextBox ID="txtset" CssClass="form-control input-sm" runat="server" placeholder="Set"></asp:TextBox>
                                                </div>
                                                <div class="col-sm-5">
                                                    <asp:TextBox ID="txtConversion" CssClass="form-control input-sm" runat="server" placeholder="Conversion"></asp:TextBox>
                                                </div>
                                            </div>
                                           
                                          
                                        </div>

                                        <div class="col-md-4">
                                             <div class="form-group row">
                                            <asp:Image ID="Image2" runat="server" CssClass="media-object" Width="100%" ImageUrl="~/Images/blankimg.png" />
                                            <span id="SpanImage-loading-progress" style="display: none;" class="please-wait">Please wait..</span>
                                                 </div>
                                             
                                        </div>
                                    </div>


                                    <div>
                                        <%--   <asp:Button ID="btnsize" runat="server" Text="Add Size" CssClass="btn btn-primary"/>--%>
                                        <asp:Button ID="btnstysize" runat="server" ValidationGroup="savestylesz" Text="Add" CssClass="btn btn-primary" />
                                       
                                        <br />
                                    </div>

                                    <div id="StyleImgord">
                                        <asp:Label ID="lblORDER_ID" runat="server" Visible="False"></asp:Label>
                                        <%--<asp:Button ID="Imageadddata" Text="Add" runat="server" CssClass="btn btn-primary" OnClick="Imageadddata_Click" />
                                        <asp:ImageButton ID="Imageadddata" runat="server" Height="50px" ImageUrl="~/Images/trolly1.png" Width="50px" OnClick="Imageadddata_Click" BorderStyle="Solid" BorderWidth="1px" />--%>
                                    </div>


                                    <div class="form-group">

                                        <asp:GridView ID="GridviewSize" runat="server" CellPadding="5" CellSpacing="0" CssClass="dt-responsive"
                                            Width="100%" AutoGenerateColumns="false"
                                            ShowHeaderWhenEmpty="True" OnSelectedIndexChanging="GridviewSize_SelectedIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Size" SortExpression="STYSIZE_NAME" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center">

                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsize" runat="server" Text='<%# Bind("STYSIZE_NAME") %>' Enabled="false"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="80px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="MRP" SortExpression="MRP" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center">

                                                    <ItemTemplate>
                                                        <asp:Label ID="lblmrp" runat="server" Text='<%# Bind("MRP") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="50px" />
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Rate" SortExpression="WSP" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center">

                                                    <ItemTemplate>
                                                        <asp:Label ID="lblWSP" runat="server" Text='<%# Bind("WSP") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="50px" />
                                                </asp:TemplateField>
                                             
                                                  <asp:TemplateField HeaderText="Qty" SortExpression="Qty" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="right" HeaderStyle-HorizontalAlign="NotSet">

                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtqty" runat="server" Text='<%# Bind("Qty") %>' Enabled="true" Width="90%"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="50px" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Avl. Qty" SortExpression="PENDING_ORD_QTY" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center">

                                                    <ItemTemplate>
                                                        <asp:Label ID="lblavlqty" runat="server" Text='<%# Bind("PENDING_ORD_QTY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="50px" />
                                                </asp:TemplateField>
                                                
                                                

                                                <asp:TemplateField HeaderText="StyleId" SortExpression="Fgstyle_id" Visible="False">

                                                    <ItemTemplate>
                                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("Fgstyle_id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Style" SortExpression="FGSTYLE_CODE" Visible="False">

                                                    <ItemTemplate>
                                                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("FGSTYLE_CODE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Type" SortExpression="FGTYPE_KEY" Visible="False">

                                                    <ItemTemplate>
                                                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("FGTYPE_KEY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Shade" SortExpression="FGSHADE_KEY" Visible="False">

                                                    <ItemTemplate>
                                                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("FGSHADE_KEY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="TypeNAME" SortExpression="FGTYPE_NAME" Visible="False">

                                                    <ItemTemplate>
                                                        <asp:Label ID="lFGTYPE_NAME12" runat="server" Text='<%# Bind("FGTYPE_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ShadeNAME" SortExpression="FGSHADE_NAME" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lFGSHADE_NAME12" runat="server" Text='<%# Bind("FGSHADE_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Cobr_id" SortExpression="Cobr_id" Visible="False">

                                                    <ItemTemplate>
                                                        <asp:Label ID="Cobr_id12" runat="server" Text='<%# Bind("Cobr_id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ALT_BARCODE" SortExpression="ALT_BARCODE" Visible="False">

                                                    <ItemTemplate>
                                                        <asp:Label ID="lblALT_BARCODE" runat="server" Text='<%# Bind("ALT_BARCODE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SRNO" SortExpression="SRNO" Visible="False">

                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSRNOBARCODE" runat="server" Text='<%# Bind("SRNO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <FooterStyle BorderStyle="Solid" BorderWidth="1px" />
                                            <HeaderStyle BorderStyle="Solid" BorderWidth="1px" />
                                        </asp:GridView>
                                        <div id="dvsize" style="width: 100%; align-content: center; border: solid; border-color: darkgrey; border-width: thin;">
                                            <span>No size available</span>

                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:GridView runat="server" ID="gdsumm" Enabled="false" Visible="False">
                                        </asp:GridView>
                                    </div>


                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <span id="Spanconfirm-loading-progress" style="display: none;" class="please-wait">Please wait..</span>
                                        <asp:Button ID="btnadddata" runat="server" Text="Confirm" ValidationGroup="savestylesz" CssClass="btn btn-primary" />
                                        <button type="button" id="Cancel" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                    <asp:Button ID="btnSubmit" Text="Submit" runat="server" CssClass="btn btn-primary" ValidationGroup="savestyles" OnClick="btnSubmit_Click" />
                    &nbsp;<asp:Button ID="btnreset" Text="Reset" runat="server" CssClass="btn btn-primary" OnClick="btnReset_Click" />
                    <asp:HiddenField ID="TabName" runat="server" />
                </div>

                <asp:HyperLink runat="server" CssClass=" btn-link btn" ID="Loginpage" ViewStateMode="Disabled" NavigateUrl="~/Contact.aspx">Go Back</asp:HyperLink>
            </div>

        </div>
    </div>
    
</asp:Content>
