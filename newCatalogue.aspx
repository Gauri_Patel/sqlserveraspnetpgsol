﻿<%@ Page Title="Catalogue" Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="newCatalogue.aspx.cs" Inherits="GPGSOLASPNET.newCatalogue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<link href="Content/jquery-ui.css" rel="stylesheet" />
	<script src="Scripts/jquery-ui.js"></script>
	<link href="Content/DataTables/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
	<link href="Content/DataTables/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />


	<script src="Scripts/JsonDataTables/jquery.tabletojson.min.js"></script>
	<script src="Scripts/DataTables/jquery.dataTables.js" type="text/javascript"></script>
	<script src="Scripts/DataTables/dataTables.responsive.min.js" type="text/javascript"></script>
	<script src="Scripts/MultiCHK/bootstrap-multiselect.js" type="text/javascript"></script>
	<link href="Scripts/MultiCHK/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript">
        
        $(document).on({
            ajaxStart: function () {
                $("body").addClass("loading");
            },
            ajaxStop: function () {
                $("body").removeClass("loading");
            }
        });
        

		$(document).ready(function () {
			var arrselsty = [];
			var toatalitmCount = 0
			var datatableVariable = null
			var datatablesize = null



			$('#SelCat').multiselect({
				maxHeight: 300,
				includeSelectAllOption: true,
				nonSelectedText: 'Select Category',
				multiple: 'multiple',
				inheritClass: true,
				enableFiltering: true,
				//   buttonContainer: '<div class="btn-group" />'
				onDropdownHidden: fillproducts,
				delimiterText: ',',
				includeResetOption: true,
				includeResetDivider: true,
				buttonWidth: '30%',
				resetText: 'Clear',
				enableCaseInsensitiveFiltering: true,


			});
			$('#SelProd').multiselect({
				maxHeight: 200,
				includeSelectAllOption: true,
				nonSelectedText: 'Select product',
				multiple: 'multiple',
				inheritClass: true,
				enableFiltering: true,
				buttonWidth: '30%',
				enableCaseInsensitiveFiltering: true,

				//   buttonContainer: '<div class="btn-group" />'
				// onDropdownHide: fillproducts
				onDropdownHidden: function (event) {
					LoadData(event, 1)
				},



			});

			$.ajax({
				type: "POST",
				url: "newCatalogue.aspx/Fillcategories",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function (result, status, xhr) {
					var categories = JSON.parse(result.d)


					if (categories.length > 0) {
						$('#SelCat').find('option').remove();
						$.each(categories, function () {
							$('#SelCat').find('option')

								.end()
								.append('<option value="' + this.FGCAT_KEY + '">' + this.FGCAT_NAME + '</option>')
								.val('')
						});
					}
					$('#SelCat').multiselect('rebuild');
					//  $('#SelCat').trigger("change");

					fillproducts()
				}
				,
				error: function (xhr, status, error) {
					alert("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)

				}
			});
			function fillproducts() {
                if ($('#SelCat option:selected').length>5) {
					alert("Maximum 5 categories are allowed to select")
					return;
                }
				var multishdename = "''''"
				$('#SelCat option:selected').each(function () {

					if (multishdename == '') {
						multishdename += "''" + $(this).val() + "''";
					}
					else {
						multishdename += ",''" + $(this).val() + "''";
					}
				});


				if (multishdename == null) {
					multishdename = "''''"

				}
				var data = {};

				data.categories = multishdename;

				//   alert(JSON.stringify(data));
				$.ajax({
					type: "POST",
					url: "newCatalogue.aspx/FillProducts",
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					data: JSON.stringify(data),
					success: function (result, status, xhr) {
						var categories = JSON.parse(result.d)


						if (categories.length > 0) {
							$('#SelProd').find('option').remove();
							$.each(categories, function () {
								$('#SelProd').find('option')

									.end()
									.append('<option value="' + this.FGPRD_KEY + '">' + this.FGPRD_NAME + '</option>')
									.val('')
							});
						}
						$('#SelProd').multiselect('rebuild');
						//  $('#SelCat').trigger("change");
						$(function () {
							$("#<%=btnload.ClientID%>").click();
						});

					},
					error: function (xhr, status, error) {
						alert("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)

					}
				});



			};

			$("#<%=btnload.ClientID%>").click(function (e) {
				e.preventDefault();
				LoadData(e, 1);
			});

			function LoadData(e, intpage) {

				e.preventDefault();
				var multiProduct = "''''"
				$('#SelProd option:selected').each(function () {

					if (multiProduct == '') {
						multiProduct += "''" + $(this).val() + "''";
					}
					else {
						multiProduct += ",''" + $(this).val() + "''";
					}
				});


				if (multiProduct == null) {
					multiProduct = "''''"

				}
				var data = {};

				data.products = multiProduct;
				data.intpage = intpage;
				data.strStyle = $("#myInput").val();
				$.ajax({
					type: "POST",
					url: "newCatalogue.aspx/retrivestyles",
					data: JSON.stringify(data),
					contentType: "application/json; charset=utf-8",


					dataType: "json",
					success: FillStyleswithImg,
					error: function (req, status, error) {
						alert(req + " " + status + " " + error);
					}
				})

			};
			$('a[class="page-link"]').click(
				function (e) {

                    
					if ($(this).html() === "Previous") {
                        var prev = $("#pg li[class='page-item active']").prev();
                        var intprev = prev.children("a").html();
						if (intprev >= 1) {
							
							$("#pg li").attr("class", "page-item");
														
                            prev.attr("class", "page-item active");
							LoadData(e, (intprev));
						}
					}
					else if ($(this).html() === "Next") {
                        var nex = $("#pg li[class='page-item active']").next();
                        var intnext = nex.children("a").html();;
						// alert((intprev+1));
                        if (($("#pg > li:nth-child(" + intnext + ") a").html() != "Next")) {
							$("#pg li").attr("class", "page-item");

                            nex.attr("class", "page-item active");
                            LoadData(e, (intnext));
						}

					}
					else {
						$("#pg li").attr("class", "page-item");


						$(this).parent().addClass("active");
						LoadData(e, $(this).html());
					}


				});
			$("#itm3").hide();

			function FillStyleswithImg(response) {
				// alert(response.d)

				var Sizes = $(JSON.parse(response.d).d)


				if (Sizes.length > 0) {


					i = 0;
					$("#itm3").show();
					var itm = $("#Products > div:nth-child(1) ").clone(true);
					$("#Products").empty();
					$('#lblTotCount').html(' Total Count :' +Sizes.length);
					$.each(Sizes, function () {
						if (toatalitmCount == 0) {
							toatalitmCount = JSON.parse(response.d).totalcount;

							
							var i = 2
							for (var j = 0; j < (toatalitmCount); j++) {
								var pageli = $("li[class='page-item']").eq(0);
								//  alert(pageli)
								var newli = pageli.clone(true);
								$('a[class="page-link"]', newli).html(i + j)


								$(".pagination > li:nth-child(" + (i + j) + ")").after(newli);
								$(".pagination > li:nth-child(2)").addClass("page-item active");
							}
						}

						var cln = itm.clone(true);

						var stritm = ("STYLE" + this.FGSTYLE_NAME + this.FGTYPE_NAME + this.FGSHADE_NAME).replace(" ", "")
						// alert(stritm);
						cln.attr("Name", stritm)
						cln.attr("id", stritm)
						$("label", cln).html(this.FGSTYLE_NAME + " " + this.FGTYPE_NAME + " " + this.FGSHADE_NAME)
						$("input[Type='checkbox']", cln).attr("id", "chk" + this.FGSTYLE_ID)
						if ($.inArray(this.FGSTYLE_ID, arrselsty) > -1) {
							$("input[Type='checkbox']", cln).prop("checked", true)
						}
						else {
							$("input[Type='checkbox']", cln).prop("checked", false)
						}
						if (this.Image != null && this.Image.length != 0) {

							//   "~/StyleImg/{0}"
							$("img", cln).attr("src", "StyleImg/Images/StyleImg/" + this.Image);


						} else {
							$("img", cln).removeAttr("src");

							$("img", cln).attr("src", "Images/blankimg.png");

						}

						//  $("input[Type='Button']", cln).attr("id", "Buttonstyle" + this.FGSTYLE_ID)
						$("input[Type='hidden'][id*=Type]", cln).attr("Value", this.FGTYPE_KEY)
						$("input[Type='hidden'][id*=Shade]", cln).attr("Value", this.FGSHADE_KEY)
						$("input[Type='hidden'][id*=Style]", cln).attr("Value", this.FGSTYLE_ID)

						$("#Products").append(cln);

						i = i + 1;
					});
					$("#itm3").hide();
				} else {
                    $("#Products").slice(1).remove();
                   
					//$("#itm3").show();
				}



			};

			$("input[Type='checkbox']").change(function () {
				// alert(arrselsty)
				var parent = $(this).parent();

				var style_code = parent.find("input[Type='hidden'][id*=Style]").val()
				if (this.checked) {


					arrselsty.push(style_code);

				}
				else {
					// $.inArray(style_code, arrselsty)
					arrselsty = $.grep(arrselsty, function (n) {
						return n != style_code;
					});

				}
				// alert(arrselsty)
			});

			//$("#Close").click(function (e) {
			//    $('#ModalSize').modal.visible = false;
			//});

			$("input[id*=ButtonSTYLE]").click(function (e) {
				e.preventDefault()
				if (arrselsty.length == 0) {
					alert("select atleast one record")
					return;
				}
				var parent = $(this).parent();


				$('#ModalSize').modal('hide');

				//   alert('{"StyleId":"' + arrselsty + '"}')

				$("h4.modal-title").html('Select Color')

				$.ajax({
					type: "POST",
					dataType: "json",
					url: "newCatalogue.aspx/bindShadeData",
					contentType: "application/json; charset=utf-8",
					data: '{"StyleId":"' + arrselsty + '"}',
					error: function (xhr, error, code) {
						alert("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
					},
					success: function (data) {
						//  alert(data.d)
						$('#divSize').css('display', 'none');
						$('#divShade').css('display', 'block');
						if (datatableVariable === null) {
							datatableVariable = $('#GridviewShade').DataTable({
								"paging": false,
								"ordering": false,
								"info": false,
								"destroy": true,
								"searching": false,
								"responsive": true,
								"language": {
									"infoEmpty": "No data available in table"//Change your default empty table message
								},
								"columnDefs": [
									{ responsivePriority: 1, targets: 1 },
									{ responsivePriority: 3, targets: 3 }
								],
								data: JSON.parse(data.d),
								columns: [
									{
										"data": 'IMG',
										"name": 'IMG',
										"title": 'Item',
										"render": function (data) {
											var imgpath = ""
											if (data != null && data.length != 0) {
												imgpath = "StyleImg/Images/StyleImg/" + data;
											} else {
												imgpath = "Images/blankimg.png";
											}
											return '<img src="' + imgpath + '"  class="ml-3 mt-3" style="width:60px;height:60px">';
										}
									},
									{
										'data': 'chk',
										"name": "chk",
										"title": "Sel",
										"visible": true,
										"searchable": false,

										"render": function (data, type, row) {
											if (type === 'display') {
												return '<input type="checkbox" class="editor-active">';
											}
											return data;
										},
										//  "className": "dt-body-center"
									},
									{
										'data': 'FGSTYLE_ID',
										"name": "FGSTYLE_ID",
										"title": "FGSTYLE_ID",
										"visible": true,
										"searchable": false,
										"className": "dpass"
									},
									{
										"data": 'FGSHADE_KEY',
										"name": "FGSHADE_KEY",
										"title": "FGSHADE_KEY",
										"visible": true,
										"searchable": false,
										"className": "dpass"
									},
									{
										'data': 'FGSTYLE_NAME',
										"name": "FGSTYLE_NAME",
										"title": "Style",
										"visible": true,
										"searchable": true
									},
									{
										'data': 'FGTYPE_NAME',
										"name": "FGTYPE_NAME",
										"title": "Type",
										"visible": true,
										"searchable": true
									},

									{
										'data': 'FGSHADE_NAME',
										"name": "FGSHADE_NAME",
										"title": "Color",
										"visible": true,
										"searchable": true
									}


								]
							});
						} else {
							datatableVariable.rows().remove().draw();
							datatableVariable.rows.add(JSON.parse(data.d))
								.draw();

						}


						$($.fn.dataTable.tables(true)).DataTable()
							.columns.adjust()
							.responsive.recalc();


					}
				});

				$('#ModalSize').modal('show');
				$("#SpanBarcode-loading-progress").hide();
			});
			$('#GridviewShade').on('change', 'input.editor-active', function () {

				$(this).closest('td').attr("data-override", $(this).prop('checked') ? 1 : 0);
			});
			$('#GridviewSize').on('change', 'input.editor-active', function () {

				$(this).closest('td').attr("data-override", $(this).val());
			});

			$("#<%=btnShadeProceed.ClientID%>").click(function (e) {
				// alert("äbc");
				e.preventDefault();
				if ($("#divShade").is(":visible")) {


					e.preventDefault();
					$("#Spanconfirm-loading-progress").show();
					var jsondata = $("#GridviewShade").tableToJSON(
						{
							ignoreHiddenRows: true,
							allowHTML: false
						}
					);

					var selected = JSON.parse(JSON.stringify(jsondata)).filter(function (entry) {
						return entry.Sel === "1";
					});
					if (selected.length == 0) {
						alert("You must check at least one color.")
						$("#<%=btnShadeProceed.ClientID%>").focus();
						return;
					}

					var table = "'" + JSON.stringify(selected) + "'"



					$("h4.modal-title").html('Enter Qty')

					$.ajax({
						type: "POST",
						url: "newCatalogue.aspx/fillsizedata",
						contentType: "application/json; charset=utf-8",
						data: '{"strdt":' + table + '}',
						dataType: "json",
						error: function (xhr, error, code) {
							alert("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
						},
						success: function (data) {
							//  alert(data.d)
							$('#divSize').css('display', 'block');
							$('#divShade').css('display', 'none');
							if (datatablesize === null) {
								datatablesize = $('#GridviewSize').DataTable({
									"paging": false,
									"ordering": false,
									"info": false,
									"destroy": true,
									"searching": false,
									"responsive": false,
									"scrollX": true,
									"scrollCollapse": true,
									"language": {
										"infoEmpty": "No data available in table"//Change your default empty table message
									},

									data: JSON.parse(data.d),
									columns: [
										//{
										//    "data": 'FGSHADE_KEY',
										//    "name": "IMG",
										//    "title": 'Item',
										//    "render": function (data) {
										//        return '<img src="Images/blankimg.png"  style="width: 50%, height: 50%">';
										//    }
										//},
										{
											'data': 'Qty',
											"name": "Qty",
											"title": "Qty",

											"visible": true,
											"searchable": false,

											"autoWidth": false,
											"render": function (data, type, row) {
												if (type === 'display') {
													return '<input type="number" min="0" name="qty"  class="editor-active" style="width:60px">';
												}
												return data;
											},

										},
										{
											'data': 'STYSIZE_NAME',
											"name": "STYSIZE_NAME",
											"title": "Size",

											"autoWidth": false,
											"visible": true,
											"searchable": true
										},
										{
											'data': 'FGSTYLE_ID',
											"name": "FGSTYLE_ID",
											"title": "FGSTYLE_ID",

											"visible": true,
											"searchable": false,

											"className": "dpass"
										},
										{
											"data": 'FGSHADE_KEY',
											"name": "FGSHADE_KEY",
											"title": "FGSHADE_KEY",

											"visible": true,
											"searchable": false,

											"className": "dpass"
										},
										{
											"data": 'FGTYPE_KEY',
											"name": "FGTYPE_KEY",
											"title": "FGTYPE_KEY",

											"visible": true,
											"searchable": false,

											"className": "dpass"
										},
										{
											"data": 'STYSIZE_ID',
											"name": "STYSIZE_ID",
											"title": "STYSIZE_ID",

											"visible": true,
											"searchable": false,

											"className": "dpass"
										},
										{
											'data': 'FGSTYLE_CODE',
											"name": "FGSTYLE_CODE",
											"title": "Style",

											"autoWidth": false,
											"visible": true,
											"searchable": true
										},
										{
											'data': 'FGTYPE_NAME',
											"name": "FGTYPE_NAME",
											"title": "Type",

											"autoWidth": false,
											"visible": true,
											"searchable": true
										},

										{
											'data': 'FGSHADE_NAME',
											"name": "FGSHADE_NAME",
											"title": "Color",

											"autoWidth": false,
											"visible": true,
											"searchable": true
										},

										{
											'data': 'MRP',
											"name": "MRP",
											"title": "MRP",

											"autoWidth": false,
											"visible": true,
											"searchable": true
										},
										{
											'data': 'WSP',
											"name": "WSP",
											"title": "WSP",

											"autoWidth": false,
											"visible": true,
											"searchable": true
										},
										{
											'data': 'PENDING_ORD_QTY',
											"name": "PENDING_ORD_QTY",
											"title": "Pend Qty",

											"autoWidth": false,
											"visible": true,
											"searchable": true
										},

									]
								});
							} else {
								datatablesize.rows().remove().draw();
								datatablesize.rows.add(JSON.parse(data.d))
									.draw();

							}


							$($.fn.dataTable.tables(true)).DataTable()
								.columns.adjust()
								.responsive.recalc();
							//   $('#ModalSize').modal('show');
							$("#SpanBarcode-loading-progress").hide();

						}
					});

				} else {

					var jsondata = $("#GridviewSize").tableToJSON(
						{
							ignoreHiddenRows: true,
							allowHTML: false
						}
					);
					//   alert(JSON.stringify(jsondata))
					var selected = JSON.parse(JSON.stringify(jsondata)).filter(function (entry) {
						return entry.Qty > 0;
					});
					if (selected.length == 0) {
						alert("Please, Enter Qty")
						$("#<%=btnShadeProceed.ClientID%>").focus();
						return;
					}

					var table = "'" + JSON.stringify(selected) + "'"



					$("h4.modal-title").html('Enter Qty')

					$.ajax({
						type: "POST",
						url: "newCatalogue.aspx/binddata",
						contentType: "application/json; charset=utf-8",
						// data: '{"strdt":' + table + ',"strsrno":1,"fgstyle_id":0,"fgtype":"","fgtypename":"","Fgstylecode":"","multishdename":"","FromCatlogue":1}',
						data: '{"strdt":' + table + '}',
						dataType: "json",
						error: function (xhr, error, code) {
							alert("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
						},
						success: function (response) {
							// alert(response.d)
							if (response.d == "1") {
								var url = "OrderBooking.aspx";
								$(location).attr('href', url);
							}

						}
					});

				}

			});



			//$("#myInput").on("keyup", function () {
			//	var value = $(this).val().toLowerCase();
			//	$("#Products div[name*=STYLE]").filter(function () {
			//		//  alert($(this).attr("id"))
			//		$(this).toggle(this.id.toLowerCase().indexOf(value) > -1)
			//	});
			//});

		});

    </script>
	 <div class="overlay"></div>
	 <div class="row">
        <div class="col-md-12">

            <div class="panel panel-primary" id="list-panel">
                <div class="panel-heading">
                    <h1 class="panel-title">Catlogue</h1>
                </div>

                <div class="panel-body">
		<div class="container-fluid processedfloat">
			<input type="button" data-toggle="modal" value="Proceed" id="ButtonSTYLE" class="btn btn-primary">
		</div>
		
		<asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
			<p class="text-danger">
				<asp:Literal runat="server" ID="FailureText" />
			</p>
		</asp:PlaceHolder>
		<div>
			<div class="form-inline">

				<label for="SelCat" class="mb-2 mr-sm-3">Category:</label>


				<select id="SelCat" class="form-control mb-2 mr-sm-3 " multiple>
				</select>

				<label for="SelProd" class="mb-2 mr-sm-3">Product:</label>


				<select id="SelProd" class="form-control mb-2 mr-sm-3 ">
					<option>Select Product</option>
				</select>



			</div>



			<ul class="pagination" id="pg" style="display:none" >
				<li class="page-item"><a class="page-link" href="#">Previous</a></li>
				<li class="page-item active" id="pg1"><a class="page-link" href="#">1</a></li>
				<%-- <li class="page-item active"><a class="page-link" href="#">2</a></li>
				<li class="page-item"><a class="page-link" href="#">3</a></li>--%>
				<li class="page-item"><a class="page-link" href="#">NexNext</a></li>
			</ul>
			<br />


			<div class="row">
				<div class="col-lg-12">
					<label id="lblTotCount" class="mb-2 mr-sm-3"></label>

					<asp:Button ID="btnload" Text="Load" runat="server" CssClass="btn btn-primary col-lg-2" Style="float: right"  />
					<input type="text" class="form-control col-lg-10" id="myInput" placeholder="Search by Style code" style="float: right">
				</div>
			</div>


			<br />
			<div class="form-inline" id="Products">

				<div class="col-lg-3 col-md-4 col-sm-12" id="itm3">
					<div class="panel-group">
						<div class="panel panel-default">
							<%-- <div class="panel-heading list-panel-heading">
								<h4 class="panel-title list-panel-title"></h4>
								 
							</div>--%>
							<div class="panel-body">
								<input type="checkbox" class="form-control-lg" value="">
								<label></label>


								<input type="hidden" id="Type" name="Type" value="" />
								<input type="hidden" id="Shade" name="Shade" value="" />
								<input type="hidden" id="Style" name="Style" value="" />
								<img src="Images/blankimg.png" style="width: 100%" />

								<%-- <button type="button"  class="btn btn-primary">Add to cart</button>--%>
							</div>
						</div>
					</div>

				</div>

			</div>

		</div>



		<div id="ModalSize" class="modal fade" role="dialog" aria-labelledby="Edit" aria-hidden="true">


			<div class="modal-dialog modal-lg">
				<div class="modal-content">

					<!-- Modal Header -->
					<div class="modal-header">

						<button type="button" class="close" data-dismiss="modal"></Button>
						<h4 class="modal-title">Order Booking</h4>
					</div>

					<!-- Modal body -->
					<div class="modal-body">

						<div class="row">
							<div class="col-md-12">
								<div id="divShade">
									<table id="GridviewShade" style="width: 100%">
									</table>
								</div>
								<div id="divSize">
									<table id="GridviewSize" style="width: 100%">
									</table>
								</div>
							</div>


						</div>

						<!-- Modal footer -->
						<div class="modal-footer">
							<asp:Button ID="btnShadeProceed" runat="server" ValidationGroup="btnAddSize" Text="Proceed" CssClass="btn btn-primary" />
							<button type="button" class="btn btn-default" id="Close" data-dismiss="modal">Close</button>
						</div>

					</div>
				</div>
			</div>
		</div>
		
					</div>
				</div>
			</div>
		 </div>
	
</asp:Content>
