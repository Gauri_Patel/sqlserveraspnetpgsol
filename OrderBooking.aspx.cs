﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

using System.IO;
using System.Collections;
using Microsoft.VisualBasic;
using System.Web.SessionState;
using System.Threading;
using System.Web.Services;
using System.Text;
using System.Web.Security;
using Common;
using System.Web.Script.Serialization;
using Newtonsoft.Json;


namespace GPGSOLASPNET
{
    public partial class OrderBooking : System.Web.UI.Page
    {
        DataTable dttemp = new DataTable();
        DataTable dtOrdDtls = new DataTable();
        DataTable dtdetails = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!User.Identity.IsAuthenticated) // if the user is already logged in
                {
                    Response.Redirect(FormsAuthentication.LoginUrl);
                }
                if (!Page.IsPostBack)
                {
                    combo_fill();
                    rdstyle.Checked = true;
                    rdnormal.Checked = true;
                    txtdlv_date.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");// Convert.ToString( DateTime.Now.Date);
                    
                    fillgridSizewithDummyData();
                    
                    if (SessionManager.dtcatelogdtl != null && SessionManager.dtcatelogdtl.Rows.Count > 0)
                    {
                        int i = 0;
                        DataRow drRow;
                        if (SessionManager.ordtbl.Rows.Count > 0)
                        {

                            i = Convert.ToInt32(SessionManager.ordtbl.Compute("MAX(SRNO)", ""));
                        }
                        else
                        {
                            i = 0;
                        }



                        foreach (DataRow dr in SessionManager.dtcatelogdtl.Rows)
                        {
                            i = i + 1;
                            drRow = SessionManager.ordtbl.NewRow();
                            drRow["SRNO"] = i;
                            drRow["ALT_BARCODE"] = "";
                            drRow["FGSTYLE_ID"] = dr["FGSTYLE_ID"];
                            drRow["FGSTYLE_CODE"] = dr["FGSTYLE_CODE"];
                            drRow["FGTYPE_KEY"] = dr["fgtype_key"];
                            drRow["FGTYPE_NAME"] = dr["FGTYPE_NAME"];
                            drRow["FGSHADE_KEY"] = dr["FGSHADE_KEY"];
                            drRow["FGSHADE_NAME"] = dr["FGSHADE_NAME"];
                            drRow["STYSIZE_NAME"] = dr["STYSIZE_NAME"];
                            drRow["STYSIZE_ID"] = dr["STYSIZE_ID"];
                            //drRow["QTY"] = (Convert.ToString(drQty["QTY"]).Trim().Length == 0 ? 0 : drQty["QTY"]);
                            drRow["Qty"] = dr["QTY"];//to add default qty next new record if same stysize_name
                            drRow["MRP"] = dr["MRP"];
                            drRow["WSP"] = dr["WSP"];
                            drRow["AMOUNT"] = Convert.ToDouble(dr["WSP"]) * Convert.ToDouble(drRow["QTY"]);
                            drRow["PENDING_ORD_QTY"] = dr["PENDING_ORD_QTY"];
                            drRow["COBR_ID"] = SessionManager.g_strcobr_id;
                            dr.AcceptChanges();
                            SessionManager.ordtbl.Rows.Add(drRow);

                        }
                        SessionManager.dtcatelogdtl.Rows.Clear();
                    }
                    else {
                        SessionManager.ordtbl = null;
                    }

                    HttpContext.Current.Session["dtdetails"] = null;

                }


            }
            catch (Exception ex)
            {
                modMain.ShowMessage(ex.Message, this);
            }
        }

   

     
        private void fillgridSizewithDummyData()
        {

            try
            {


                //  string strquery = "EXEC ASP_SPMODULE  @NMODE='fillgridSizedata'";
                SessionManager.g_strQuery = "EXEC ASP_ORD_GETITMDTL  @NMODE='',@PARTY_KEY='',@PARTYDTL_ID=0,@COBR_ID='" + SessionManager.g_strcobr_id + "',@FGSTYLE_ID=0,@FGSHADE_KEY=''";
                DataTable dt = modMain.NewDatatable(SessionManager.g_strQuery, SessionManager.g_strConnString, null);
                dt.Rows.Clear();
                dt.Rows.Add();
                GridviewSize.DataSource = dt;

                GridviewSize.DataBind();
            }
            catch (Exception ex)
            {
                modMain.ShowMessage(ex.Message, this);
            }

        }
        private void gridfillsum(string party_key, Int32 partydtl_id)
        {
            try
            {
                string strquery = "SELECT ('" + SessionManager.g_strfcyrID + SessionManager.g_strcobr_id + lblorder_no.Text + "') ordbk_key ,'" + SessionManager.g_strfcyrID + "' as FCYR_KEY,'" + SessionManager.g_strCo_id + "' AS CO_ID,'" + SessionManager.g_strcobr_id + "' AS  COBR_ID,'" + lblorder_no.Text + "'  ORDBK_NO,getdate() ORDBK_DT, a.PARTY_KEY, PARTYDTL_ID, c.BROKER_KEY,'" + Convert.ToDateTime(txtdlv_date.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat) + "' DLV_DT,'" + txtremark.Text + "' REMK, "
                            + " IsNULL(b.DISTBTR_NAME, 'NA') as CONSIGNEE, 1 as STATUS  ,IsNULL(b.DISTBTR_KEY, 0) DISTBTR_KEY, c.SALEPERSON1_KEY,'' CREATED_DT,'' UPDATED_DT,'" + drpordtype.SelectedValue + "' AS ORDBK_TYPE "
                            + " FROM party a  left join CLIENTTERMS c on a.PARTY_KEY = c.PARTY_KEY  "
                            + " left join DISTBTR b on c.DISTBTR_KEY = b.DISTBTR_KEY "
                            + " left join PARTYDTL d on a.PARTY_KEY = d.PARTY_KEY "
                            + " WHERE a.party_key ='" + party_key + "' and d.partydtl_id=" + partydtl_id;

                modMain.gridviewbind(strquery, SessionManager.g_strConnString, gdsumm);
                ViewState["DataSUM"] = gdsumm.DataSource;

            }
            catch (SqlException ex)
            {
                modMain.ShowMessage(ex.Message, this);
            }

        }


        void getmax_id()
        {
            lblorder_no.Text = "";

            DataTable worktable = new DataTable();
            String strSeries = "66";
            try
            {

                if (drpparty.Text.Length != 0)
                {
                    string strquery = "EXEC ASP_SPMODULE @NMODE='GenerateCode',@CWhere='" + strSeries + "',@CCOBR_ID='" + SessionManager.g_strcobr_id + "'";
                    worktable = modMain.NewDatatable(strquery, SessionManager.g_strConnString, null);

                    if (worktable.Rows.Count != 0)
                    {
                        lblorder_no.Text = Convert.ToString(worktable.Rows[0]["NEW_VALUE"]);

                    }
                }
            }

            catch (Exception ex)
            {
                modMain.ShowMessage(ex.Message, this);
            }
            finally
            {
                worktable = null;
            }

        }
        void combo_fill()
        {
            string strquery = "";

            strquery = "EXEC ASP_SPMODULE  @NMODE='Partyfill',@CUSERPREFIX='" + modMain.g_strpartyfill + "',@CWhere='" + SessionManager.g_strUserId + "'";
            modMain.FillInCombo(strquery, drpparty, "PARTY_NAME", "PARTY_KEY");
            if (modMain.g_strpartyfill == "S")
            {
                drpparty.Items.Insert(0, "Select");
            }
            //if (drpparty.Text != "Select")
            //{
            //    drpparty_SelectedIndexChanged(drpparty, null);
            //}
            //strquery = "EXEC ASP_SPMODULE  @NMODE='stylefill'";
            //modMain.FillInCombo(strquery, drpstyle, "FGSTYLE_NAME", "FGSTYLE_ID");
            //drpstyle.Items.Insert(0, "Select");

        }
    
      
      

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //  return;
            String strtable_name = "";
            getmax_id();

            String strordbk = SessionManager.g_strfcyrID + SessionManager.g_strcobr_id + lblorder_no.Text;

            gridfillsum(Convert.ToString(Request.Form.Get(Request.Form.AllKeys.Where<string>(c => c.Contains(drpparty.ID)).FirstOrDefault())), Convert.ToInt32(Request.Form.Get(Request.Form.AllKeys.Where<string>(c => c.Contains(drpplace.ID)).FirstOrDefault())));

            DataSet df = (DataSet)ViewState["DataSUM"];

            DataTable dtsumm = df.Tables[0];

            if (dtsumm.Rows.Count == 0)
            {
                modMain.ShowMessage("Party is not provide!", this);

                return;
            }
            if (SessionManager.ordtbl == null || SessionManager.ordtbl.Rows.Count == 0)
            {
                modMain.ShowMessage("Detail is not provide!", this);
                return;
            }
            try
            {

                // conn.Open();


                strtable_name = "ORDBKASP";
                modMain.CreateAndInsertDataIntoTable(dtsumm, strtable_name, strordbk, null);

                strtable_name = "ORDBKASPDTL";
                modMain.CreateAndInsertDataIntoTable(SessionManager.ordtbl, strtable_name, strordbk, null);

                SessionManager.g_strQuery = " IF EXISTS(SELECT * FROM ORDBKASPDTL WHERE (ORDBK_KEY IS NULL OR ORDBK_KEY='')) "
                                 + " BEGIN "
                                 + " UPDATE ORDBKASPDTL SET ORDBK_KEY='" + strordbk + "', USER_ID=1 WHERE (ORDBK_KEY IS NULL OR ORDBK_KEY='') "
                                 + " END";
                modMain.ExecuteQuery(SessionManager.g_strQuery, null);

                //modMain.g_Trans.Commit();
                //modMain.OpenCloseConnection(false);

                //modMain.OpenCloseConnection(true);
                //modMain.g_Trans = modMain.g_Conn.BeginTransaction();
                modMain.OpenCloseConnection(true);
                modMain.g_Trans = modMain.g_sqlConn.BeginTransaction();
                SessionManager.g_strQuery = "exec ASP_ORDER @ORDER_ID='" + strordbk + "',@USER_ID=1";
                modMain.ExecuteQuery(SessionManager.g_strQuery, modMain.g_Trans);

                modMain.g_Trans.Commit();
                SessionManager.ordtbl = null;
                HttpContext.Current.Session["dtdetails"] = null;

                // modMain.ShowMessage("Order Booking Update successfully......!", this);

                Response.Write("<script language='javascript'>window.alert('Order Booking Update successfully......!');window.location='OrderBooking.aspx';</script>");
            }
            catch (SqlException ex)
            {


                modMain.ShowMessage(ex.Message, this);
                modMain.g_Trans.Rollback();
                return;
            }
            finally
            {
                modMain.OpenCloseConnection(false);

            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("OrderBooking.aspx");
        }

     
        [WebMethod]
        public static String FillImage(string StyleId, string shadekey)
        {
            try
            {

                //String strquery = "EXEC ASP_SPMODULE @NMODE='styleimgfill',@CWhere=" + StyleId + "";

                String strquery = "EXEC SP_GETIMAGEPATH @intFGSTYLE_ID=" + StyleId + ",@strFGSHADE_KEY='" + shadekey + "'";
                DataTable dt = modMain.NewDatatable(strquery, SessionManager.g_strConnString, null);
                string strimage = "";
                string strimgPath = "";
                foreach (DataRow dr in dt.Rows)
                {

                    // Read the file and convert it to Byte Array

                    string filePath = modMain.g_ImagePath;

                    if (Convert.ToString(dr["SHDE_PATH"]).Trim().Length != 0)
                    {
                        strimgPath = modMain.g_ImagePathMultiShade + (dr["SHDE_PATH"] + "");
                    }
                    else if (Convert.ToString(dr["STYLE_PATH"]).Trim().Length != 0)
                    {
                        strimgPath = modMain.g_ImagePath + (dr["STYLE_PATH"] + "");
                    }



                    string filename = Convert.ToString(dr["STYLE_PATH"]);

                    string contenttype = "image/" +

                    Path.GetExtension(filename.Replace(".", ""));

                    FileStream fs = new FileStream(strimgPath,

                    FileMode.Open, FileAccess.Read);

                    BinaryReader br = new BinaryReader(fs);

                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                    br.Close();

                    fs.Close();

                    strimage = ("data:image/png;base64," + Convert.ToBase64String(bytes, Base64FormattingOptions.None));


                }



                return strimage;

            }
            catch (Exception)
            {

                return "";
            }
        }
        [WebMethod]
        public static string RetriveEdit(string strdt, string strsrno, string fgstyle_id, string FgshadeId, string fgtype, string Fgshadecode)
        {


            return "";
        }
        [WebMethod]
        public static void UpdateCustomer(int Srno, decimal Qty)
        {

            DataTable dtOrdDtls = new DataTable();


            DataRow[] arr = SessionManager.ordtbl.Select(SessionManager.ordtbl.Columns["SRNO"].ColumnName + " =" + Srno);
            if (arr.Length > 0)
            {
                arr[0]["Qty"] = Qty;
                arr[0].AcceptChanges();
            }

        }

        [WebMethod]
        public static void DeleteSize(int Srno)
        {

            DataRow[] drdelete;
            drdelete = SessionManager.ordtbl.Select(SessionManager.ordtbl.Columns["SRNO"].ColumnName + " =" + Srno + "");

            foreach (DataRow dr in drdelete)
            {
                SessionManager.ordtbl.Rows.Remove(dr);
            }

            SessionManager.ordtbl.AcceptChanges();
        }

        [WebMethod]
        public static string binddata(string strdt, string strsrno, string fgstyle_id, string fgtype_key, string fgtypename, string Fgstylecode, string multishdename)
        {
            if (strdt.Length==0)
            {
                DataTable dttmp=null;
                //string strquery = "EXEC ASP_SPMODULE  @NMODE='fillgridSize'";
                if (SessionManager.ordtbl.Rows.Count != 0)
                {
                    dttmp = SessionManager.ordtbl;
                }
                else
                {
                    SessionManager.g_strQuery = "EXEC ASP_ORD_GETITMDTL  @NMODE='',@PARTY_KEY='',@PARTYDTL_ID=0,@COBR_ID='" + SessionManager.g_strcobr_id + "',@FGSTYLE_ID=0,@FGSHADE_KEY=''";
                    dttmp = modMain.NewDatatable(SessionManager.g_strQuery, SessionManager.g_strConnString, null);
                    dttmp.Rows.Clear();
                 
                }
                return JsonConvert.SerializeObject(dttmp);
            }
            int i = 0;
            DataTable dt = (DataTable)JsonConvert.DeserializeObject(strdt, (typeof(DataTable)));
            DataRow drRow;

            if (SessionManager.ordtbl.Rows.Count > 0)
            {

                i = Convert.ToInt32(SessionManager.ordtbl.Compute("MAX(SRNO)", ""));
            }
            else
            {
                i = 0;
            }
        
                DataTable dttemp = new DataTable();

                if ((DataTable)HttpContext.Current.Session["dtdetails"] != null)
                {
                    dttemp = (DataTable)HttpContext.Current.Session["dtdetails"];

                }
                if (strsrno == "1")
                {

                    String[] strlist = new string[0];

                    char[] spearator = { ',' };

                    // using the method 


                    strlist = multishdename.Split(spearator);

                    foreach (String multishadekey in strlist)
                    {

                        foreach (DataRow dr in dttemp.Rows)
                        {

                            DataRow drQty = dt.Select("Size='" + dr["STYSIZE_NAME"] + "'")[0];
                            drRow = SessionManager.ordtbl.NewRow();
                            drRow["SRNO"] = i + 1;
                            drRow["ALT_BARCODE"] = "";
                            drRow["FGSTYLE_ID"] = fgstyle_id;
                            drRow["FGSTYLE_CODE"] = Fgstylecode;
                            drRow["FGTYPE_KEY"] = fgtype_key;
                            if (multishadekey != "")
                            {
                                drRow["FGSHADE_KEY"] = multishadekey.Substring(0, multishadekey.IndexOf("-"));
                                drRow["FGSHADE_NAME"] = multishadekey.Substring(multishadekey.IndexOf("-") + 1);
                            }
                            else
                            {
                                drRow["FGSHADE_KEY"] = "";
                                drRow["FGSHADE_NAME"] = "";

                            }
                            drRow["FGTYPE_NAME"] = fgtypename;

                            drRow["STYSIZE_NAME"] = dr["STYSIZE_NAME"];
                            drRow["QTY"] = (Convert.ToString(drQty["QTY"]).Trim().Length == 0 ? 0 : drQty["QTY"]);
                            dr["Qty"] = drRow["QTY"];//to add default qty next new record if same stysize_name
                            drRow["MRP"] = dr["MRP"];
                            drRow["WSP"] = dr["WSP"];
                            drRow["AMOUNT"] = Convert.ToDouble(dr["WSP"]) * Convert.ToDouble(drRow["QTY"]);
                            drRow["PENDING_ORD_QTY"] = dr["PENDING_ORD_QTY"];
                            drRow["COBR_ID"] = SessionManager.g_strcobr_id;
                            dr.AcceptChanges();
                            SessionManager.ordtbl.Rows.Add(drRow);
                            i = i + 1;
                        }

                    }



                }
                else
                {
                    DataRow[] dredit;
                    dredit = SessionManager.ordtbl.Select(SessionManager.ordtbl.Columns["SRNO"].ColumnName + " =" + strsrno + "");

                }

                //DataSet ds = new DataSet();
                // ds.Tables.Remove(dtOrdDtls);
                //DataTable dtdetails = new DataTable();
                //dtdetails = SessionManager.ordtbl.Copy();

                //ds.Tables.Add(dtdetails);
                //ds.Tables[0].TableName = "Size";
                //SessionManager.ordtbl = ds.Tables[0];
                return JsonConvert.SerializeObject(SessionManager.ordtbl);
            
        }
        [WebMethod]
        public static string bind(string StyleId, string FgshadeId, string Strtype, string strParty_key, Int32 intPartydtl_id, int? SETQTY = 0, decimal? conversion = 0)
        {
            DataTable worktable = new DataTable();
            if (Strtype != "B")
            {
                SessionManager.g_strQuery = "EXEC ASP_ORD_GETITMDTL  @NMODE='',@PARTY_KEY='" + strParty_key + "',@PARTYDTL_ID=" + intPartydtl_id + ",@COBR_ID='" + SessionManager.g_strcobr_id + "',@FGSTYLE_ID=" + StyleId + ",@FGSHADE_KEY='" + FgshadeId + "',@QTY=" + (SETQTY * conversion) + "";
                //worktable = modMain.NewDatatable(SessionManager.g_strQuery, SessionManager.g_strConnString, null);
                //if (worktable.Rows.Count == 0)
                //{
                //    SessionManager.g_strQuery = "EXEC ASP_ORD_GETITMDTL  @NMODE='fillgridStyle',@FGSTYLE_ID=" + StyleId + ",@FGSHADE_KEY='" + FgshadeId + "',@CCOBR_ID='" + modMain.strcobr_id + "',@CFCYR='" + SessionManager.g_strfcyrID + "'";
                //}
            }
            else
            {
                //SessionManager.g_strQuery = "EXEC ASP_SPMODULE  @NMODE='fillgridAltBarcode',@ALT_BARCODE='" + StyleId + "',@CCOBR_ID='" + modMain.strcobr_id + "'";
                //worktable = modMain.NewDatatable(SessionManager.g_strQuery, SessionManager.g_strConnString, null);
                //if (worktable.Rows.Count == 0)
                //{
                //    SessionManager.g_strQuery = "EXEC ASP_SPMODULE  @NMODE='fillgridBarcode',@ALT_BARCODE='" + StyleId + "',@CCOBR_ID='" + modMain.strcobr_id + "'";
                //    worktable = modMain.NewDatatable(SessionManager.g_strQuery, SessionManager.g_strConnString, null);
                //}
                //if (worktable.Rows.Count == 0)
                //{

                //    SessionManager.g_strQuery = "SELECT 'INVALID BARCODE' Error";

                //}
                //else
                //{

                //    SessionManager.g_strQuery = "EXEC ASP_SPMODULE  @NMODE='fillgrid',@FGSTYLE_ID=" + worktable.Rows[0]["FGSTYLE_ID"] + ",@FGSHADE_KEY='" + worktable.Rows[0]["FGShade_key"] + "',@CCOBR_ID='" + modMain.strcobr_id + "'";
                //    worktable = modMain.NewDatatable(SessionManager.g_strQuery, SessionManager.g_strConnString, null);
                //    if (worktable.Rows.Count == 0)
                //    {
                //        SessionManager.g_strQuery = "EXEC ASP_SPMODULE  @NMODE='fillgridStyle',@FGSTYLE_ID=" + worktable.Rows[0]["FGSTYLE_ID"] + ",@FGSHADE_KEY='" + worktable.Rows[0]["FGShade_key"] + "',@CCOBR_ID='" + modMain.strcobr_id + "',@CFCYR='" + SessionManager.g_strfcyrID + "'";
                //    }
                //}
                SessionManager.g_strQuery = "EXEC ASP_ORD_GETITMDTL  @NMODE='',@PARTY_KEY='" + strParty_key + "',@PARTYDTL_ID=" + intPartydtl_id + ",@COBR_ID='" + SessionManager.g_strcobr_id + "',@ALT_BARCODE='" + StyleId + "',@FGSTYLE_ID=0,@FGSHADE_KEY='" + FgshadeId + "',@QTY=" + (SETQTY * conversion) + "";

            }
            DataSet ds = modMain.NewDataset(SessionManager.g_strQuery, SessionManager.g_strConnString, null);
            ds.Tables[0].TableName = "Size";
            if ((SETQTY * conversion) == 0)
            {
                if (!ds.Tables[0].Columns.Contains("Error"))
                {


                    if (HttpContext.Current.Session["dtdetails"] != null)
                    {
                        DataTable dt = (DataTable)HttpContext.Current.Session["dtdetails"];
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {

                            DataRow[] arr = dt.Select("STYSIZE_NAME='" + item["STYSIZE_NAME"] + "'");
                            if (arr.Length > 0)
                            {
                                item["Qty"] = arr[0]["Qty"];
                            }
                        }
                    }
                    HttpContext.Current.Session["dtdetails"] = (DataTable)ds.Tables[0];
                }
            }
            return ds.GetXml();
        }



        [WebMethod]
        public static string FILLSTYLEFILL(string StyleId)
        {
            try
            {

                String strquery = "EXEC ASP_SPMODULE @NMODE='stylefill',@CWhere='" + StyleId + "'";
                DataTable worktable = modMain.NewDatatable(strquery, SessionManager.g_strConnString, null);
                StringBuilder stringBuilder = new StringBuilder();

                //stringBuilder.Append("<option value=Select>Select</option>");
                for (int i = 0; i <= worktable.Rows.Count - 1; i++)
                {
                    DataRow dr = worktable.Rows[i];
                    if (i == 0)
                    {
                        stringBuilder.Append("<option selected value=\"" + dr["FGSTYLE_ID"] + "\">" + dr["FGSTYLE_NAME"] + "</option>");
                    }
                    else
                    {
                        stringBuilder.Append("<option value=\"" + dr["FGSTYLE_ID"] + "\">" + dr["FGSTYLE_NAME"] + "</option>");
                    }
                }




                return stringBuilder.ToString();
            }
            catch (Exception)
            {

                return "";
            }
        }

        [WebMethod]
        public static string FillAddr(string StrParty_key)
        {
            try
            {
                String strquery = "EXEC ASP_SPMODULE @NMODE='Partydetailsfill',@CWhere=" + StrParty_key + "";
                DataTable worktable = modMain.NewDatatable(strquery, SessionManager.g_strConnString, null);
                StringBuilder stringBuilder = new StringBuilder();

                foreach (DataRow dr in worktable.Rows)
                    stringBuilder.Append(Convert.ToString(dr["ADDR"]));

                return stringBuilder.ToString();
            }
            catch (Exception)
            {

                return "";
            }
        }

        [WebMethod]
        public static string FillConsi(string StrParty_key)
        {
            try
            {
                String strquery = "EXEC ASP_SPMODULE @NMODE='Partydetailsfill',@CWhere=" + StrParty_key + "";
                DataTable worktable = modMain.NewDatatable(strquery, SessionManager.g_strConnString, null);
                StringBuilder stringBuilder = new StringBuilder();

                foreach (DataRow dr in worktable.Rows)
                    stringBuilder.Append(Convert.ToString(dr["Consignee_name"]));

                return stringBuilder.ToString();
            }
            catch (Exception)
            {

                return "";
            }
        }

        [WebMethod]
        public static string FillAddrPartydtl(string StrParty_key)
        {
            try
            {

                String strquery = "EXEC ASP_SPMODULE @NMODE='PartyDtldetailsfill',@CWhere=" + StrParty_key + "";
                DataTable worktable = modMain.NewDatatable(strquery, SessionManager.g_strConnString, null);
                StringBuilder stringBuilder = new StringBuilder();

                foreach (DataRow dr in worktable.Rows)
                    stringBuilder.Append(Convert.ToString(dr["ADDR"]));

                return stringBuilder.ToString();
            }
            catch (Exception)
            {

                return "";
            }
        }

        [WebMethod]
        public static string FillConsiPartydtl(string StrParty_key)
        {
            try
            {

                String strquery = "EXEC ASP_SPMODULE @NMODE='PartyDtldetailsfill',@CWhere=" + StrParty_key + "";
                DataTable worktable = modMain.NewDatatable(strquery, SessionManager.g_strConnString, null);
                StringBuilder stringBuilder = new StringBuilder();

                foreach (DataRow dr in worktable.Rows)
                    stringBuilder.Append(Convert.ToString(dr["Consignee_name"]));

                return stringBuilder.ToString();
            }
            catch (Exception)
            {

                return "";
            }
        }

        [WebMethod]
        public static string Fillplace(string StrParty_key)
        {
            try
            {
                String strquery = "EXEC ASP_SPMODULE @NMODE='PARTYPLACEFILL',@CWhere=" + StrParty_key + "";
                DataTable worktable = modMain.NewDatatable(strquery, SessionManager.g_strConnString, null);
                StringBuilder stringBuilder = new StringBuilder();

                foreach (DataRow dr in worktable.Rows)
                    stringBuilder.Append("<option value=\"" + dr["PARTYDTL_ID"] + "\">" + dr["PLACE"] + "</option>");

                return stringBuilder.ToString();
            }
            catch (Exception)
            {

                return "";
            }
        }

        [WebMethod]
        public static string FillTypeByStyleId(string StyleId)
        {
            try
            {
                String strquery = "EXEC ASP_SPMODULE @NMODE='styletypefill',@CWhere=" + StyleId + "";
                DataTable worktable = modMain.NewDatatable(strquery, SessionManager.g_strConnString, null);
                StringBuilder stringBuilder = new StringBuilder();

                foreach (DataRow dr in worktable.Rows)
                    stringBuilder.Append("<option value=\"" + dr["FGTYPE_KEY"] + "\">" + dr["FGTYPE_NAME"] + "</option>");

                return stringBuilder.ToString();
            }
            catch (Exception)
            {

                return "";
            }
        }

        [WebMethod]
        public static string FillShadeByStyleId(string StyleId, string strflag = "Allocated")
        {
            try
            {
                String strquery = "EXEC ASP_SPMODULE @NMODE='styleshadefill',@CWhere=" + StyleId + ", @CUSERPREFIX='" + strflag.ToUpper() + "'";
                DataTable worktable = modMain.NewDatatable(strquery, SessionManager.g_strConnString, null);
                StringBuilder stringBuilder = new StringBuilder();

                foreach (DataRow dr in worktable.Rows)
                    stringBuilder.Append("<option value=\"" + dr["FGSHADE_KEY"] + "\">" + dr["FGSHADE_NAME"] + "</option>");


                return stringBuilder.ToString();
            }
            catch (Exception)
            {

                return "";
            }
        }
        void combo_fillstyle()
        {
            string strquery = "";


            // strquery = "EXEC ASP_SPMODULE  @NMODE='stylefill'";
            strquery = "EXEC ASP_SPMODULE  @NMODE='stylefill',@CWhere='" + txtstyle.Text + "'";
            modMain.FillInCombo(strquery, drpstyle, "FGSTYLE_NAME", "FGSTYLE_ID");
            drpstyle.Items.Insert(0, "Select");

        }

        protected void txtstyle_TextChanged(object sender, EventArgs e)
        {
            combo_fillstyle();
        }

    }

}
