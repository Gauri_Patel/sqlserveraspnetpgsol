﻿<%@ Page Title="Catalogue" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Catalogue.aspx.cs" Inherits="GPGSOLASPNET.Catalogue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Content/jquery-ui.css" rel="stylesheet" />
    <script src="Scripts/jquery-ui.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            // Select tab by name
            $('.nav-tabs a[href="#main"]').tab('show')

            $('.nav-tabs a').click(function () {
                $("#MainContent_TabName").val($(this).attr("href").replace("#", ""))

                $(this).tab('show');

            })

        });


        $(function () {
            $("#dialog").dialog({
                autoOpen: false,
                modal: true,
                height: 600,
                width: 600,
                title: "Zoomed Image"
            });
            $("[id*=datalist1] img").click(function () {
                $('#dialog').html('');
                $('#dialog').append($(this).clone());
                $('#dialog').dialog('open');
            });
        });

    </script>
    <div class="row">
        <div class="col-md-12">
            <br />
            <div class="panel panel-primary list-panel" id="list-panel">
                <div class="panel-heading list-panel-heading">
                    <h1 class="panel-title list-panel-title">Catalogue</h1>
                </div>
                <br />
                <div class="container">


                   
                    <div class="panel panel-default" style="width: 95%; padding: 10px; margin: 10px">
                        
                       
                         
                                <div class="form-group row">
                                    <asp:Label runat="server" AssociatedControlID="drptype" CssClass="control-label col-sm-2">Type:</asp:Label>

                                   <div class="col-sm-10"> 

                                    <asp:DropDownList ID="drptype" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="drptype_SelectedIndexChanged"></asp:DropDownList>

                                   </div>
                                </div>
                         <div class="form-group row">
                        <asp:Label runat="server" AssociatedControlID="lblcount" CssClass="control-label col-sm-2">Img count:</asp:Label>
                        <asp:Label runat="server" ID="lblcount" ></asp:Label>
                        </div>
                        <br />

                                <div>
                                    <asp:DataList ID="datalist1" runat="server" RepeatColumns="3" RepeatDirection="horizontal">
                                        <ItemTemplate>

                                            <div>
                                                <asp:Label runat="server" ID="lblfgstyle_id" Text='<%#Eval("fgstyle_key")%>'></asp:Label>
                                               </div>
                                                <div>
                                                <asp:Image runat="server" Width="200" Height="150" ImageUrl='<%#Eval("Image", "~/StyleImg/{0}") %>' />
                                            </div>
                                           
                                           
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                            </div>
                        
                    <asp:HiddenField ID="TabName" runat="server" />

                     <div>
                    <asp:Button runat="server" ID="btnloadimg" Text="Load More" CssClass="btn btn-primary" OnClick="btnloadimg_Click" />

                            <asp:HyperLink runat="server" CssClass=" btn-link btn" ID="Loginpage" ViewStateMode="Disabled" NavigateUrl="~/Contact.aspx">Go Back</asp:HyperLink>
                </div>

               
                    
                     

                </div>

               
           

               
            </div>



        </div>
    </div>

 



</asp:Content>
