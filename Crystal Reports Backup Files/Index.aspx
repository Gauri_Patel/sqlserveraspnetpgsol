﻿<%@ Page Title="Login Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="GPGSOLASPNET.Index" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


    <div class="container login">
        <div >

            <div class="panel panel-primary list-panel" id="list-panel">
                <div class="panel-heading list-panel-heading">
                    <h1 class="panel-title list-panel-title">Login</h1>
                </div>

                <div class="panel-body">

                    <div class="row">
                      
                        <div class="col-md-5" style="padding-top: 10%">
                            
                            <img src="Images/logo.jpg" class="img-responsive"  />
                            <div class="col-md-12 mobile-hide" id="contact-info">
                                <br /><br /><br />
                                
                                <%--   <img src="Images/PRASYST_Logo.png" class="img-responsive" />--%>
                                <span>Powered By :</span><br />
                                <span style="color: red; font-weight: bold">PRATHAM SYSTECH INDIA LTD.</span><br />
                                <span style="color: blueviolet; font-size: small">The symbol of business integration</span><br />
                                <span>022 - 28953371 - 76</span><br />
                                <span>support@prathamglobal.com</span>
                            </div>
                        </div>
                        <div class="col-md-7">


                            <div class="form-group row">
                                <asp:Label runat="server" AssociatedControlID="drpYear" CssClass="control-label col-md-3">Year</asp:Label>

                                <div class="col-md-9">

                                    <asp:DropDownList ID="drpYear" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="1415">2014 - 2015</asp:ListItem>
                                        <asp:ListItem Value="1516">2015 - 2016</asp:ListItem>
                                        <asp:ListItem Value="1617">2016 - 2017</asp:ListItem>
                                        <asp:ListItem Value="1718">2017 - 2018</asp:ListItem>
                                        <asp:ListItem Value="1920">2019 - 2020</asp:ListItem>
                                        <asp:ListItem Value="2021">2020 - 2021</asp:ListItem>
                                    </asp:DropDownList>

                                  <%--  <asp:DropDownList ID="Compcode" runat="server" CssClass="form-control" Visible="false"></asp:DropDownList>--%>

                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="drpYear"
                                        CssClass="text-danger" ErrorMessage="The year is required." />
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                     <label class="radio-inline">
                                        <asp:RadioButton runat="server" ID="rdUser" GroupName="cust_salesper" Text="User" Checked="true" /></label>
                                    <label class="radio-inline">
                                        <asp:RadioButton runat="server" ID="rdcust" GroupName="cust_salesper" Text="Customer"  /></label>
                                    <label class="radio-inline">
                                        <asp:RadioButton runat="server" ID="rdsalesper" GroupName="cust_salesper" Text="Salesperson" /></label>
                                </div>
                            </div>


                            <div class="form-group row">
                                <asp:Label runat="server" AssociatedControlID="username" CssClass="control-label col-md-3">User Name</asp:Label>
                                <div class="col-md-9">
                                    <asp:TextBox runat="server" ID="username" CssClass="form-control" AutoPostBack="false" OnTextChanged="username_TextChanged" />

                                </div>
                            </div>

                            <div class="form-group row">
                                <asp:Label runat="server" AssociatedControlID="Password" CssClass="control-label col-md-3">Password</asp:Label>
                                <div class="col-md-9">
                                    <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                                    <%--   <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" CssClass="text-danger" ErrorMessage="The password field is required." />--%>
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Button runat="server" OnClick="LogIn" Text="Log in" CssClass="btn btn-default" />
                                    <asp:HyperLink runat="server" CssClass=" btn-link btn" ID="ForgotPasswordHyperLink" ViewStateMode="Disabled" NavigateUrl="~/forgot_password.aspx">Forgot your password?</asp:HyperLink>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4" style="vertical-align: middle; text-align: center;">
                                    <asp:Label runat="server" align="center" Text=""> </asp:Label>
                                </div>
                                <div class="col-md-4" style="vertical-align: middle; text-align: center;">
                                    <asp:Label runat="server" align="center" Text="--- OR ---"> </asp:Label>
                                </div>
                                <div class="col-md-4" style="vertical-align: middle; text-align: center;">
                                    <asp:Label runat="server" align="center" Text=""> </asp:Label>
                                </div>
                            </div>


                            <div class="form-group row">
                                <asp:Label runat="server" AssociatedControlID="txtmobileno" CssClass="control-label col-md-3">Mobile No</asp:Label>
                                <div class="col-md-9">
                                    <asp:TextBox runat="server" ID="txtmobileno" CssClass="form-control" AutoPostBack="True" />
                                    <%--  <asp:RequiredFieldValidator runat="server" ControlToValidate="username"
                                                CssClass="text-danger" ErrorMessage="The User Name field is required." />--%>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Button runat="server" OnClick="generateotp" Text="Generate OTP" CssClass="btn btn-default" />
                                    <%--<div class="col-md-10">--%>
                                    <asp:TextBox ID="txtOTP" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator runat="server" ControlToValidate="txtOTP"
                                          CssClass="text-danger" ErrorMessage="The Otp is required." />--%>
                                    <%--</div>--%>
                                </div>
                            </div>
                            <div class="form-group row">

                                <div class="col-md-12">
                                    <asp:Button runat="server" ID="btnverfyotp" OnClick="Verifyotp" Visible="false" Text="Verify OTP" CssClass="btn btn-default" />
                                </div>
                            </div>


                            <div class="form-group row">

                                <div class="col-md-12">
                                    <asp:Label runat="server" ID="lblMessage" CssClass="text-danger" Font-Bold="False" ForeColor="#FF3300"></asp:Label>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>


            </div>
        </div>

    </div>






</asp:Content>
