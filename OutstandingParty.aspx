﻿<%@ Page Title="Outstanding" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OutstandingParty.aspx.cs" Inherits="GPGSOLASPNET.OutstandingParty" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.4000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <link href="Content/DataTables/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <link href="Content/DataTables/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
  
    <script src="Scripts/DataTables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="Scripts/DataTables/dataTables.responsive.min.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
                     

            $('#<%=grdviewnew.ClientID%>').DataTable({
                "paging": true, "ordering": true, "searching": true,
                 "responsive": true,
                "language": {
                  //  "infoEmpty": "No data available in table"//Change your default empty table message
                },
            });
            
        });
    </script>
 
    <div class="row">
        <div class="col-md-12">
            <br />
            <div class="panel panel-primary list-panel" id="list-panel">
                <div class="panel-heading list-panel-heading">
                    <h1 class="panel-title list-panel-title">Outstanding</h1>
                </div>
                <br />
                <div class="panel-body">



                    <div class="row" style="font-size: inherit" >
                        <div class="col-md-12">
                            <%--<div class="form-group row">
                                <asp:RadioButtonList ID="rbFormat" runat="server" RepeatDirection="Horizontal" CellPadding="2" CellSpacing="2">
                                    <asp:ListItem Text="Word" Value="Word" />
                                    <asp:ListItem Text="Excel" Value="Excel" />
                                    <asp:ListItem Text="PDF" Value="PDF" Selected="True" />
                                    <asp:ListItem Text="CSV" Value="CSV" />
                                </asp:RadioButtonList>


                            </div>--%>

                          <%--  <div class="form-group row">
                                <asp:Label runat="server" AssociatedControlID="drpstate" CssClass="control-label col-md-2">State</asp:Label>
                                <div class="col-md-10">
                                    <asp:DropDownList ID="drpstate" runat="server" CssClass="form-control" AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-group row">
                                <asp:Label runat="server" AssociatedControlID="drpcity" CssClass="control-label col-md-2">City</asp:Label>
                                <div class="col-md-10">
                                    <asp:DropDownList ID="drpcity" runat="server" CssClass="form-control" AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                            </div>

                          --%> 
                            <div class="form-group row">
                                <asp:Label runat="server" AssociatedControlID="drpparty" CssClass="control-label col-md-2">Party</asp:Label>
                                <div class="col-md-10">
                                    <asp:DropDownList ID="drpparty" runat="server" CssClass="form-control" AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                            </div>

<%--                            <div class="form-group row">
                                <asp:Label runat="server" AssociatedControlID="drpbroker" CssClass="control-label col-md-2">Broker</asp:Label>
                                <div class="col-md-10">
                                    <asp:DropDownList ID="drpbroker" runat="server" CssClass="form-control" AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-group row">
                                <asp:Label runat="server" AssociatedControlID="drpsaleperson" CssClass="control-label col-md-2">Salesperson</asp:Label>
                                <div class="col-md-10">
                                    <asp:DropDownList ID="drpsaleperson" runat="server" CssClass="form-control" AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-group row">
                                <asp:Label runat="server" AssociatedControlID="drpsaletype" CssClass="control-label col-md-2">Salesperson</asp:Label>
                                <div class="col-md-10">
                                    <asp:DropDownList ID="drpsaletype" runat="server" CssClass="form-control" AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-group row">
                                <asp:Label runat="server" AssociatedControlID="lblODDays" CssClass="control-label col-md-2">O/D Days</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="lblODDays" CssClass="form-control" Text="0" />
                                    <br />
                                </div>
                            </div>

                            <div class="form-group row">
                                <asp:Label runat="server" AssociatedControlID="lblOSDays" CssClass="control-label col-md-2">O/S Days</asp:Label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="lblOSDays" CssClass="form-control" Text="0" />
                                    <br />
                                </div>
                            </div>

                         <div class="form-group row">

                                <asp:Label runat="server" AssociatedControlID="rdyes" CssClass="control-label col-md-2">Page Break On Party</asp:Label>
                                <div class="col-md-10">
                                    <label class="radio-inline">
                                        <asp:RadioButton ID="rdyes" runat="server" Text="Yes" GroupName="type_bar_style" /></label>
                                    <label class="radio-inline">
                                        <asp:RadioButton ID="rdno" runat="server" Text="No" GroupName="type_bar_style" Checked="true" /></label>
                                    <br />

                                </div>
                            </div>--%>



                        </div>
                        <%--<div class="col-md-8">
                            <iframe src="ReportViewer.aspx" frameborder="0" height="100%" scrolling="yes" width="100%" style="height: -webkit-fill-available; width: -webkit-fill-available;"></iframe>
                        </div>--%>
                    </div>
                    <div class="row" style="font-size: smaller">
                        <div class="col-md-12">
                            <asp:Button ID="btnview" CssClass="btn btn-primary" Text="View" runat="server" OnClick="View" />
                            
                            &nbsp;<asp:Button ID="btnExport" CssClass="btn btn-primary" Text="Export" runat="server" OnClick="Export"/>

                            
                        </div>
                    </div>
                    <div>

                    </div>
                      <div>

                    </div>
                     <div>

                         <%--CssClass="dt-responsive"--%>
                    <asp:GridView ID="grdviewnew" runat="server" ShowFooter="true"  HeaderStyle-BackColor="#99ccff" FooterStyle-BackColor="#99ccff"
                        AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" Width="100%" Font-Size="X-Small" Font-Bold="true" HorizontalAlign="Left" >
                        <Columns>
                            <asp:TemplateField HeaderText="Trj.Type" SortExpression="TRJ_TYPE" ItemStyle-HorizontalAlign="Left" >

                                <ItemTemplate>
                                    <asp:Label ID="lblTRJ_TYPE" runat="server" Text='<%# Bind("TRJ_TYPE") %>' Enabled="false" Width="70px"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="70px" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>

                             <asp:TemplateField HeaderText="PARTY" SortExpression="PARTY_NAME" Visible="false">

                                <ItemTemplate>
                                    <asp:Label ID="lblPARTY_NAME" runat="server" Text='<%# Bind("PARTY_NAME") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="0px" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Bill NO" SortExpression="DOC_NO" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:label ID="lblDOC_NO" runat="server" Text='<%# Bind("DOC_NO") %>'  Width="70px"></asp:label>
                                </ItemTemplate>
                                <ItemStyle Width="70px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date" SortExpression="DOC_DT" ItemStyle-HorizontalAlign="Left">

                                <ItemTemplate>
                                    <asp:Label ID="lblDOC_DT" runat="server" Text='<%# Bind("DOC_DT", "{0:dd/MM/yyyy}") %>' Width="70px"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="70px" />
                            </asp:TemplateField>
                            

                            <asp:TemplateField HeaderText="Due Date" SortExpression="DUE_DT" ItemStyle-HorizontalAlign="Left">

                                <ItemTemplate>
                                    <asp:Label ID="lblDUE_DT" runat="server" Text='<%# Bind("DUE_DT", "{0:dd/MM/yyyy}") %>' Width="70px"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="70px" />
                            </asp:TemplateField>
                           
                               <asp:TemplateField HeaderText="Bill Amt" SortExpression="Bill_Net" ItemStyle-HorizontalAlign="Left">

                                <ItemTemplate>
                                    <asp:Label ID="lblBill_Bal" runat="server" Text='<%# Bind("Bill_Net") %>' Width="70px"></asp:Label>
                                </ItemTemplate>
                                   <ItemStyle Width="70px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="OS Amt" SortExpression="Bill_Bal" ItemStyle-HorizontalAlign="Left">

                                <ItemTemplate>
                                    <asp:Label ID="lblBAL_AMT" runat="server" Text='<%# Bind("Bill_Bal") %>' Width="50px"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="70px" />
                            </asp:TemplateField>
                               <asp:TemplateField HeaderText="O/D Days" SortExpression="OD_Days" ItemStyle-HorizontalAlign="Left">

                                <ItemTemplate>
                                    <asp:Label ID="OD_Days" runat="server" Text='<%# Bind("OD_Days") %>' Width="30px"></asp:Label>
                                </ItemTemplate>
                                   <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="O/S Days" SortExpression="Os_Days" ItemStyle-HorizontalAlign="Left">

                                <ItemTemplate>
                                    <asp:Label ID="Os_Days" runat="server" Text='<%# Bind("Os_Days") %>' Width="30px"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>

                               <asp:TemplateField HeaderText="Broker" SortExpression="Broker_Name"  ItemStyle-HorizontalAlign="Left" Visible="false">

                                <ItemTemplate>
                                    <asp:Label ID="lblBroker_Name" runat="server" Text='<%# Bind("Broker_Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="City" SortExpression="CITY_NAME"  Visible="false">

                                <ItemTemplate>
                                    <asp:Label ID="lblCITY_NAME" runat="server" Text='<%# Bind("CITY_NAME") %>' Width="80px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="State" SortExpression="STATE_NAME" ItemStyle-HorizontalAlign="Left"  Visible="false">

                                <ItemTemplate>
                                    <asp:Label ID="lblSTATE_NAME" runat="server" Text='<%# Bind("STATE_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Sale.Type" SortExpression="SaleType_Name"  ItemStyle-HorizontalAlign="Left"  Visible="false">

                                <ItemTemplate>
                                    <asp:Label ID="lblSaleType_Name" runat="server" Text='<%# Bind("SaleType_Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Saleperson" SortExpression="SALEPERSON_NAME"  ItemStyle-HorizontalAlign="Left"  Visible="false">

                                <ItemTemplate>
                                    <asp:Label ID="lblSALEPERSON_NAME" runat="server" Text='<%# Bind("SALEPERSON_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

<%--


                            <asp:TemplateField HeaderText="CONTACT_PERSON" SortExpression="CONTACT_PERSON" ItemStyle-HorizontalAlign="Left">

                                <ItemTemplate>
                                    <asp:Label ID="lblCONTACT_PERSON" runat="server" Text='<%# Bind("CONTACT_PERSON") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="MOBILE_NO" SortExpression="MOBILE_NO" ItemStyle-HorizontalAlign="Left">

                                <ItemTemplate>
                                    <asp:Label ID="lblMOBILE_NO" runat="server" Text='<%# Bind("MOBILE_NO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                         

                             <asp:TemplateField HeaderText="cobr_id" SortExpression="cobr_id" ItemStyle-HorizontalAlign="Left">

                                <ItemTemplate>
                                    <asp:Label ID="lblcobr_id" runat="server" Text='<%# Bind("cobr_id") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="OD_DAYS" SortExpression="OD_DAYS" ItemStyle-HorizontalAlign="Left">

                                <ItemTemplate>
                                    <asp:Label ID="lblOD_DAYS" runat="server" Text='<%# Bind("OD_DAYS") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                         
                            <asp:TemplateField HeaderText="CO_NAME" SortExpression="CO_NAME" ItemStyle-HorizontalAlign="Left">

                                <ItemTemplate>
                                    <asp:Label ID="lblCO_NAME" runat="server" Text='<%# Bind("CO_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="COBR_NAME" SortExpression="COBR_NAME" ItemStyle-HorizontalAlign="Left">

                                <ItemTemplate>
                                    <asp:Label ID="lblCOBR_NAME" runat="server" Text='<%# Bind("COBR_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BRAND_NAME" SortExpression="BRAND_NAME" ItemStyle-HorizontalAlign="Left">

                                <ItemTemplate>
                                    <asp:Label ID="lblBRAND_NAME" runat="server" Text='<%# Bind("BRAND_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>

                        </Columns>
                      
                    </asp:GridView>
                    
                      <%--  <asp:HyperLink runat="server" CssClass=" btn-link btn" ID="HyperLink1" ViewStateMode="Disabled" NavigateUrl="~/Contact.aspx">Go Back</asp:HyperLink>--%>

                   
<div class="row">
                        <div class="col-md-12">
                            <asp:HyperLink runat="server" CssClass=" btn-link btn" ID="Loginpage" ViewStateMode="Disabled" NavigateUrl="~/Contact.aspx">Go Back</asp:HyperLink>
                        </div>
                    </div>

                
                </div>



                </div>
            </div>

        </div>

      <%--  <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">
                        <iframe src="ReportViewer.aspx" frameborder="0" height="100%" scrolling="yes" width="100%" style="height: -webkit-fill-available; width: -webkit-fill-available;"></iframe>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>--%>
    </div>

</asp:Content>
