
using Common;
using CORE.Caching;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Core
{
    public class SecurityParam
    {
        public long SecPm_Id { get; set; }
        public string SecPm_Name { get; set; }
        public string Remark { get; set; }

    }
    /// <summary>
    /// Security service
    /// </summary>
    public partial class SecurityACCService : ISecurityACCService
    {
        #region Constants

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : show hidden records?
        /// </remarks>
        private const string Securities_ALL_KEY = "Security.all-{0}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string Securities_PATTERN_KEY = "Security.";

        #endregion

        #region Fields

        ICacheManager _cacheManager;
        #endregion

        #region Ctor


        public SecurityACCService(ICacheManager cacheManager

           )
        {
            this._cacheManager = cacheManager;

        }

        #endregion

        #region Methods



        public virtual IList<SecurityParam> GetAllSecurities()
        {
            string key = string.Format(Securities_ALL_KEY, false);
            return _cacheManager.Get(key, (Func<List<SecurityParam>>)(() =>
            {
                SessionManager.g_strQuery = "Select * from SecurityParam";
                DataTable dt = modMain.NewDatatable(SessionManager.g_strQuery, (string)SessionManager.g_strConnStringACC, null);

                var query = (from dr in dt.AsEnumerable()
                             select new SecurityParam()
                             {

                                 SecPm_Id = dr.Field<long>("SecPm_Id"),

                                 SecPm_Name = dr.Field<string>("SecPm_Name"),

                                 Remark = dr.Field<string>("Remark"),
                             }

                          );

                query = query.OrderBy(c => c.SecPm_Name).ThenBy(c => c.SecPm_Name);
                List<SecurityParam> securities = new List<SecurityParam>();
                securities = query.ToList();
                return securities;

            })



            );
        }



        public virtual SecurityParam GetSecurityById(long SecurityId)
        {
            if (SecurityId == 0)
            {
                return null;

            }
           

            return GetAllSecurities().Where(x => x.SecPm_Id == SecurityId).FirstOrDefault();
        }
        public virtual string GetSecurityIdByName(string Name)
        {
            var Id = from c in GetAllSecurities().AsEnumerable()
                     where c.SecPm_Name == Name
                     select c.SecPm_Id;

            string cn = Convert.ToString(Id.FirstOrDefault());
            return cn;
        }

        public bool AllowAccessToCurrentUser(out string strMassage)
        {
            string HDSerialNo = modMain.EncryptDecryptValue("", modMain.g_ParamClientId);//modMain.GetDriveSerial()
            string WinUser = modMain.GetWindowsUserName();
            SessionManager.g_strQuery = "Select IsNull(Count(*), 0) From UserInfo";
            Int32 TotalUsersAccessed =Convert.ToInt32(modMain.ExecuteScalarACC(SessionManager.g_strQuery));
            SessionManager.g_strQuery = "Select IsNull(Count(*), 0) " + " From UserInfo "
            + " Where HWId = '" + HDSerialNo + "' And UserName = '" + WinUser + "'";

            Int32 cnt = Convert.ToInt32(modMain.ExecuteScalarACC(SessionManager.g_strQuery));
            if (cnt == 0)
            {
                if (TotalUsersAccessed >= modMain.g_ParamAccessToUsers)
                {

                    strMassage = "Total users exceeds maximum users allowed !";

                    return false;
                }
                else if (TotalUsersAccessed < modMain.g_ParamAccessToUsers)
                {

                    SessionManager.g_strQuery = "Insert Into UserInfo Values ('" + HDSerialNo + "','" + WinUser + "','" + Environment.MachineName + "',GETDATE())";
                    modMain.ExecuteQueryACC(SessionManager.g_strQuery);
                    string tmp = modMain.EncryptValue(modMain.g_ParamClientId, modMain.Gc_systemPassword);
                    Common.LicensingInfo.GetCustomerLicenseInfo obj1;
                    obj1 = new Common.LicensingInfo.GetCustomerLicenseInfo();
                    string dtDate = modMain.EncryptValue(Convert.ToString(DateTime.Now.Date), modMain.g_ParamClientId);
                    string tmtime = modMain.EncryptValue(DateTime.Now.TimeOfDay.Hours.ToString("00") + ":" + DateTime.Now.TimeOfDay.Minutes, modMain.g_ParamClientId);
                    string AppVer = modMain.EncryptValue(modMain.Gc_strAppVersion, modMain.g_ParamClientId);

                    string FcYr = modMain.EncryptValue(SessionManager.g_strfcyr.Substring(0, 2) + "-" + SessionManager.g_strfcyr.Substring(2, 2), modMain.g_ParamClientId);

                    string UserCnt = modMain.EncryptValue(TotalUsersAccessed.ToString(), modMain.g_ParamClientId);
                    bool objStatus = obj1.UpdateCustomerLicenseInfo(modMain.g_WSServer, modMain.g_WSDatabase, modMain.g_WSUser, modMain.g_WSPassword, tmp, dtDate, tmtime, FcYr, AppVer, UserCnt);

                }
            }
            // 'get expiry date
            DateTime ValidTill = Convert.ToDateTime(modMain.g_ParamValidTill);

            // 'calc date after grace period
            var ExtendedDt = ValidTill.AddDays(modMain.g_ParamFreePeriod);

            // 'calc date after extended period
            var FurtherExtendedDt = ExtendedDt.AddDays(modMain.g_ParamExtendDays);
            modMain.g_SleepSeconds = 0;

            // 'chk whether current date is more thane date after extended period
            if (DateTime.Now.Date > FurtherExtendedDt)
            {

                string SoftStatus = modMain.EncryptValue(9.ToString(), modMain.g_ParamClientId);
                SessionManager.g_strQuery = "Update SecurityParam Set Remark = '" + SoftStatus + "' Where SecPm_Id = 3";
                modMain.ExecuteQueryACC(SessionManager.g_strQuery);
                modMain.g_ParamSoftwareStatus = Convert.ToBoolean(Convert.ToInt16(modMain.DecryptValue(SoftStatus, modMain.g_ParamClientId)) == 9 ? false : true);
                modMain.g_ParamIsTerminated = Convert.ToBoolean(Convert.ToInt16(modMain.DecryptValue(SoftStatus, modMain.g_ParamClientId)) == 0 ? true : false);
            } // 'chk whether date is more than date after grace period
            else if (DateTime.Now.Date > ExtendedDt)
            {
                Int32 AppDateDiff = DateTime.Now.Date.Subtract(ExtendedDt).Days;
                Int32 DiffDate = DateTime.Now.Date.Subtract(FurtherExtendedDt).Days;
                modMain.g_SleepSeconds = Convert.ToInt32(Math.Round(AppDateDiff / (double)modMain.g_ParamAfterExtensionWaitDays * modMain.g_ParamAfterExtensionWaitSec, 2));
                if (modMain.g_SleepSeconds > modMain.g_ParamMaxWaitSec)
                {
                    modMain.g_SleepSeconds = modMain.g_ParamMaxWaitSec;
                }


                if (modMain.g_ParamMsg2.Length > 0)
                {
                    if (modMain.g_ParamMsg2.IndexOf("@days") > 0)
                    {
                        modMain.g_ParamMsg2 = modMain.g_ParamMsg2.Substring(0, modMain.g_ParamMsg2.IndexOf("@days") - 1);

                        strMassage = modMain.g_ParamMsg2 + " " + DiffDate + " days";

                        return false;
                    }
                    else
                    {
                        strMassage = modMain.g_ParamMsg2;

                        return false;
                    }
                }
            }


            // 'chk whether date is more than exppiry date
            else if (DateTime.Now.Date > ValidTill)
            {
                strMassage = modMain.g_ParamMsg1;

                return false;

            }

            if (!modMain.g_ParamSoftwareStatus)
            {

                strMassage = modMain.g_ParamMsg4;

                return false;

            }

            strMassage = "";
            return true;
        }


        public object callweb(string tmp)
        {
            try
            {
                Common.LicensingInfo.GetCustomerLicenseInfo obj;
                obj = new Common.LicensingInfo.GetCustomerLicenseInfo();
                var dt = obj.CustomerLicenseInfo(modMain.g_WSServer, modMain.g_WSDatabase, modMain.g_WSUser, modMain.g_WSPassword, tmp).Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                SessionManager.LogError = ex.Message;
                return null;
            }
        }

        public void SyncCheckAndUpdate()
        {
            try
            {
                string tmp = GetSecurityById(10).Remark;
                short SyncInterval = Convert.ToInt16((string.IsNullOrEmpty(tmp) ? "0" : modMain.DecryptValue(tmp, modMain.g_ParamClientId)));
                modMain.g_ParamSyncDueDt = modMain.g_ParamLastUpdatedOn.AddDays(SyncInterval);
                string strSyncDueDt = modMain.g_ParamSyncDueDt.ToString();
                if (DateTime.Now.Date > modMain.g_ParamSyncDueDt)
                {


                    tmp = modMain.EncryptValue(modMain.g_ParamClientId, modMain.Gc_systemPassword);
                    string dtDate = modMain.EncryptValue(Convert.ToString(DateTime.Now.Date), modMain.g_ParamClientId);
                    string tmtime = modMain.EncryptValue(DateTime.Now.TimeOfDay.Hours.ToString("00") + ":" + DateTime.Now.TimeOfDay.Minutes, modMain.g_ParamClientId);



                    DataTable dt = (DataTable)callweb(tmp);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            if (Convert.ToInt32(modMain.DecryptValue(Convert.ToString(dt.Rows[0]["SyncActive"]), modMain.g_ParamClientId)) == 1)
                            {
                                var drRow = dt.Rows[0];
                                modMain.OpenCloseConnectionACC(true);
                                var Trans = modMain.g_sqlConnACC.BeginTransaction();
                                SessionManager.g_strQuery = Convert.ToString("Update SecurityParam Set Remark = '" + drRow["ClientId"] + "' Where SecPm_Id = 2");
                                modMain.ExecuteQueryACC(SessionManager.g_strQuery, Trans);
                                SessionManager.g_strQuery = Convert.ToString("Update SecurityParam Set Remark = '" + drRow["Status"] + "' Where SecPm_Id = 3");
                                modMain.ExecuteQueryACC(SessionManager.g_strQuery, Trans);
                                SessionManager.g_strQuery = Convert.ToString("Update SecurityParam Set Remark = '" + drRow["NoOfUsers"] + "' Where SecPm_Id = 4");
                                modMain.ExecuteQueryACC(SessionManager.g_strQuery, Trans);
                                SessionManager.g_strQuery = Convert.ToString("Update SecurityParam Set Remark = '" + drRow["DueDate"] + "' Where SecPm_Id = 5");
                                modMain.ExecuteQueryACC(SessionManager.g_strQuery, Trans);
                                SessionManager.g_strQuery = Convert.ToString("Update SecurityParam Set Remark = '" + drRow["FreePeriod"] + "' Where SecPm_Id = 6");
                                modMain.ExecuteQueryACC(SessionManager.g_strQuery, Trans);
                                SessionManager.g_strQuery = Convert.ToString("Update SecurityParam Set Remark = '" + drRow["RestrictedGraceDaysUpto"] + "' Where SecPm_Id = 7");
                                modMain.ExecuteQueryACC(SessionManager.g_strQuery, Trans);
                                SessionManager.g_strQuery = Convert.ToString("Update SecurityParam Set Remark = '" + drRow["RestrictedGrace_IncrementDays"] + "' Where SecPm_Id = 8");
                                modMain.ExecuteQueryACC(SessionManager.g_strQuery, Trans);
                                SessionManager.g_strQuery = Convert.ToString("Update SecurityParam Set Remark = '" + drRow["RestrictedGrace_IncrementTime"] + "' Where SecPm_Id = 9");
                                modMain.ExecuteQueryACC(SessionManager.g_strQuery, Trans);
                                SessionManager.g_strQuery = Convert.ToString("Update SecurityParam Set Remark = '" + drRow["SyncInterval"] + "' Where SecPm_Id = 10");
                                modMain.ExecuteQueryACC(SessionManager.g_strQuery, Trans);


                                SessionManager.g_strQuery = Convert.ToString("Update SecurityParam Set Remark = '" + drRow["MaxSeconds"] + "' Where SecPm_Id = 12");
                                modMain.ExecuteQueryACC(SessionManager.g_strQuery, Trans);
                                if (Convert.ToString(drRow["Message1"]) != "")
                                {
                                    SessionManager.g_strQuery = Convert.ToString("Update SecurityParam Set Remark = '" + drRow["Message1"] + "" + "'" + " Where SecPm_Id = 13");
                                    modMain.ExecuteQueryACC(SessionManager.g_strQuery, Trans);
                                }

                                if (Convert.ToString(drRow["Message2"]) != "")
                                {
                                    SessionManager.g_strQuery = Convert.ToString("Update SecurityParam Set Remark = '" + drRow["Message2"] + "" + "'" + " Where SecPm_Id = 14");
                                    modMain.ExecuteQueryACC(SessionManager.g_strQuery, Trans);
                                }


                                SessionManager.g_strQuery = Convert.ToString("Update SecurityParam Set Remark = '" + drRow["Message3"] + "" + "'" + " Where SecPm_Id = 15");
                                modMain.ExecuteQueryACC(SessionManager.g_strQuery, Trans);


                                if (Convert.ToString(drRow["Message4"]) != "")
                                {
                                    SessionManager.g_strQuery = Convert.ToString("Update SecurityParam Set Remark = '" + drRow["Message4"] + "" + "'" + " Where SecPm_Id = 16");
                                    modMain.ExecuteQueryACC(SessionManager.g_strQuery, Trans);
                                }

                                if (drRow.Table.Columns.Contains("software"))
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(drRow["software"])))
                                    {
                                        SessionManager.g_strQuery = "IF NOT EXISTS(SELECT SecPm_Id FROM SecurityParam WHERE SecPm_Id=17)  BEGIN "
                                            + "  INSERT securityparam ( SecPm_Id, SecPm_Name, Remark, Created_By, Created_Dt, Updated_By, Updated_dt )  "
                                            + "  SELECT   17 AS SecPm_Id,'Variant' AS  SecPm_Name, '" + modMain.EncryptValue(Convert.ToString(drRow["software"]), modMain.g_ParamClientId) + "' AS Remark,1 AS Created_By,GETDATE() AS Created_Dt,1 AS Updated_By,GETDATE() AS Updated_dt "
                                            + "  END "
                                            + " ELSE "
                                            + "   BEGIN   Update SecurityParam Set Remark = '" + modMain.EncryptValue(Convert.ToString(drRow["software"]), modMain.g_ParamClientId) + "' Where SecPm_Id = 17 END";

                                        modMain.ExecuteQueryACC(SessionManager.g_strQuery, Trans);
                                    }
                                }

                                Trans.Commit();
                                modMain.OpenCloseConnectionACC(false);
                                Common.LicensingInfo.GetCustomerLicenseInfo obj1 = new Common.LicensingInfo.GetCustomerLicenseInfo();
                                string AppVer = modMain.EncryptValue(modMain.Gc_strAppVersion, modMain.g_ParamClientId);

                                string FcYr = modMain.EncryptValue(SessionManager.g_strfcyr.Substring(0, 2) + "-" + SessionManager.g_strfcyr.Substring(2, 2), modMain.g_ParamClientId);
                                SessionManager.g_strQuery = "Select IsNull(Count(*), 0) From UserInfo";
                                string UserCnt = modMain.ExecuteScalarACC(SessionManager.g_strQuery);
                                UserCnt = modMain.EncryptValue(UserCnt, modMain.g_ParamClientId);
                                bool objStatus = obj1.UpdateCustomerLicenseInfo(modMain.g_WSServer, modMain.g_WSDatabase, modMain.g_WSUser, modMain.g_WSPassword, tmp, dtDate, tmtime, FcYr, AppVer, UserCnt);
                                if (objStatus)
                                {
                                    SessionManager.g_strQuery = "Update SecurityParam Set Remark = '" + dtDate + "' Where SecPm_Id = 11";
                                    modMain.ExecuteQueryACC(SessionManager.g_strQuery);
                                }

                                _cacheManager.Remove("Securities_ALL_KEY");
                            }
                        }
                    }


                }
            }
            catch (SqlException sqlEx)
            {
                var fail = new Exception(sqlEx.Message);

                throw fail;
            }
            catch (Exception ex)
            {
                var fail = new Exception(ex.Message);

                throw fail;
            }
        }

        public byte Delete(SecurityParam Security)
        {
            throw new NotImplementedException();
        }

        public byte InsertOrUpdate(SecurityParam Security, modMain.enmHistoryType mode)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

   
}