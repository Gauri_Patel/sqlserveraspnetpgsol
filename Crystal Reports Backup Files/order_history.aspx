﻿<%@ Page Title="Order History" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="order_history.aspx.cs" Inherits="GPGSOLASPNET.order_history" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <link href="Content/DataTables/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <link href="Content/DataTables/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
  
    <script src="Scripts/DataTables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="Scripts/DataTables/dataTables.responsive.min.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

          

            $('#<%=grdviewnew.ClientID%>').DataTable({
                "paging": true, "ordering": true, "searching": true,
                 "responsive": true,
                "language": {
                  //  "infoEmpty": "No data available in table"//Change your default empty table message
                },
            });
            
        });
    </script>
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-primary " id="list-panel">
                <div class="panel-heading ">
                    <h1 class="panel-title ">Order History</h1>
                </div>

                <div class="panel-body">


                    <asp:GridView ID="grdviewnew" runat="server" CssClass="dt-responsive"
                        AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" Width="100%" >
                        <Columns>


                            <asp:TemplateField HeaderText="ORDBK NO" SortExpression="ORDBK_NO" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblavlqty" runat="server" Text='<%# Bind("ORDBK_NO") %>' NavigateUrl='<%#Eval("order_id","Order_history_Summary.aspx?order_id=" + Eval("order_id") + "")%>' Target="_self"></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="order_id" SortExpression="order_id" ItemStyle-HorizontalAlign="Left" Visible="false">

                                <ItemTemplate>
                                    <asp:Label ID="lblsize" runat="server" Text='<%# Bind("order_id") %>' Enabled="false"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="COBR_ID" SortExpression="COBR_ID" Visible="false">

                                <ItemTemplate>
                                    <asp:Label ID="COBR_ID" runat="server" Text='<%# Bind("COBR_ID") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ORDBK DT" SortExpression="ORDBK_DT" ItemStyle-HorizontalAlign="Left">

                                <ItemTemplate>
                                    <asp:Label ID="txtqty" runat="server" Text='<%# Bind("ORDBK_DT", "{0:dd/MM/yyyy}") %>' Width="80px"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PARTY_KEY" SortExpression="PARTY_KEY" Visible="False">

                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("PARTY_KEY") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Delivery DT" SortExpression="DLV_DT" ItemStyle-HorizontalAlign="Left">

                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("DLV_DT", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Client" SortExpression="PARTY_NAME" ItemStyle-HorizontalAlign="Left">

                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("PARTY_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CONSIGNEE" SortExpression="CONSIGNEE" ItemStyle-HorizontalAlign="Left">

                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("CONSIGNEE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type" SortExpression="BROKER_KEY" Visible="False" ItemStyle-HorizontalAlign="Left">

                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("BROKER_KEY") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BROKER" SortExpression="BROKER_NAME" ItemStyle-HorizontalAlign="Left">

                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("BROKER_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="REMK" SortExpression="REMK" ItemStyle-HorizontalAlign="Left">

                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("REMK") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>
                      
                    </asp:GridView>
                    
                        <asp:HyperLink runat="server" CssClass=" btn-link btn" ID="Loginpage" ViewStateMode="Disabled" NavigateUrl="~/Contact.aspx">Go Back</asp:HyperLink>

                   

                
                </div>

                

            </div>


        </div>

    </div>
</asp:Content>
