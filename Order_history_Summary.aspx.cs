﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Common;

namespace GPGSOLASPNET
{
    public partial class Order_history_Summary : System.Web.UI.Page
    {
        String strquery = "";
      
        protected void Page_Load(object sender, EventArgs e)
        {

         lblorderid.Text = Request.QueryString["ORDER_ID"];
            
            try
            {
                if (!Page.IsPostBack)
                {
                    Grid_fill();
                }

             
            }
            catch (Exception ex)
            {
                modMain.ShowMessage(ex.Message, this);
            }


        }

        void Grid_fill()
        {
            try
            {
                strquery = "EXEC ASP_SPMODULE  @NMODE = 'OrdHistoryFill',@cdoc_key = '" + lblorderid.Text + "'";
                modMain.gridviewbind(strquery, SessionManager.g_strConnString, GridviewSize);
               
                DataTable dt = new DataTable();
                dt = ((DataSet)GridviewSize.DataSource).Tables[0];
                dt.TableName = "table1";
                GridviewSize.HeaderRow.TableSection = TableRowSection.TableHeader;

              

            }
            catch (SqlException ex)
            {
                modMain.ShowMessage(ex.Message, this);
            }
           
        }
      
        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Order_history_Summary.aspx?order_id=" + lblorderid.Text + "");
        }
    }
}