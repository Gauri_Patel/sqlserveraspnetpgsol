﻿using System;
using System.Net.Mail;
using System.Web.UI.WebControls;

namespace Common

{
    public static class SMSEMAIL
    {
        public static string sendMailMessage(string host_name, string from, string password, string recepient, string bcc, string cc, string subject, string body,ref string strmsg, string attachment)
        {
            string mailResponse = "";
            // Instantiate a new instance of MailMessage
             strmsg = "";
            MailMessage mMailMessage = new MailMessage();
            try
            {
                // Set the sender address of the mail message
                mMailMessage.From = new MailAddress(from);

                // Set the recepient address of the mail message
                // dim mailaddress as new listitem    	
                // mailaddress = recepient.Split(",")
                // for i = 0 to mailaddress.count-1
                // mMailMessage.To.Add(New MailAddress(mailaddress(i).trim))
                // next i
                // Split string based on spaces
                string[] mailaddress = recepient.Split(new char[] { ',' });

                // Use For Each loop over words and display them
                //  string word;
                foreach (var word in mailaddress)
                {
                    if (word.Trim() != "")
                        mMailMessage.To.Add(new MailAddress(word.Trim()));
                }

                // mMailMessage.To.Add(New MailAddress(recepient))
                // mMailMessage.To.Add(New MailAddress("kalpesh@marineelectricals.com"))
                // Check if the bcc value is nothing or an empty string

                if (bcc != null & bcc != string.Empty)
                {
                    // Set the Bcc address of the mail message
                    mailaddress = bcc.Split(new char[] { ',' });

                    foreach (var word in mailaddress)
                    {
                        if (word.Trim() != "")
                            mMailMessage.Bcc.Add(new MailAddress(word.Trim()));
                    }
                }


                // Check if the cc value is nothing or an empty value
                if (cc != null & cc != string.Empty)
                {
                    // Set the CC address of the mail message
                    mailaddress = cc.Split(new char[] { ',' });

                    foreach (var word in mailaddress)
                    {
                        if (word.Trim() != "")
                            mMailMessage.CC.Add(new MailAddress(word.Trim()));
                    }
                }

                // Set the subject of the mail message
                mMailMessage.Subject = subject;
                // Set the body of the mail message
                mMailMessage.Body = body;

                // Set the format of the mail message body as HTML
                mMailMessage.IsBodyHtml = true;
                // Set the priority of the mail message to normal
                mMailMessage.Priority = MailPriority.Normal;

                // mMailMessage.Attachments.Add(New System.Net.Mail.Attachment(attachment))
                if (attachment != null & attachment != string.Empty)
                {
                    string[] attachments = attachment.Split(new char[] { ',' });
                    // Use For Each loop over words and display them
                    //  string word1;
                    foreach (var word1 in attachments)
                        mMailMessage.Attachments.Add(new System.Net.Mail.Attachment(word1.Trim()));
                }
                // Instantiate a new instance of SmtpClient
                SmtpClient mSmtpClient = new SmtpClient();
                // mSmtpClient.EnableSsl = False
                mSmtpClient.EnableSsl = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["EnableSsl"]);
                mSmtpClient.Host = host_name;
                mSmtpClient.Credentials = new System.Net.NetworkCredential(from, password);
                mSmtpClient.Port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["smtpPort"]);
                // Send the mail message
                if (body != "")
                {
                    mSmtpClient.Send(mMailMessage);
                    mailResponse = "SUCCESS || Mail send Successfully.";
                }
                else
                    strmsg = "No Data Available To send";
            }
            catch (Exception Exc)
            {

                strmsg = Exc.Message;
            }
            mMailMessage.Dispose();
           
            return mailResponse;
        }
    }
}