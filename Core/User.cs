﻿using Common;
using Core;
using CORE.Caching;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Core
{
    public partial class USER
    {

        public USER()
        {

        }

        public long USER_ID { get; set; }
        public string USER_NAME { get; set; }
        public string USER_PWD { get; set; }
        public string DEPT_KEY { get; set; }
        public string DEGN_KEY { get; set; }
        public string USER_ADD { get; set; }
        public string TEL_NO { get; set; }
        public string CELL_NO { get; set; }
        public string EMAIL_ID { get; set; }
        public string REMK { get; set; }
        public Nullable<System.DateTime> LOGINDATETIME { get; set; }
        public Nullable<System.DateTime> LOGOUTDATETIME { get; set; }
        public string STATUS { get; set; }
        public Nullable<long> CREATED_BY { get; set; }
        public Nullable<System.DateTime> CREATED_DT { get; set; }
        public Nullable<long> UPDATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_DT { get; set; }
        public string EDIT_STATUS { get; set; }
        public string USER_ROLE { get; set; }
        public string COMP_NAME { get; set; }
        public string COBR_ID { get; set; }
        public string ALL_COBR { get; set; }
        public string MPOS_FLG { get; set; }
        public string USER_TYPE { get; set; }
        public string UserGrp_Id { get; set; }
        public string LOGIN_TYPE { get; set; }
        public byte[] THUMB_SAMPLE { get; set; }
        public string BK_DT_ENT { get; set; }
        public string SERIES_CHG { get; set; }
        public string RATE_CHG { get; set; }
        public Nullable<long> BK_DT_DAYS { get; set; }


    }

    public partial class USERPARAM
    {
        public long USERPM_ID { get; set; }
        public string USERPM_NAME { get; set; }
        public string REMARK { get; set; }
        public Nullable<long> CREATED_BY { get; set; }
        public Nullable<System.DateTime> CREATED_DT { get; set; }
        public Nullable<long> UPDATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_DT { get; set; }
        public string CoBr_Id { get; set; }
    }
    public class UserService : IDisposable
    {
        #region Constants

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : show hidden records?
        /// </remarks>
        private const string USERS_ALL_KEY = "User.all-{0}";
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : show hidden records?
        /// </remarks>
        private const string USERSPARAM_ALL_KEY = "UserPARAM.all-{0}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string USERS_PATTERN_KEY = "User.";

        #endregion
        private bool disposedValue;
        private ISecurityACCService _SecurityService;
        ICacheManager _cacheManager;
        public UserService()
        {
            
            this._cacheManager = new PerRequestCacheManager();
            this._SecurityService = new SecurityACCService(_cacheManager);


        }
        public DataTable getLoginData(string _userName, string _PassWord, string _custfix, string _custtype)
        {
            String strQuery = "EXEC ASP_SPMODULE  @NMODE='getLoginData',@userName='" + _userName + "' , @USER_PWD = '" + _PassWord + "',@CUSERTY='" + _custtype + "',@CUSERPREFIX='" + _custfix + "'";
            return modMain.NewDatatable(strQuery, SessionManager.g_strConnString, null);
        }

        public DataTable getLoginID(string _userName, string _custfix, string _custtype)
        {
            String strQuery = "EXEC ASP_SPMODULE @NMODE='getLoginID',@userName='" + _userName + "',@CUSERTY='" + _custtype + "',@CUSERPREFIX='" + _custfix + "'";
            return modMain.NewDatatable(strQuery, SessionManager.g_strConnString, null);
        }
        public bool IsValidUser(string UserName, string Password, out string strMessage)
        {

            bool IsValid = false;
            SessionManager.g_strUserId = "0";
            modMain.g_blnSystemUser = false;
            modMain.g_blnSuperUser = false;
            modMain.g_blnAllowSpecific = false;
            if ((UserName.ToUpper() ?? "") == modMain.Gc_systemUser)
            {
                if ((Password ?? "") == modMain.Gc_PrasystPassword) // gc_systemPassword
                {
                    SessionManager.g_strUsername = modMain.Gc_systemUser;
                    SessionManager.g_strUserId = "9999";
                    modMain.g_blnSystemUser = true;

                    IsValid = true;
                }
                else
                {
                    goto Last;

                }
            }
            else
            {


                if ((UserName.ToUpper().Trim() ?? "") != modMain.Gc_systemUser)
                {
                    var Security = _SecurityService.GetSecurityById(1);

                    if (Security.Remark != "1")
                    {
                        strMessage = "Product is not Licensed";

                        return false;

                    }
                    Security = _SecurityService.GetSecurityById(2);
                    if (string.IsNullOrEmpty(Security.Remark))
                    {
                        strMessage = "Client Id is not specified!";

                        return false;
                    }
                }
                SessionManager.g_strQuery = "delete from applnversion where applnversion = ''";
                modMain.ExecuteQuery(SessionManager.g_strQuery);
                SessionManager.g_strQuery = "Select ApplnVersion From ApplnVersion  Order By ApplnVersion Desc";
                var applnversion = modMain.ExecuteScalar(SessionManager.g_strQuery);


                if (applnversion.ToString().Length > 0)
                {
                    // Dim dtExpDt As DateTime = CDate(EncryptDecryptValue(dtVersion.Rows(0)("VersionVal"), "0"))
                    if (string.Compare(modMain.Gc_strAppVersion, Convert.ToString(applnversion), true) != 0)
                    {
                        strMessage = "WRONG VERSION ! " + "CURRENT VERSION ==> " + applnversion + ""
                             + "Please contact your system administrator";

                        return false;
                    }
                }


                if ((UserName.ToUpper() ?? "") == (modMain.Gc_strSpecUser.ToUpper() ?? "") && (Password ?? "") == (modMain.Gc_strSpecPwd ?? ""))
                {
                    modMain.g_blnAllowSpecific = true;

                }

                var User = new USER();
                if (modMain.g_blnAllowSpecific)
                {

                    User = (from u in GetAllUsers().AsEnumerable<USER>()
                            where u.USER_NAME.ToUpper() == "ADMIN"
                            select u).FirstOrDefault();
                }
                else
                {
                    User  = (from u in GetAllUsers().AsEnumerable<USER>()
                                   where u.USER_NAME.ToUpper() == UserName.ToUpper()
                                   select u).FirstOrDefault();
                }

                if (User != null)
                {
                    if (_SecurityService.GetAllSecurities().Any(s => (s.SecPm_Id > 2 && s.SecPm_Id < 12) && (s.Remark ?? "") == ""))
                    {

                        strMessage = "License is not registered, " + Environment.NewLine + " Click Activate Now to register";

                        return false;
                    }

                    if (User.MPOS_FLG == "1" || User.MPOS_FLG == "2")
                    {
                        strMessage = "Unauthorised user !";

                        return false;
                    }
                    else if (User.MPOS_FLG == "0" || User.MPOS_FLG == "3")
                    {
                        string strEncDec = modMain.EncryptDecryptValue(User.USER_PWD, "0");
                        if ((Password ?? "") != (strEncDec ?? ""))
                        {
                            goto Last;
                        }
                        else
                        {
                            if ((User.LOGIN_TYPE ?? "") == "B")
                            {

                                strMessage = "You are not authorized to Login through Password !";

                                return false;
                            }

                            // modMain.g_User = User;


                            modMain.g_ParamClientId = _SecurityService.GetSecurityById(2).Remark;
                            modMain.g_ParamClientId = modMain.DecryptValue(modMain.g_ParamClientId, modMain.Gc_systemPassword);


                            if (modMain.g_ParamClientId == "")
                            {

                                strMessage = "Invalid application Id specified !";

                                return false;
                            }
                            modMain.g_ParamCheckLicense = _SecurityService.GetSecurityById(1).Remark == "1";
                            if (modMain.g_ParamCheckLicense && !modMain.g_blnSystemUser)
                            {
                                UpdateLicenseParameters();
                            }
                            if (!_SecurityService.AllowAccessToCurrentUser(out strMessage))
                            {
                                return false;
                            }
                            _SecurityService.SyncCheckAndUpdate();

                            UpdateSystemParameters(User);

                            SessionManager.g_strUserId = User.USER_ID.ToString();
                            SessionManager.g_strUsername = User.USER_NAME;
                            if (User.USER_TYPE == "0")
                            {
                                modMain.g_blnSuperUser = true;
                            }

                            SessionManager.g_strQuery = "UPDATE USERS SET LOGINDATETIME =getdate() , "
                                + " COMP_NAME = '" + modMain.ReplaceStringChar(Environment.MachineName) + "'" + " WHERE USER_ID = '" + SessionManager.g_strUserId + "'";
                            modMain.ExecuteQuery(SessionManager.g_strQuery);

                            IsValid = true;
                        }


                    }
                }
                else
                {

                    goto Last;


                }


            }


        Last:
            if (IsValid)
            {
                strMessage = "";
                               
                return IsValid;
            }
            else
            {

                strMessage = "Invalid user or Password specified !";

                return false;
            }

        }



        private void UpdateSystemParameters(USER uSER)
        {
            try
            {



                var query = GetAllUserParam();


                if (query.Count() > 0)
                {
                    foreach (USERPARAM up in query)
                    {



                        switch (up.USERPM_ID)
                        {
                            case 1:
                                modMain.g_ParamComm = up.REMARK;
                                break;
                            case 2:
                                {
                                    modMain.g_ParamMRPUp = Convert.ToDouble(up.REMARK); break;
                                }
                            case 3:
                                {
                                    modMain.g_ParamCity = Convert.ToString(up.REMARK); break;
                                }
                            case 4:
                                {
                                    modMain.g_ParamAllowNegativeStock = Convert.ToChar(up.REMARK); break;
                                }
                            case 5:
                                {
                                    modMain.G_BarcodePrinter = Convert.ToChar(up.REMARK); break;
                                }
                            case 6:
                                {
                                    modMain.g_ParamFabStkLoc = Convert.ToString(up.REMARK); break;
                                }
                            case 7:
                                {
                                    modMain.g_ParamDisplayMsg = Convert.ToString(up.REMARK); break;
                                }
                            case 8:
                                {
                                    modMain.g_ParamVoiceOn = Convert.ToString(up.REMARK); break;
                                }
                            case 9:
                                {
                                    modMain.g_ParamMarkDownSizeMRP = Convert.ToString(up.REMARK); break;
                                    //  modMain.g_ParamMarkDownSizeMRP_temp = Convert.ToString(up.REMARK); break;
                                }
                            case 10:
                                {
                                    modMain.g_ParamProcRctStk = Convert.ToString(up.REMARK); break;
                                }
                            case 11:
                                {
                                    modMain.g_ParamDuplicateStyle = Convert.ToString(up.REMARK); break;
                                }
                            case 12:
                                {
                                    modMain.g_ParamShadeAllocation = Convert.ToString(up.REMARK); break;
                                }
                            case 13:
                                {
                                    modMain.g_ParamOrderReference = Convert.ToString(up.REMARK); break;
                                }
                            case 14:
                                {
                                    modMain.g_ParamSizeWiseMRP = Convert.ToString(up.REMARK); break;
                                }
                            case 15:
                                {
                                    modMain.g_ParamQCVerify = Convert.ToString(up.REMARK); break;
                                }
                            case 16:
                                {
                                    modMain.g_ParamPackRateFromMst = Convert.ToString(up.REMARK); break;
                                }
                            case 17:
                                {
                                    modMain.g_ParamDynamicSizePrn = Convert.ToString(up.REMARK); break;
                                }
                            case 18:
                                {
                                    modMain.g_ParamBranchWiseSeries = Convert.ToString(up.REMARK); break;
                                }
                            case 19:
                                {
                                    modMain.g_ParamAllowProductEdit = Convert.ToString(up.REMARK); break;
                                }
                            case 20:
                                {
                                    modMain.g_ParamBranchWiseAcc = Convert.ToString(up.REMARK); break;
                                }
                            case 21:
                                {
                                    modMain.g_ParamOrdNoFiFoPack = Convert.ToString(up.REMARK); break;
                                }
                            case 22:
                                {
                                    modMain.g_ParamFranchAcc = Convert.ToString(up.REMARK); break;
                                }
                            case 23:
                                {
                                    modMain.g_ParamEditMRP = Convert.ToString(up.REMARK); break;
                                }
                            case 24:
                                {
                                    modMain.g_ParamAllowSMS = Convert.ToString(up.REMARK); break;
                                }
                            case 25:
                                {
                                    if ((modMain.g_ParamAllowSMS ?? "") == "1")
                                    {
                                        modMain.g_ParamSMSUser = Convert.ToString(up.REMARK);
                                        modMain.g_ParamSMSUser = modMain.EncryptDecryptValue(modMain.g_ParamSMSUser, "0");
                                    }
                                    break;
                                }
                            case 26:
                                {
                                    if ((modMain.g_ParamAllowSMS ?? "") == "1")
                                    {
                                        modMain.g_ParamSMSPassword = Convert.ToString(up.REMARK);
                                        modMain.g_ParamSMSPassword = modMain.EncryptDecryptValue(modMain.g_ParamSMSPassword, "0");
                                    }
                                    break;
                                }
                            case 27:
                                {
                                    modMain.g_ParamUniqueBarCodeGen = Convert.ToChar(up.REMARK); break;
                                }
                            case 28:
                                {
                                    modMain.g_ParamExportDataWithoutBill = Convert.ToString(up.REMARK); break;
                                }
                            case 29:
                                {
                                    modMain.g_ParamStockChkOnTransaction = Convert.ToString(up.REMARK); break;
                                }
                            case 30:
                                {
                                    modMain.g_ParamPrintCompany = Convert.ToString(up.REMARK); break;
                                }
                            case 70:
                                {
                                    modMain.g_paramShowAllPendingBills = (Convert.ToString(up.REMARK) ?? "") == "1"; break;
                                }
                            case 31:
                                {
                                    modMain.g_ParamAllowRoadPermit = Convert.ToString(up.REMARK); break;
                                }
                            case 32:
                                {
                                    modMain.g_ParamAllowExcessQtyProcRcpt = Convert.ToString(up.REMARK); break;
                                }
                            case 33:
                                {
                                    modMain.g_ParamAutoBackup = Convert.ToString(up.REMARK); break;
                                }
                            case 34:
                                {
                                    modMain.g_ParamWgtInStyle = Convert.ToString(up.REMARK); break;
                                }
                            case 35:
                                {
                                    modMain.g_ParamMaintainSet = Convert.ToString(up.REMARK); break;
                                }
                            case 36:
                                {
                                    modMain.g_ParamJobApprvReqd = Convert.ToString(up.REMARK); break;
                                }
                            case 37:
                                {
                                    modMain.g_ParamSMSBillValidity = Convert.ToString(up.REMARK); break;
                                }
                            case 38:
                                {
                                    modMain.g_ParamBackupAt = Convert.ToString(up.REMARK); break;
                                }
                            case 39:
                                {
                                    modMain.g_ParamBankChargesId = Convert.ToInt32(up.REMARK);
                                    modMain.g_ParamBankChargesNm = "";
                                    if (modMain.g_ParamBankChargesId > 0)
                                    {
                                        //SessionManager.g_strQuery = "Select AccLed_Name  From AccLed Where AccLed_Id = " + modMain.g_ParamBankChargesId;
                                        //var BkChg = modAcc.AccNewDatatable(SessionManager.g_strQuery, modAcc.g_strConnStringAcc);
                                        //if (BkChg.Rows.Count > 0)
                                        //{
                                        //    modMain.g_ParamBankChargesNm = Convert.ToString(BkChg.Rows[0][0] + "");
                                        //}

                                        //BkChg = null;
                                    }
                                    break;
                                }
                            case 40:
                                {
                                    // g_ParamUsersAllowed = Asc(dtTable.Rows(i)("Remark"))
                                    modMain.g_ParamExcisePerc = Convert.ToDouble(up.REMARK); break;
                                }
                            case 41:
                                {
                                    modMain.g_ParamExciseAppbl = up.REMARK == "1" ? true : false; break;
                                }
                            case 42:
                                {
                                    modMain.g_ParamIsAdminUserOne = Convert.ToInt16((up.REMARK)); break;
                                }
                            case 43:
                                {
                                    modMain.g_ParamPayrollModule = (up.REMARK); break;
                                }
                            case 44:
                                {
                                    modMain.g_ParamWspEnable = up.REMARK == "1" ? true : false; ; break;
                                }
                            case 45:
                                {
                                    modMain.g_ParamPackCaption = up.REMARK; break;
                                }
                            case 46:
                                {
                                    modMain.g_ParamPartyCatBrandDiscEnable = true; break; // dtTable.Rows(i).Item("REMARK").ToString
                                }
                            case 47:
                                {
                                    modMain.g_ParamPrnDetail = up.REMARK; break;
                                }
                            case 48:
                                {
                                    modMain.g_paramBrand = up.REMARK; break;
                                }
                            case 49:
                                {
                                    modMain.g_paramBranchWiseDocNo = up.REMARK == "1" ? true : false; break;
                                }
                            case 50:
                                {
                                    modMain.g_ParamProdnPlan = up.REMARK == "1" ? true : false; break;
                                }
                            case 51:
                                {
                                    modMain.g_ParamMRPChgOnBarCdPrn = up.REMARK == "1" ? true : false; break;
                                }
                            case 52:
                                {
                                    modMain.g_paramCompanyWiseProdParty = up.REMARK == "1" ? true : false; break;
                                }
                            case 53:
                                {
                                    modMain.g_ParamPartyApproval = up.REMARK == "1" ? true : false; break;
                                }
                            case 54:
                                {
                                    modMain.g_ParamMultiplyByWeightForQty = up.REMARK == "1" ? true : false; break;
                                }
                            case 55:
                                {
                                    modMain.g_ParamMaintainStylePart = up.REMARK; break;
                                }
                            case 56:
                                {
                                    modMain.g_ParamRatioBasedQty = up.REMARK == "1" ? true : false; break;
                                }
                            case 57:
                                {
                                    modMain.g_ParamPaperReport = up.REMARK == "1" ? true : false; break;
                                }
                            case 58:
                                {
                                    modMain.g_ParamLotEnable = up.REMARK == "1" ? true : false; break;
                                }
                            case 59:
                                {
                                    modMain.g_ParamApproval = up.REMARK == "1" ? true : false; break;
                                }
                            case 60:
                                {
                                    modMain.g_ParamStoreItemSizeWise = up.REMARK == "1" ? true : false; break;
                                }
                            case 61:
                                {
                                    modMain.g_ParamCreateWorkOrder = up.REMARK == "1" ? true : false; break;
                                }
                            case 62:
                                {
                                    modMain.g_ParamShadeChgProcess = up.REMARK == "1" ? true : false; break;
                                }
                            case 63:
                                {
                                    modMain.g_ParamIncludeStkInTallyExp = up.REMARK == "1" ? true : false; break;
                                }
                            case 64:
                                {
                                    modMain.g_ParamShowMachineEmployeeInProcessTransfer = up.REMARK == "1" ? true : false; break;
                                }
                            case 65:
                                {
                                    modMain.g_ParamAllowDetailEditInPackingBarcode = up.REMARK == "1" ? true : false; break;
                                }
                            case 66:
                                {
                                    modMain.g_ParamLotWisePaking = up.REMARK == "1" ? true : false; break;
                                }
                            case 67:
                                {
                                    modMain.g_paramTransApproval = up.REMARK == "1" ? true : false; break;
                                }
                            case 68:
                                {
                                    modMain.g_paramWarrantyBarcode = up.REMARK == "1" ? true : false; break;
                                }
                            case 69:
                                {
                                    modMain.g_paramLockOnSale = up.REMARK; break;
                                }
                            case 71:
                                {
                                    modMain.g_paramWebSynchronization = up.REMARK == "1" ? true : false; break;
                                }
                            case 73:
                                {
                                    modMain.g_paramWIPAutoCreate_SOParts = string.Compare(Convert.ToString(up.REMARK), "1", true) == 0; break;
                                }
                            case 74:
                                {
                                    modMain.g_paramGSTEnabled = string.Compare(Convert.ToString(up.REMARK), "1", true) == 0; break;
                                }
                            case 75:
                                {
                                    modMain.g_paramOrdShowStk = string.Compare(Convert.ToString(up.REMARK), "1", true) == 0; break;
                                }
                            case 76:
                                {
                                    modMain.g_paramLineDiscEnabled = string.Compare(Convert.ToString(up.REMARK), "1", true) == 0; break;
                                }
                            case 77:
                                {
                                    modMain.g_ParamUS9Rate = (Convert.ToString(up.REMARK)); break;
                                }
                            case 78:
                                {
                                    modMain.g_paramDistrubuterModule = string.Compare(Convert.ToString(up.REMARK), "1", true) == 0; break;
                                }
                            case 79:
                                {
                                    modMain.g_paramSzSetBrcodeStkEnabled = string.Compare(Convert.ToString(up.REMARK), "1", true) == 0; break;
                                }
                            case 80:
                                {
                                    modMain.g_paramAllowEditBill_Pack = string.Compare(Convert.ToString(up.REMARK), "1", true) == 0; break;
                                }
                            case 81:
                                {
                                    modMain.g_paramSMSExpired = string.Compare(Convert.ToString(up.REMARK), "1", true) == 0; break;
                                }
                            case 82:
                                {
                                    modMain.g_paramCoDivisionEnabled = string.Compare(Convert.ToString(up.REMARK), "1", true) == 0; break;
                                }
                            case 83:
                                {
                                    modMain.g_paramAllowProStkInstk = string.Compare(Convert.ToString(up.REMARK), "1", true) == 0; break;
                                }
                            case 84:
                                {
                                    modMain.g_paramProjSource = Convert.ToChar(Convert.ToString(up.REMARK)); break;
                                }
                            case 85:
                                {
                                    modMain.g_paramAllowBITool = string.Compare(Convert.ToString(up.REMARK), "1", true) == 0; break;
                                }
                            case 86:
                                {
                                    modMain.g_paramBIToolExtendedRights = string.Compare(Convert.ToString(up.REMARK), "1", true) == 0; break;
                                }
                            case 87:
                                {
                                    modMain.g_paramInterBrDispAgainstord = string.Compare(Convert.ToString(up.REMARK), "1", true) == 0; break;
                                }
                            case 88:
                                {
                                    modMain.g_paramEmailonLREntry = string.Compare(Convert.ToString(up.REMARK), "1", true) == 0; break;
                                }
                            case 89:
                                {
                                    modMain.g_paramValidateCodeLength = string.Compare(Convert.ToString(up.REMARK), "1", true) == 0; break;
                                }

                            case 90:
                                {
                                    modMain.g_blnPrdSrWiseMRPchng = string.Compare(Convert.ToString(up.REMARK), "1", true) == 0; break;
                                }
                            case 91:
                                {
                                    modMain.g_ParamShiftClosingTime = Convert.ToInt32((up.REMARK)); break;
                                }
                            case 92:
                                {
                                    modMain.g_ParamUniversalMCEfficiency = Convert.ToInt32((up.REMARK)); break;
                                }
                            case 93:
                                {
                                    modMain.g_paramblnValidateMachUnpack = string.Compare(Convert.ToString(up.REMARK), "1", true) == 0; break;
                                }
                            case 94:
                                {
                                    modMain.g_ParamLotShadeWise_Ord = string.Compare(Convert.ToString(up.REMARK), "1", true) == 0; break;
                                }
                            case 95:
                                {
                                    modMain.g_ParamLotprocesstransfer = string.Compare(Convert.ToString(up.REMARK), "1", true) == 0; break;
                                }
                            case 96:
                                {
                                    modMain.g_ParamPGSIMGDB = string.Compare(Convert.ToString(up.REMARK), "1", true) == 0; break;
                                }
                            default:
                                break;
                        }

                    }

                }



                modMain.g_Currentserverdate =Convert.ToDateTime(modMain.ExecuteScalar("select  GETDATE()"));
                if (modMain.g_blnSystemUser == false)
                {
                    modMain.g_BackDate = modMain.g_Currentserverdate.AddDays(-Convert.ToDouble(uSER.BK_DT_DAYS));
                    modMain.g_AllowBkDtEnt = (uSER.BK_DT_ENT == "1");
                    modMain.g_AllowSeriesChnage = (uSER.SERIES_CHG == "1");
                    modMain.g_AllowRateChnage = (uSER.RATE_CHG == "1");

                }



                //if ((modMain.g_ParamAllowSMS ?? "") == "1" & modMain.g_ParamSMSUser.Trim().Length != 0 & modMain.g_ParamSMSPassword.Trim().Length != 0)
                //{
                //    if (modMain.ConvertInt(modMain.ExecuteScalar("select count(*) from smslog where Convert(varchar(12),smsdate,103 )=Convert(varchar(12),getdate(),103 )")) == 0)
                //    {
                //        var SMS = new PGS.SendSMS(PGS.SendSMS.TransType.Manual);
                //        SMS.PartyTypeIs = PGS.SendSMS.PartyType.Manual;
                //        SMS.MobileNo = "9321020014";
                //        SMS.PartyKey = "";
                //        SMS.PartyName = "";
                //        SMS.DocumentKey = "";
                //        SMS.DisplayAlert = PGS.SendSMS.Notification.No;
                //        SMS.SMSAllow = PGS.SendSMS.Notification.Yes;
                //        SMS.TransNo = "";
                //        SMS.TransAmount = 0;
                //        SMS.TransDate = DateAndTime.Now.Date;
                //        SMS.SMSMessage = "" + Constants.vbCrLf + "*****" + Constants.vbCrLf + modMain.g_strCompanyName;
                //        SMS.SendSMSMessage();
                //    }
                //}

            }
            catch (Exception ex)
            {
                var fail = new Exception(ex.ToString());

                throw fail;
            }

        }


        private void UpdateLicenseParameters()
        {
            try
            {

                // 'Read all license related info in variables for ref

                if ((SessionManager.g_strUsername ?? "") != modMain.Gc_systemUser)
                {
                    foreach (SecurityParam param in _SecurityService.GetAllSecurities())
                    {
                        switch (param.SecPm_Id)
                        {

                            case 1:
                                {
                                    modMain.g_ParamCheckLicense = param.Remark == "1" ? true : false; break;
                                }
                            case 2:
                                {
                                    modMain.g_ParamClientId = modMain.DecryptValue(param.Remark, modMain.Gc_systemPassword); break;

                                }
                            case 17:
                                {

                                    modMain.g_ProductVariant = modMain.DecryptValue(Convert.ToString(param.Remark), modMain.g_ParamClientId); ; break;
                                }

                            case 3:
                                {

                                    modMain.g_ParamSoftwareStatus = (modMain.DecryptValue(Convert.ToString(param.Remark), modMain.g_ParamClientId) == "9" ? false : true);
                                    modMain.g_ParamIsTerminated = (modMain.DecryptValue(Convert.ToString(param.Remark), modMain.g_ParamClientId) == "0" ? true : false); break;
                                }
                            case 4:
                                {
                                    modMain.g_ParamAccessToUsers = Convert.ToInt16((modMain.DecryptValue(Convert.ToString(param.Remark), modMain.g_ParamClientId))); break;
                                }
                            case 5:
                                {
                                    modMain.g_ParamValidTill = modMain.DecryptValue(Convert.ToString(param.Remark), modMain.g_ParamClientId); break;
                                }
                            case 6:
                                {
                                    modMain.g_ParamFreePeriod = Convert.ToInt16((modMain.DecryptValue(Convert.ToString(param.Remark), modMain.g_ParamClientId))); break;
                                }
                            case 7:
                                {
                                    modMain.g_ParamExtendDays = Convert.ToInt16((modMain.DecryptValue(Convert.ToString(param.Remark), modMain.g_ParamClientId))); break;
                                }
                            case 8:
                                {
                                    modMain.g_ParamAfterExtensionWaitDays = Convert.ToInt16((modMain.DecryptValue(Convert.ToString(param.Remark), modMain.g_ParamClientId))); break;
                                }
                            case 9:
                                {
                                    modMain.g_ParamAfterExtensionWaitSec = (Convert.ToInt32(modMain.DecryptValue(Convert.ToString(param.Remark), modMain.g_ParamClientId)) * 1000); break;
                                }
                            case 10:
                                {
                                    modMain.g_ParamUpdateInterval = Convert.ToByte((modMain.DecryptValue(Convert.ToString(param.Remark), modMain.g_ParamClientId))); break;
                                }
                            case 11:
                                {
                                    modMain.g_ParamLastUpdatedOn = Convert.ToDateTime(modMain.DecryptValue(Convert.ToString(param.Remark), modMain.g_ParamClientId));
                                    modMain.g_ParamLastUpdatedOnEncry = Convert.ToString(param.Remark); break;
                                }
                            case 12:
                                {
                                    modMain.g_ParamMaxWaitSec = (Convert.ToInt32(modMain.DecryptValue(Convert.ToString(param.Remark), modMain.g_ParamClientId)) * 1000); break;
                                }


                            case 13:
                                {
                                    modMain.g_ParamMsg1 = Convert.ToString(param.Remark); break;
                                }
                            case 14:
                                {
                                    modMain.g_ParamMsg2 = Convert.ToString(param.Remark); break;
                                }
                            case 15:
                                {
                                    modMain.g_ParamMsg3 = Convert.ToString(param.Remark); break;
                                }
                            case 16:
                                {
                                    modMain.g_ParamMsg4 = Convert.ToString(param.Remark); break;
                                }
                            default:
                                break;
                        }

                    }


                }


            }
            catch (Exception ex)
            {
                var fail = new Exception(ex.Message);

                throw fail;
            }

        }

        public virtual IList<USER> GetAllUsers()
        {
            string key = string.Format(USERS_ALL_KEY, false);
            return _cacheManager.Get(key, () =>
            {
                SessionManager.g_strQuery = "Select * from USERS";
                DataTable dt = modMain.NewDatatable(SessionManager.g_strQuery, SessionManager.g_strConnString, null);

                var query = (from dr in dt.AsEnumerable()
                             select new USER()
                             {

                                 USER_ID = dr.Field<Int64>("USER_ID"),
                                 USER_NAME = dr.Field<string>("USER_NAME"),
                                 USER_PWD = dr.Field<string>("USER_PWD"),
                                 DEPT_KEY = dr.Field<string>("DEPT_KEY"),
                                 DEGN_KEY = dr.Field<string>("DEGN_KEY"),
                                 USER_ADD = dr.Field<string>("USER_ADD"),
                                 TEL_NO = dr.Field<string>("TEL_NO"),
                                 CELL_NO = dr.Field<string>("CELL_NO"),
                                 EMAIL_ID = dr.Field<string>("EMAIL_ID"),
                                 REMK = dr.Field<string>("REMK"),
                                 LOGINDATETIME = dr.Field<Nullable<System.DateTime>>("LOGINDATETIME"),
                                 LOGOUTDATETIME = dr.Field<Nullable<System.DateTime>>("LOGOUTDATETIME"),
                                 STATUS = dr.Field<string>("STATUS"),
                                 CREATED_BY = dr.Field<Nullable<long>>("CREATED_BY"),
                                 CREATED_DT = dr.Field<Nullable<System.DateTime>>("CREATED_DT"),
                                 UPDATED_BY = dr.Field<Nullable<long>>("UPDATED_BY"),
                                 UPDATED_DT = dr.Field<Nullable<System.DateTime>>("UPDATED_DT"),
                                 EDIT_STATUS = dr.Field<string>("EDIT_STATUS"),
                                 USER_ROLE = dr.Field<string>("USER_ROLE"),
                                 COMP_NAME = dr.Field<string>("COMP_NAME"),
                                 COBR_ID = dr.Field<string>("COBR_ID"),
                                 ALL_COBR = dr.Field<string>("ALL_COBR"),
                                 MPOS_FLG = dr.Field<string>("MPOS_FLG"),
                                 USER_TYPE = dr.Field<string>("USER_TYPE"),
                                 UserGrp_Id = dr.Field<string>("UserGrp_Id"),
                                 LOGIN_TYPE = dr.Field<string>("LOGIN_TYPE"),
                                 THUMB_SAMPLE = dr.Field<byte[]>("THUMB_SAMPLE"),
                                 BK_DT_ENT = dr.Field<string>("BK_DT_ENT"),
                                 SERIES_CHG = dr.Field<string>("SERIES_CHG"),
                                 RATE_CHG = dr.Field<string>("RATE_CHG"),
                                 BK_DT_DAYS = dr.Field<Nullable<long>>("BK_DT_DAYS"),

                             }

                              );

                query = query.OrderBy(c => c.USER_ID).ThenBy(c => c.USER_NAME);
                List<USER> securities = new List<USER>();
                securities = query.ToList();
                return securities;

            }



            );
        }


        public virtual IList<USERPARAM> GetAllUserParam()
        {
            string key = string.Format(USERSPARAM_ALL_KEY, false);
            return _cacheManager.Get(key, (Func<List<USERPARAM>>)(() =>
            {
                SessionManager.g_strQuery = "Select * from USERPARAM";
                DataTable dt = modMain.NewDatatable(SessionManager.g_strQuery, (string)SessionManager.g_strConnStringACC, null);

                var query = (from dr in dt.AsEnumerable()
                             select new USERPARAM()
                             {
                                 USERPM_ID= dr.Field<long>("USERPM_ID"),
                                 USERPM_NAME = dr.Field<string>("USERPM_NAME"),
                                 REMARK = dr.Field<string>("REMARK"),
                                 CREATED_BY = dr.Field<long?>("CREATED_BY"),
                                 CREATED_DT = dr.Field<DateTime?>("CREATED_DT"),
                                 UPDATED_BY = dr.Field<long?>("UPDATED_BY"),
                                 UPDATED_DT = dr.Field<DateTime?>("UPDATED_DT"),
                                 CoBr_Id = dr.Field<string>("CoBr_Id"),


                             }

                              );

                query = query.OrderBy(c => c.USERPM_ID).ThenBy(c => c.USERPM_NAME);
                List<USERPARAM> securities = new List<USERPARAM>();
                securities = query.ToList();
                return securities;

            })



            );
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    //  this.Dispose();
                }
                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~User()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
