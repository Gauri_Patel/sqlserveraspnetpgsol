﻿using Common;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Data;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using System.Web;

namespace GPGSOLASPNET
{
    public partial class OutstandingParty : System.Web.UI.Page
    {
        //TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        //TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        //ReportDocument cryRpt;
        //Tables CrTables;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated) // if the user is already logged in
            {
                Response.Redirect(FormsAuthentication.LoginUrl);
            }
            if (!Page.IsPostBack)
            {
                combofill();
                if (modMain.g_strpartyfill == "C")
                {
                    drpparty.SelectedValue = SessionManager.g_strUserId;
                    drpparty.Text = SessionManager.g_strUsername;

                }

                Grid_fill();
            }


        }
        void combofill()
        {
            String g_strQuery = "";

            g_strQuery = "EXEC ASP_SPMODULEDRPFILL  @NMODE='Partyfill',@CUSERPREFIX='" + modMain.g_strpartyfill + "',@CWhere='" + SessionManager.g_strUserId + "'";
            DataTable dtparty = modMain.GetDataTableWithAll(g_strQuery, SessionManager.g_strConnStringACC);
            modMain.FillInComboALL(dtparty, drpparty, "PARTY_NAME", "PARTY_KEY");

            //g_strQuery = "EXEC ASP_SPMODULEDRPFILL  @NMODE='Cityfill',@CUSERPREFIX='" + modMain.g_strpartyfill + "',@CWhere='" + SessionManager.g_strUserId + "'";
            //DataTable dtcity = modMain.GetDataTableWithAll(g_strQuery, SessionManager.g_strConnStringACC);
            //modMain.FillInComboALL(dtcity, drpcity, "CITY_NAME", "CITY_KEY");

            //g_strQuery = "EXEC ASP_SPMODULEDRPFILL  @NMODE='Statefill',@CUSERPREFIX='" + modMain.g_strpartyfill + "',@CWhere='" + SessionManager.g_strUserId + "'";
            //DataTable dtState = modMain.GetDataTableWithAll(g_strQuery, SessionManager.g_strConnStringACC);
            //modMain.FillInComboALL(dtState, drpstate, "STATE_NAME", "STATE_KEY");

            //g_strQuery = "EXEC ASP_SPMODULEDRPFILL  @NMODE='Brokerfill',@CUSERPREFIX='" + modMain.g_strpartyfill + "',@CWhere='" + SessionManager.g_strUserId + "'";
            //DataTable dtbroker = modMain.GetDataTableWithAll(g_strQuery, SessionManager.g_strConnStringACC);
            //modMain.FillInComboALL(dtbroker, drpbroker, "BROKER_NAME", "BROKER_KEY");

            //g_strQuery = "EXEC ASP_SPMODULEDRPFILL  @NMODE='Saletypefill',@CUSERPREFIX='" + modMain.g_strpartyfill + "',@CWhere='" + SessionManager.g_strUserId + "'";
            //DataTable dtsaletype = modMain.GetDataTableWithAll(g_strQuery, SessionManager.g_strConnStringACC);
            //modMain.FillInComboALL(dtsaletype, drpsaletype, "SALETYPE_NAME", "SALETYPE_ID");


            //g_strQuery = "EXEC ASP_SPMODULEDRPFILL  @NMODE='Salepersonfill',@CUSERPREFIX='" + modMain.g_strpartyfill + "',@CWhere='" + SessionManager.g_strUserId + "'";
            //DataTable dtsaleperson = modMain.GetDataTableWithAll(g_strQuery, SessionManager.g_strConnStringACC);
            //modMain.FillInComboALL(dtsaleperson, drpsaleperson, "SALEPERSON_NAME", "SALEPERSON_KEY");

        }
        protected void View(object sender, EventArgs e)
        {
            Grid_fill();
        }

        void Grid_fill()
        {
            try
            {


                string strState = "";
                string strCity = "";
                string strParty = "";
                string strBroker = "";
                int shtOverDue;
                int shtOutstanding;
                string strSaleType = "";
                string strSalesPerson1 = "";

                //if (drpstate.SelectedIndex > 0)
                //{
                //       strState = " And State.State_Name = ''" + drpstate.SelectedItem.Text + "''";
                //}

                //if (drpcity.SelectedIndex > 0)
                //{
                //       strCity = " And City.City_Name = ''" + drpcity.SelectedItem.Text + "''";
                //}

                if (drpparty.SelectedIndex > 0)
                {
                    strParty = " And Party.Party_Name = ''" + drpparty.SelectedItem.Text + "''";
                }

                //if (drpbroker.SelectedIndex > 0)
                //{
                //    strBroker = " And Broker.Broker_Name = ''" + drpbroker.SelectedItem.Text + "''";
                //}

                //if (drpsaletype.SelectedIndex > 0)
                //{
                //    strSaleType = " And SaleType.SaleType_Name = ''" + drpsaletype.SelectedItem.Text + "''";
                //}

                //if (drpsaleperson.SelectedIndex > 0)
                //{
                //    strSalesPerson1 = " And SalePerson.SalePerson_Name = ''" + drpsaleperson.SelectedItem.Text + "''";
                //}

                shtOverDue = 0; //Convert.ToInt32(lblODDays.Text);
                shtOutstanding = 0;// Convert.ToInt32(lblOSDays.Text);

                string DateFltr = "";
                string DaysFltr = "";
                // string strdays =  " And OD_DAYS >= " + lblODDays.Text + "  And BILL_DAYS >= " + lblOSDays.Text + " ";
                //string strdays = " And OD_DAYS >=0 And BILL_DAYS >=0 ";
                // if (YNFlag == WithDate.Yes)
                // DateFltr = " And Bill.Doc_Dt >= '" + Format(dtpFrom.Value, "MM/dd/yyyy") + " 00:00:00'"
                //+ " And Bill.Doc_Dt <= '" + Format(dtpTo.Value, "MM/dd/yyyy") + " 23:59:59'";
                // else
                DaysFltr = " Having DateDiff(Day, Due_Dt, GetDate()) >= " + shtOverDue + " And DateDiff(Day, Doc_Dt, GetDate()) >= " + shtOutstanding;

                String strwhere = strState + strCity + strParty + strBroker + DateFltr + strSaleType + strSalesPerson1;

                //string strquery = "Select Trj_Type, Party_Name, Doc_No, Doc_Dt, Due_Dt, Cast(Sum(Doc_Amt) As Varchar) + Case When Amt_Flg = 'C' Then ' Cr' Else ' Dr' End Bill_Net, Cast(Sum(Bal_Amt) As Varchar) + Case When Amt_Flg = 'C' Then ' Cr' Else ' Dr' End Bill_Bal, DateDiff(Day, Due_Dt, GetDate()) OD_Days, DateDiff(Day, Doc_Dt, GetDate()) Os_Days, Broker_Name, State_Name, City_Name,SaleType_Name, SalePerson.SalePerson_Name"
                //    + " From View_OsBills Bill Inner Join PartyDtl On Bill.PartyDtl_Id = PartyDtl.PartyDtl_Id "
                //    + " Inner Join Party On Party.Party_Key = PartyDtl.Party_Key "
                //    + " Inner Join Broker On Bill.Broker_Key = Broker.Broker_Key "
                //    + " Inner Join City On PartyDtl.City_Key = City.City_Key "
                //    + " Inner Join State On City.State_Key = State.State_Key "
                //    + " Inner Join SaleType On SaleType.SaleType_Id= Bill.SaleType_Id "
                //    + " Left Join CLIENTTERMS On Party.Party_Key = CLIENTTERMS.Party_Key "
                //    + " Left Join SalePerson On CLIENTTERMS.SALEPERSON1_KEY = SalePerson.SalePerson_Key  "
                //    + " Where CoBr_Id = '" + SessionManager.g_strcobr_id + "' And FcYr_Key = '" + SessionManager.g_strfcyrID + "'"
                //    + strState + strCity + strParty + strBroker + DateFltr + strSaleType + strSalesPerson1
                //    + " Group By Trj_Type, Party_Name, Amt_Flg, Broker_Name, State_Name, City_Name, Doc_No, Doc_Dt, Due_Dt, Doc_Dt, Amt_Flg ,saletype_name, SalePerson.SalePerson_Name"
                //    + " Order By Party_Name, Doc_Dt, Broker_Name, State_Name, City_Name";


                string strquery = "EXEC ASP_SPMODULEDRPFILL  @NMODE='OUTSTANDINGfill',@CCOBR_ID='" + SessionManager.g_strcobr_id + "',@CFCYR = '" + SessionManager.g_strfcyrID + "',@CUSERPREFIX='" + modMain.g_strpartyfill + "',@CWhere='" + DaysFltr + "',@CWhere1='" + strwhere + "'";


                //string strquery = "SELECT * FROM VIEW_ASP_OUTSTANDING WHERE CoBr_Id = '" + SessionManager.g_strcobr_id + "' and FcYr_Key = '" + SessionManager.g_strfcyrID + "' "
                //                + " And OD_DAYS >= " + lblODDays.Text + " And BILL_DAYS >= " + lblOSDays.Text + "" + strState + strCity + strParty + strBroker + strSaleType + strSalesPerson1
                //                + " ORDER BY PARTY_NAME, CITY_NAME, SALEPERSON_NAME,DOC_NO DESC, DOC_DT DESC";

                modMain.gridviewbind(strquery, SessionManager.g_strConnStringACC, grdviewnew);

                DataTable dt = new DataTable();
                dt = ((DataSet)grdviewnew.DataSource).Tables[0];
                //dt.TableName = "table1";
                //grdviewnew.HeaderRow.TableSection = TableRowSection.TableHeader;

                grdviewnew.DataSource = dt;

                grdviewnew.DataBind();
                //here add code for column total sum and show in footer  
                object total = 0; 
                object totalbal = 0;

                if (dt.Rows.Count > 0)
                {

                    total = dt.Compute("Sum(Bill_Net)", string.Empty);
                    totalbal = dt.Compute("Sum(Bill_Bal)", string.Empty);
                }

                grdviewnew.FooterRow.Cells[4].Text = "Grand Total";
                grdviewnew.FooterRow.Cells[5].Font.Bold = true;
                grdviewnew.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Left;

                grdviewnew.FooterRow.Cells[5].Text = total.ToString();
                grdviewnew.FooterRow.Cells[6].Text = totalbal.ToString();
                grdviewnew.FooterRow.Cells[5].Font.Bold = true;
                grdviewnew.FooterRow.Cells[6].Font.Bold = true;
                

                //for (int k = 1; k < dt.Columns.Count - 1; k++)
                //{
                //    total=Convert.ToInt32( dt.Compute("Sum(Bill_Net)", ""));
                //    totalbal = Convert.ToInt32(dt.Compute("Sum(Bill_Bal)", ""));
                //    // total = dt.AsEnumerable().Sum(row => row.Field<Int32>(dt.Columns[k].ToString()));
                //    //  total = dt.Compute["Sum(Ord_Qty)"];
                //    grdviewnew.FooterRow.Cells[k].Text = total.ToString();
                //    grdviewnew.FooterRow.Cells[k].Font.Bold = true;
                //    grdviewnew.FooterRow.BackColor = System.Drawing.Color.Beige;
                //}

            }
            catch (SqlException sqlEx)
            {
                modMain.ShowMessage(sqlEx.Message, this);
            }
            catch (Exception ex)
            {
                modMain.ShowMessage(ex.Message, this);
            }
            finally
            {

            }
            //try
            //{
            //    string strState = "";
            //    string strCity = "";
            //    string strParty = "";
            //    string strBroker = "";
            //    string strSaleType = "";
            //    string strSalesPerson1 = "";

            //    if (drpstate.SelectedIndex > 0)
            //    {
            //        strState = " And State_Name = '" + drpstate.Text + "'";
            //    }

            //    if (drpcity.SelectedIndex > 0)
            //    {
            //        strCity = " And City_Name = '" + drpcity.Text + "'";
            //    }

            //    if (drpparty.SelectedIndex > 0)
            //    {
            //        strParty = " And Party_Name = '" + drpparty.Text + "'";
            //    }

            //    if (drpbroker.SelectedIndex > 0)
            //    {
            //        strBroker = " And Broker_Name = '" + drpbroker.Text + "'";
            //    }

            //    if (drpsaletype.SelectedIndex > 0)
            //    {
            //        strSaleType = " And SaleType_Name = '" + drpsaletype.Text + "'";
            //    }
            //    if (drpsaleperson.SelectedIndex > 0)
            //    {
            //        strSalesPerson1 = " And SalePerson_Name = '" + drpsaleperson.Text + "'";
            //    }


            //    string strquery = "SELECT * FROM VIEW_ASP_OUTSTANDING WHERE CoBr_Id = '" + SessionManager.g_strcobr_id + "' and FcYr_Key = '" + SessionManager.g_strfcyrID + "' "
            //                    + " And OD_DAYS >= " + lblODDays.Text + " And BILL_DAYS >= " + lblOSDays.Text + "" + strState + strCity + strParty + strBroker + strSaleType + strSalesPerson1
            //                    + " ORDER BY PARTY_NAME, CITY_NAME, SALEPERSON_NAME,DOC_NO DESC, DOC_DT DESC";

            //   // string strquery = "SELECT * FROM VIEW_ASP_OUTSTANDING ORDER BY PARTY_NAME, CITY_NAME, SALEPERSON_NAME,DOC_NO DESC, DOC_DT DESC";


            //    modMain.gridviewbind(strquery, SessionManager.g_strConnStringACC, grdviewnew);

            //    DataTable dt = new DataTable();
            //    dt = ((DataSet)grdviewnew.DataSource).Tables[0];
            //    dt.TableName = "table1";
            //    grdviewnew.HeaderRow.TableSection = TableRowSection.TableHeader;

            //}
            //catch (SqlException ex)
            //{
            //    modMain.ShowMessage(ex.Message, this);
            //}

        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            // Ensure that the control is nested in a server form.
            if (Page != null)
            {
                /////  Page.VerifyRenderingInServerForm(this);
            }
            //   base.Render(writer);
        }


        private void ExportGridToExcel()
        {
            //Response.Clear();
            //Response.Buffer = true;
            //Response.ClearContent();
            //Response.ClearHeaders();
            //Response.Charset = "";
            //string FileName = "Outstanding1" + DateTime.Now + ".xls";
            //StringWriter strwritter = new StringWriter();
            //HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.ContentType = "application/vnd.ms-excel";
            //Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
            //grdviewnew.GridLines = GridLines.Both;
            //grdviewnew.HeaderStyle.Font.Bold = true;
            //grdviewnew.RenderControl(htmltextwrtter);
            //Response.Write(strwritter.ToString());
            //Response.End();

            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Outstanding" + DateTime.Now + ".xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            grdviewnew.AllowPaging = false;
            //Grid_fill();
            //Change the Header Row back to white color
            grdviewnew.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //Applying stlye to gridview header cells
            for (int i = 0; i < grdviewnew.HeaderRow.Cells.Count; i++)
            {
                grdviewnew.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
            }
            grdviewnew.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();


        }
        protected void Export(object sender, EventArgs e)
        {
            ExportGridToExcel();
            //string strState = "";
            //string strCity = "";
            //string strParty = "";
            //string strBroker = "";
            //string strSaleType = "";
            //string strSalesPerson1 = "";

            //if (drpstate.SelectedIndex > 0)
            //{
            //    strState = " And {State.State_Name} = '" + drpstate.Text + "'";
            //}

            //if (drpcity.SelectedIndex > 0)
            //{
            //    strCity = " And {City.City_Name} = '" + drpcity.Text + "'";
            //}

            //if (drpparty.SelectedIndex > 0)
            //{
            //    strParty = " And {Party.Party_Name} = '" + drpparty.Text + "'";
            //}

            //if (drpbroker.SelectedIndex > 0)
            //{
            //    strBroker = " And {Broker.Broker_Name} = '" + drpbroker.Text + "'";
            //}

            //if (drpsaletype.SelectedIndex > 0)
            //{
            //    strSaleType = " And {SaleType.SaleType_Name} = '" + drpsaletype.Text + "'";
            //}
            //if (drpsaleperson.SelectedIndex > 0)
            //{
            //    strSalesPerson1 = " And {SalePerson.SalePerson_Name} = '" + drpsaleperson.Text + "'";
            //}

            //cryRpt = new ReportDocument();
            //cryRpt.Load(Server.MapPath("Report\\OsBills.rpt"));

            //////ConnectionInfo crConn = new ConnectionInfo();
            //////crConn.ServerName = modMain.g_strServerName;
            //////crConn.DatabaseName = modMain.g_strDatabaseAcc + modMain.strfcyr;
            //////crConn.UserID = modMain.g_strUsername;
            //////crConn.Password = modMain.g_strPassword;
            //cryRpt.DataSourceConnections[0].IntegratedSecurity = false;
            //cryRpt.DataSourceConnections[0].SetConnection("PGSACC", SessionManager.g_strDatabaseAcc + SessionManager.g_strYearText, "", "");

            //cryRpt.DataDefinition.FormulaFields["Page_Brk"].Text = "0"; //rdyes.Checked ? "1" : "0";
            //cryRpt.DataDefinition.FormulaFields["ODDays_Head"].Text = lblODDays.Text;
            //cryRpt.DataDefinition.FormulaFields["OSDays_Head"].Text = lblOSDays.Text;

            //if (modMain.g_strpartyfill == "C")
            //{
            //    if (strParty.Length == 0 || strParty == "")
            //    {
            //        strParty = " And {Party.Party_Name} = '" + SessionManager.g_strUsername + "'";
            //    }

            //}
            //cryRpt.RecordSelectionFormula = "{View_OsBills.CoBr_Id} = '" + SessionManager.g_strcobr_id + "' And {View_OsBills.FcYr_Key} = '" + SessionManager.g_strfcyrID + "' And {@OD_Days} >= " + lblODDays.Text + " And {@OS_Days} >= " + lblOSDays.Text + "" + strState + strCity + strParty + strBroker + strSaleType + strSalesPerson1;
            //// this.CrystalReportViewer1.ReportSource = cryRpt;


            //ExportFormatType formatType = ExportFormatType.NoFormat;
            ////switch (rbFormat.SelectedItem.Value)
            ////{
            ////    case "Word":
            ////        formatType = ExportFormatType.WordForWindows;
            ////        break;
            ////    case "PDF":
            //     formatType = ExportFormatType.PortableDocFormat;
            ////        break;
            ////    case "Excel":
            ////        formatType = ExportFormatType.Excel;
            ////        break;
            ////    case "CSV":
            ////        formatType = ExportFormatType.CharacterSeparatedValues;
            ////        break;
            ////}

            //cryRpt.ExportToHttpResponse(formatType, Response, true, "Outstanding");

            //Response.End();

        }



        public static void AddReportParameter(ReportDocument rpt, string ParameterName, object val)
        {

            ParameterValues prms;

            ParameterDiscreteValue prm = new ParameterDiscreteValue();

            prms = rpt.DataDefinition.ParameterFields[ParameterName].CurrentValues;

            prm.Value = val;

            prms.Add(prm);

            rpt.DataDefinition.ParameterFields[ParameterName].ApplyCurrentValues(prms);

        }

    }
}