﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="GPGSOLASPNET.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <br />
            <div class="panel panel-primary list-panel" id="list-panel">
                <div class="panel-heading list-panel-heading">
                    <h1 class="panel-title list-panel-title">Select Menu</h1>
                </div>
                <div class="panel-body">
                  
                    <div class="row">
                        <div class="col-md-12">
                            <%-- <div class="col-md-3">
                                <asp:ImageButton ID="imgordbook" runat="server" BorderStyle="Solid" ImageUrl="~/Images/ord_book.png" BorderWidth="1px" Width="200" OnClick="imgordbook_Click" />
                            </div>--%>
                            <div class="col-md-3 dashboard-main">

                                <a runat="server" cssclass="dashboard-logo" href="OrderBooking"><div class="dashboard-logo">
                                </div>Order Booking</a>
                            </div>

                            <div class="col-md-3 dashboard-main">
                                <%-- <asp:ImageButton ID="imgordcat" CssClass="dashboard-logo" runat="server"   OnClick="imgordcat_Click" ImageUrl="~/Images/Dashboard_logo.png" />--%>
                                                               
                                <a runat="server" href="newCatalogue"><div class="dashboard-logo">
                                </div>Catalogue</a>
                            </div>


                            <div class="col-md-3 dashboard-main">
                                <%--<asp:ImageButton ID="imgoutstanding" runat="server" ImageUrl="~/Images/Outstanding_ord.png" BorderStyle="Solid" BorderWidth="1px" Width="200" OnClick="imgoutstanding_Click" />--%>
                                
                               <%-- <a runat="server" href="OutstandingParty">
                                    <div class="dashboard-logo">
                                </div>Outstanding</a>--%>

                                 <a runat="server" href="Outstanding">
                                    <div class="dashboard-logo">
                                </div>Outstanding</a>
                            </div>
                            <div class="col-md-3 dashboard-main">
                                <%--<asp:ImageButton ID="imgordhistory" runat="server" ImageUrl="~/Images/ord_history.png" BorderStyle="Solid" BorderWidth="1px" Width="200" OnClick="imgordhistory_Click" />--%>
                                
                                <a runat="server" href="order_history"><div class="dashboard-logo">
                                </div>Order History</a>
                            </div>
                        
                            <div class="col-md-3 dashboard-main">

                               
                                <%--<asp:HyperLink runat="server" CssClass="dashboard-text"   Target="_blank"   NavigateUrl="http://catalog.prasyst.com/">Cloud Catalogue</asp:HyperLink>--%>
                                <a runat="server" href="http://catalog.prasyst.com/" target="_blank"> <div class="dashboard-logo">
                                </div>Cloud Catalogue</a>

                            </div>
                      </div>
                      

                    </div>
                </div>
            </div>
        </div>
    </div>





</asp:Content>
