﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Common
{
   public  static class SessionManager
    {
        public static string g_strQuery
        {
            get
            {
                return HttpContext.Current.Session["g_strQuery"] == null ?
                    null : HttpContext.Current.Session["g_strQuery"].ToString();
            }

            set
            {
                HttpContext.Current.Session["g_strQuery"] = value;
            }

        }

        public static string UserType
        {
            get
            {
                return HttpContext.Current.Session["UserType"] == null ?
                    null : HttpContext.Current.Session["UserType"].ToString();
            }

            set
            {
                HttpContext.Current.Session["UserType"] = value;
            }

        }
        public static string g_strUserId
        {
            get
            {
                return HttpContext.Current.Session["UserID"] ==null ?
                   "1" : Convert.ToString(HttpContext.Current.Session["UserID"]);
            }

            set
            {
                HttpContext.Current.Session["UserID"] = value;
            }

        }
        public static string g_strUsername
        {
            get
            {
                return HttpContext.Current.Session["g_strUsername"] == null ?
                    "" : HttpContext.Current.Session["g_strUsername"].ToString();
            }

            set
            {
                HttpContext.Current.Session["g_strUsername"] = value;
            }

        }

        public static string AccountId
        {
            get
            {
                return HttpContext.Current.Session["AccountId"] == null ?
                    null : (string)HttpContext.Current.Session["AccountId"];
            }

            set
            {
                if (HttpContext.Current.Session["AccountId"] == null)
                {
                    HttpContext.Current.Session.Add("AccountId", value);
                }
                else {

                    HttpContext.Current.Session["AccountId"]= value;
                }
            }

        }
        public static string LogError
        {
            get
            {
                return HttpContext.Current.Session["logerror"] == null ?
                    null : HttpContext.Current.Session["logerror"].ToString();
            }

            set
            {
                HttpContext.Current.Session["logerror"] = value;
            }

        }
        public static string g_strConnString
        {
            get
            {
                return HttpContext.Current.Session["g_strConnString"] == null ?
                    "" : HttpContext.Current.Session["g_strConnString"].ToString();
            }

            set
            {
                HttpContext.Current.Session["g_strConnString"] = value;
            }

        }
        public static string g_strConnStringACC
        {
            get
            {
                return HttpContext.Current.Session["g_strConnStringACC"] == null ?
                    "" : HttpContext.Current.Session["g_strConnStringACC"].ToString();
            }

            set
            {
                HttpContext.Current.Session["g_strConnStringACC"] = value;
            }

        }
        public static string g_strYearText
        {
            get
            {
                return HttpContext.Current.Session["g_strYearText"] == null ?
                    null : HttpContext.Current.Session["g_strYearText"].ToString();
            }

            set
            {
                HttpContext.Current.Session["g_strYearText"] = value;
            }

        }

        public static string g_strCompanyName
        {
            get
            {
                return HttpContext.Current.Session["g_strCompanyName"] == null ?
                    null : HttpContext.Current.Session["g_strCompanyName"].ToString();
            }

            set
            {
                HttpContext.Current.Session["g_strCompanyName"] = value;
            }

        }

        public static string g_strCo_id
        {
            get
            {
                return HttpContext.Current.Session["g_strCo_id"] == null ?
                    null : HttpContext.Current.Session["g_strCo_id"].ToString();
            }

            set
            {
                HttpContext.Current.Session["g_strCo_id"] = value;
            }


        }
        public static string g_strcobr_id
        {
            get
            {
                return HttpContext.Current.Session["COBR_ID"] == null ?
                    null : HttpContext.Current.Session["COBR_ID"].ToString();
            }

            set
            {
                HttpContext.Current.Session["COBR_ID"] = value;
            }

        }
        public static string LOGINOTP
        {
            get
            {
                return HttpContext.Current.Session["LOGINOTP"] == null ?
                    null : HttpContext.Current.Session["LOGINOTP"].ToString();
            }

            set
            {
                HttpContext.Current.Session["LOGINOTP"] = value;
            }

        }

        public static string g_strDatabase
        {
            get
            {
                return HttpContext.Current.Session["g_strDatabase"] == null ?
                    System.Configuration.ConfigurationManager.AppSettings["dbname"] : HttpContext.Current.Session["g_strDatabase"].ToString();
            }

            set
            {
                HttpContext.Current.Session["g_strDatabase"] = value;
            }

        }
        public static string g_strDatabaseAcc
        {
            get
            {
                return HttpContext.Current.Session["g_strDatabaseAcc"] == null ?
                    System.Configuration.ConfigurationManager.AppSettings["dbnameacc"] : HttpContext.Current.Session["g_strDatabaseAcc"].ToString();
            }

            set
            {
                HttpContext.Current.Session["g_strDatabaseAcc"] = value;
            }

        }
        public static string g_strDBUsername
        {
            get
            {
                return HttpContext.Current.Session["g_strDBUsername"] == null ?
                    System.Configuration.ConfigurationManager.AppSettings["userdbname"] : HttpContext.Current.Session["g_strDBUsername"].ToString();
            }

            set
            {
                HttpContext.Current.Session["g_strDBUsername"] = value;
            }

        }

        public static string g_strDBPassword
        {
            get
            {
                return HttpContext.Current.Session["g_strDBPassword"] == null ?
                    System.Configuration.ConfigurationManager.AppSettings["pwddbname"] : HttpContext.Current.Session["g_strDBPassword"].ToString();
            }

            set
            {
                HttpContext.Current.Session["g_strDBPassword"] = value;
            }

        }

        public static string g_strServerName
        {
            get
            {
                return HttpContext.Current.Session["g_strServerName"] == null ?
                    System.Configuration.ConfigurationManager.AppSettings["servername"] : HttpContext.Current.Session["g_strServerName"].ToString();
            }

            set
            {
                HttpContext.Current.Session["g_strServerName"] = value;
            }

        }

        public static string g_strfcyr
        {
            get
            {
                return HttpContext.Current.Session["g_strfcyr"] == null ?
                    "" : HttpContext.Current.Session["g_strfcyr"].ToString();
            }

            set
            {
                HttpContext.Current.Session["g_strfcyr"] = value;
            }
        }
        public static string g_strfcyrID
        {
            get
            {
                return HttpContext.Current.Session["g_strfcyrID"] == null ?
                    "" : HttpContext.Current.Session["g_strfcyrID"].ToString();
            }

            set
            {
                HttpContext.Current.Session["g_strfcyrID"] = value;
            }
        }

        public static DataTable ordtbl
        {
            get
            {

                DataTable dt;
                if (HttpContext.Current.Session["ordtbl"] == null)
                {
                    dt = new DataTable();
                    HttpContext.Current.Session["ordtbl"] = dt;
                }
                else
                {
                    dt = (DataTable)HttpContext.Current.Session["ordtbl"];

                }
                if (dt.Columns.Count==0)
                {
                    dt.Columns.Add("SRNO", typeof(int));
                    dt.Columns.Add("ORDBKDTL_ID", typeof(int));
                    dt.Columns.Add("ORDER_ID", typeof(int));
                    dt.Columns.Add("BARCODE", typeof(string));
                    dt.Columns.Add("ALT_BARCODE", typeof(string));
                    dt.Columns.Add("FGSTYLE_ID", typeof(int));
                    dt.Columns.Add("FGSTYLE_CODE", typeof(string));
                    dt.Columns.Add("FGTYPE_KEY", typeof(string));
                    dt.Columns.Add("FGTYPE_NAME", typeof(string));
                    dt.Columns.Add("FGSHADE_KEY", typeof(string));
                    dt.Columns.Add("FGSHADE_NAME", typeof(string));
                    dt.Columns.Add("STYSIZE_ID", typeof(int));
                    dt.Columns.Add("STYSIZE_NAME", typeof(string));
                    dt.Columns.Add("QTY", typeof(double));
                    dt.Columns.Add("MRP", typeof(double));
                    dt.Columns.Add("WSP", typeof(double));
                    dt.Columns.Add("AMOUNT", typeof(double));
                    dt.Columns.Add("COBR_ID", typeof(string));
                    dt.Columns.Add("PENDING_ORD_QTY", typeof(double));
                }
                return dt;
            }

            set
            {
                HttpContext.Current.Session["ordtbl"] = value;
            }
        }

        public static DataTable dtcatelogdtl
        {
            get
            {

                DataTable dt;
                if (HttpContext.Current.Session["catelogdtl"] == null)
                {
                    dt = new DataTable();
                    HttpContext.Current.Session["catelogdtl"] = dt;
                }
                else
                {
                    dt = (DataTable)HttpContext.Current.Session["catelogdtl"];

                }
                if (dt.Columns.Count == 0)
                {
                    dt.Columns.Add("SRNO", typeof(int));
                    dt.Columns.Add("BARCODE", typeof(string));
                    dt.Columns.Add("ALT_BARCODE", typeof(string));
                    dt.Columns.Add("FGSTYLE_ID", typeof(int));
                    dt.Columns.Add("FGSTYLE_CODE", typeof(string));
                    dt.Columns.Add("FGTYPE_KEY", typeof(string));
                    dt.Columns.Add("FGTYPE_NAME", typeof(string));
                    dt.Columns.Add("FGSHADE_KEY", typeof(string));
                    dt.Columns.Add("FGSHADE_NAME", typeof(string));
                    dt.Columns.Add("STYSIZE_ID", typeof(int));
                    dt.Columns.Add("STYSIZE_NAME", typeof(string));
                    dt.Columns.Add("QTY", typeof(double));
                    dt.Columns.Add("MRP", typeof(double));
                    dt.Columns.Add("WSP", typeof(double));
                    dt.Columns.Add("AMOUNT", typeof(double));
                    dt.Columns.Add("COBR_ID", typeof(string));
                    dt.Columns.Add("PENDING_ORD_QTY", typeof(double));
                }
                return dt;
            }

            set
            {
                HttpContext.Current.Session["catelogdtl"] = value;
            }
        }

        public static void ClearALL()
        {
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.Abandon();
            HttpContext.Current.Session.RemoveAll();

        }
    }
}
