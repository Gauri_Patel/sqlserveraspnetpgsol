﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Collections;
using Microsoft.VisualBasic;
using System.Web.SessionState;
using System.Threading;
using System.Web.Services;
using System.Text;
using Microsoft.Ajax.Utilities;
using System.Web.Security;
using Common;

namespace GPGSOLASPNET
{
    public partial class Catalogue : System.Web.UI.Page
    {
        DataTable dtTable = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated) // if the user is already logged in
            {
                Response.Redirect(FormsAuthentication.LoginUrl);
            }
            if (!Page.IsPostBack)
            {
                lblcount.Text = "0";
                String strquery = "SELECT 3 PRODTYPE_KEY,'OTHER' PRODTYPE_NAME UNION SELECT 2 PRODTYPE_KEY,'FASHION' PRODTYPE_NAME UNION SELECT 1 PRODTYPE_KEY,'CORE' PRODTYPE_NAME ";
                modMain.FillInCombo(strquery, drptype, "PRODTYPE_NAME", "PRODTYPE_KEY", SessionManager.g_strConnString);
                drptype.Items.Insert(0, "Select");
                // drptype.SelectedIndex = drptype.Items.Count - 1;
            }

        }


       

        protected void drptype_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblcount.Text = "0";
            datalist1.DataSource = null;
            datalist1.DataBind();
            ViewState["nosimg"] = 0;
            if (drptype.Text.ToString() == "Select")
            {
                return;
            }
           
            getByCat(drptype.SelectedValue, 3);
        }

        protected void btnloadimg_Click(object sender, EventArgs e)
        {
            getByCat(drptype.SelectedValue, 3);
        }
        public void getByCat(string Strstyleproducttype, Int32 intnos)
        {
            DataTable dt = new DataTable();
            if (ViewState["nosimg"] != null)
            {
                intnos = Convert.ToInt32(ViewState["nosimg"]) + intnos;
            }

            String strquery = "EXEC ASP_SPMODULE  @NMODE='CatlogueFill',@CWhere='" + Strstyleproducttype + "',@nnum=" + intnos + "";
            dt = modMain.NewDatatable(strquery, SessionManager.g_strConnString, null);


            datalist1.DataSource = dt;
            datalist1.DataBind();
            ViewState["nosimg"] = intnos;
            lblcount.Text =Convert.ToString( dt.Rows.Count);
            if (datalist1.Items.Count <= 0)
            {
                modMain.ShowMessage("No Image Found", this);
            }

            //  }


        }


    }
}