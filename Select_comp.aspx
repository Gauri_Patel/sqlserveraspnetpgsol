﻿<%@ Page Title="Select Company" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Select_comp.aspx.cs" Inherits="GPGSOLASPNET.Select_comp" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
      <%--<script type="text/javascript">
          $(document).ready(function () {
              var x = document.getElementById("Navigation");
                  x.style.display = "none";

          });

      </script>--%>
    <div class="container login">
        <div >
            <br />
            <div class="panel panel-primary list-panel" id="list-panel">
                <div class="panel-heading list-panel-heading">
                    <h1 class="panel-title list-panel-title">Select Company</h1>
                </div>
                <br />
                <div class="container">
                    
                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="ddlCompany" CssClass="col-md-2 control-label">Company</asp:Label>
                        <div class="col-md-8">
                           
                            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="form-control" ></asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="ddlCompany"
                                CssClass="text-danger" ErrorMessage="The User Name field is required." />
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                            <asp:Button runat="server" OnClick="Continue" Text="Continue" CssClass="btn btn-default" />
                            <%-- Enable this once you have account confirmation enabled for password reset functionality --%>
                            <asp:HyperLink runat="server" CssClass=" btn-link btn" ID="Loginpage" ViewStateMode="Disabled" NavigateUrl="~/Index.aspx">Go Back</asp:HyperLink>

                        </div>

                    </div>



                </div>
            </div>

        </div>


    </div>


</asp:Content>
