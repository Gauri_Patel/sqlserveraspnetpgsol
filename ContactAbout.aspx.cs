﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GPGSOLASPNET
{
    public partial class ContactAbout : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (! User.Identity.IsAuthenticated) // if the user is already logged in
            {
                Response.Redirect(FormsAuthentication.LoginUrl);
            }

        }
    }
}