IF OBJECT_ID('ASP_SPMODULEDRPFILL','P') IS NOT NULL
	DROP PROCEDURE ASP_SPMODULEDRPFILL
GO---PRASYSTSQL---
CREATE  PROCEDURE dbo.ASP_SPMODULEDRPFILL                                          
(                                          
@NMODE      nvarchar(50)='',   
@userName   T_DESC_25='',
@USER_PWD   varchar(20)='',
@CFCYR      VARCHAR(5)='',                                          
@NUSERID     INT=0,                                          
@CUSERTY     VARCHAR(50)='CUSTOMER', 
@CUSERPREFIX     VARCHAR(5)='S', 
@CCOBR_ID     VARCHAR(5)='',                                          
@CCO_ID      VARCHAR(5) ='',
@cdoc_key    varchar(20)='',
@CWhere nVarchar(500)='',
@nnum varchar(10)='0',
@FGSTYLE_ID BIGINT=0,
@FGSHADE_KEY VARCHAR(5)='',
@FGTYPE_KEY VARCHAR(5)='',
@ALT_BARCODE varchar(100)='',
@CWhere1 nVarchar(4000)=''

                              
)                                          
AS                                           
BEGIN   
	DECLARE @CCMD NVARCHAR(MAX),@CERR_MSG VARCHAR(2000)     

	if (@NMODE='Partyfill')
		   BEGIN 
				if (@CUSERPREFIX='C')
				BEGIN
					Set @CCMD='SELECT DISTINCT A.PARTY_KEY,A.PARTY_NAME FROM PARTY A WHERE A.PARTY_KEY='''+@CWHERE+'''  ORDER BY PARTY_NAME'
					print @CCMD
					EXECUTE SP_EXECUTESQL @CCMD   
				END
				ELSE if (@CUSERPREFIX='S')
				BEGIN
					Set @CCMD='select distinct b.PARTY_KEY,b.PARTY_NAME from CLIENTTERMS a left join party b on a.PARTY_KEY=b.PARTY_KEY
							 left join SALEPERSON c on c.SALEPERSON_KEY=a.SALEPERSON1_KEY where  C.SALEPERSON_KEY='''+ @CWhere +''''
					print @CCMD
				EXECUTE SP_EXECUTESQL @CCMD   
				END
				Else
				BEGIN
					Set @CCMD='SELECT DISTINCT A.PARTY_KEY,A.PARTY_NAME FROM PARTY A ORDER BY PARTY_NAME'
					print @CCMD
					EXECUTE SP_EXECUTESQL @CCMD   
				END
		END

	    if (@NMODE='Statefill')
		   BEGIN 
				if (@CUSERPREFIX='C')
				BEGIN
					Set @CCMD='Select distinct c.STATE_KEY, c.STATE_NAME FROM  party a join city b on a.city_key=b.city_key left join state c on b.state_key=c.state_key 
							 Where a.party_key='''+@CWhere+''' And c.State_Key <> '''' And C.Status = ''1'' Order By c.STATE_NAME'
					print @CCMD
					EXECUTE SP_EXECUTESQL @CCMD   
				END
				ELSE IF (@CUSERPREFIX='S')
				BEGIN
					Set @CCMD='select distinct c.STATE_KEY, c.STATE_NAME from CLIENTTERMS a left join party e on a.PARTY_KEY=e.PARTY_KEY
							 join city b on e.city_key=b.city_key left join state c on b.state_key=c.state_key
							 where  a.SALEPERSON1_KEY='''+ @CWhere +''' And c.State_Key <> '''' And C.Status = ''1'' Order By c.STATE_NAME '
					print @CCMD
					EXECUTE SP_EXECUTESQL @CCMD   
				END
				Else
				BEGIN
					Set @CCMD='SELECT DISTINCT A.STATE_KEY, A.STATE_NAME FROM STATE A ORDER BY STATE_NAME'
					print @CCMD
					EXECUTE SP_EXECUTESQL @CCMD   
				END
			END

	   if (@NMODE='Cityfill')
		   BEGIN 
		   
				if (@CUSERPREFIX='C')
				BEGIN
					Set @CCMD='select distinct B.CITY_KEY,B.CITY_NAME from party a join city b on a.city_key=b.city_key 
					where a.party_key='''+@CWhere+''' 
					And b.city_key <> '''' And b.Status = ''1'' Order By b.CITY_NAME'
					
				  END
				
				ELSE if (@CUSERPREFIX='S')
				BEGIN
				Set @CCMD='select distinct B.CITY_KEY,B.CITY_NAME from CLIENTTERMS a left join party e on a.PARTY_KEY=e.PARTY_KEY
							 join city b on e.city_key=b.city_key 
							 left join SALEPERSON c on c.SALEPERSON_KEY=a.SALEPERSON1_KEY  
							 where a.SALEPERSON1_KEY='''+@CWhere+''' 
					         And b.city_key <> '''' And b.Status = ''1'' Order By b.CITY_NAME'
					
					
				END
				else
				BEGIN
				Set @CCMD='select distinct B.CITY_KEY,B.CITY_NAME from city b  Order By b.CITY_NAME'
				END
				print @CCMD
				EXECUTE SP_EXECUTESQL @CCMD  
					
		   END

	   if (@NMODE='Brokerfill')
		   BEGIN 
				if (@CUSERPREFIX='C')
				BEGIN
					Set @CCMD='select distinct c.BROKER_KEY, c.BROKER_NAME from CLIENTTERMS a left join party b on a.PARTY_KEY=b.PARTY_KEY
							 left join Broker c on c.Broker_Key=a.Broker_Key where  a.Party_KEY='''+ @CWhere +''' and c.Broker_Key<>'''' 
							 And c.Status = ''1'' order by c.BROKER_NAME '
					print @CCMD
					EXECUTE SP_EXECUTESQL @CCMD   
				END
				ELSE if (@CUSERPREFIX='S')
				BEGIN
					Set @CCMD='select distinct c.BROKER_KEY, c.BROKER_NAME  from CLIENTTERMS a left join party b on a.PARTY_KEY=b.PARTY_KEY
							 left join Broker c on c.Broker_Key=a.Broker_Key   
							 where  a.SALEPERSON1_KEY='''+ @CWhere +'''  and c.Broker_Key<>'''' 
							 And c.Status = ''1'' order by c.BROKER_NAME '
					print @CCMD
					EXECUTE SP_EXECUTESQL @CCMD   
				END
				Else
				BEGIN
					Set @CCMD='select distinct c.BROKER_KEY, c.BROKER_NAME from  Broker c order by c.BROKER_NAME '
					print @CCMD
					EXECUTE SP_EXECUTESQL @CCMD   
				END
		   END

	    if (@NMODE='Saletypefill')
		   BEGIN 
				if (@CUSERPREFIX='C')
				BEGIN
					Set @CCMD='select distinct c.SALETYPE_ID, c.SALETYPE_NAME from CLIENTTERMS a left join party b on a.PARTY_KEY=b.PARTY_KEY
							 left join SaleType c on c.SaleType_Id=a.SaleType_Id where  a.Party_KEY='''+ @CWhere +''' and c.SALETYPE_ID<>''0'' 
							 And c.Status = ''1'' order by c.SALETYPE_NAME'
					print @CCMD
					EXECUTE SP_EXECUTESQL @CCMD   
				END
				ELSE if (@CUSERPREFIX='S')
				BEGIN
					Set @CCMD='select distinct c.SALETYPE_ID, c.SALETYPE_NAME from CLIENTTERMS a left join party b on a.PARTY_KEY=b.PARTY_KEY
							 left join SaleType c on c.SaleType_Id=a.SaleType_Id where  a.SALEPERSON1_KEY='''+ @CWhere +'''
							 and c.SALETYPE_ID<>''0'' And c.Status = ''1'' order by c.SALETYPE_NAME'
					print @CCMD
					EXECUTE SP_EXECUTESQL @CCMD   
				END
				Else
				BEGIN
					Set @CCMD='select distinct c.SALETYPE_ID, c.SALETYPE_NAME from  SaleType c order by c.SALETYPE_NAME '
					print @CCMD
					EXECUTE SP_EXECUTESQL @CCMD   
				END
		   END

	   if (@NMODE='Salepersonfill')
		   BEGIN 
				if (@CUSERPREFIX='C')
				BEGIN
					Set @CCMD='select distinct c.SALEPERSON_key, c.SALEPERSON_NAME from CLIENTTERMS a left join party b on a.PARTY_KEY=b.PARTY_KEY
							 left join SALEPERSON c on c.SALEPERSON_KEY=a.SALEPERSON1_KEY where  a.Party_key='''+ @CWhere +''' and c.SALEPERSON_key<>'''' 
							 And c.Status = ''1'' order by c.SALEPERSON_NAME'
					print @CCMD
					EXECUTE SP_EXECUTESQL @CCMD   
				END
				ELSE if (@CUSERPREFIX='S')
				BEGIN
					Set @CCMD='select distinct c.SALEPERSON_key,c.SALEPERSON_NAME from CLIENTTERMS a left join party b on a.PARTY_KEY=b.PARTY_KEY
							 left join SALEPERSON c on c.SALEPERSON_KEY=a.SALEPERSON1_KEY where  C.SALEPERSON_KEY='''+ @CWhere +'''
							 And c.Status = ''1'' order by c.SALEPERSON_NAME'
					print @CCMD
					EXECUTE SP_EXECUTESQL @CCMD   
				END
				Else
				BEGIN
					Set @CCMD='select distinct  c.SALEPERSON_key,c.SALEPERSON_NAME  from  SALEPERSON c order by c.SALEPERSON_NAME '
					print @CCMD
					EXECUTE SP_EXECUTESQL @CCMD   
				END
		   END

		   if (@NMODE='OUTSTANDINGfill')
		   BEGIN 
				
					--Set @CCMD='Select Trj_Type, Party_Name, Doc_No, Doc_Dt, Due_Dt, Cast(Sum(Doc_Amt) As Varchar) + Case When Amt_Flg = ''C'' Then  '' Cr'' Else '' Dr''
					--End Bill_Net, Cast(Sum(Bal_Amt) As Varchar) + Case When Amt_Flg =  ''C'' Then  '' Cr'' Else '' Dr'' End Bill_Bal, DateDiff(Day, Due_Dt, GetDate()) OD_Days, 
					--DateDiff(Day, Doc_Dt, GetDate()) Os_Days, Broker_Name, State_Name, City_Name,SaleType_Name, SalePerson.SalePerson_Name
					-- From View_OsBills Bill Inner Join PartyDtl On Bill.PartyDtl_Id = PartyDtl.PartyDtl_Id 
					-- Inner Join Party On Party.Party_Key = PartyDtl.Party_Key Inner Join Broker On Bill.Broker_Key = Broker.Broker_Key 
					-- Inner Join City On PartyDtl.City_Key = City.City_Key Inner Join State On City.State_Key = State.State_Key 
					-- Inner Join SaleType On SaleType.SaleType_Id= Bill.SaleType_Id Left Join CLIENTTERMS On Party.Party_Key = CLIENTTERMS.Party_Key 
					-- Left Join SalePerson On CLIENTTERMS.SALEPERSON1_KEY = SalePerson.SalePerson_Key Where CoBr_Id = '''+@CCOBR_ID+'''  And FcYr_Key = '''+@CFCYR+'''
					-- '+@CWHERE1+' Group By Trj_Type, Party_Name, Amt_Flg, Broker_Name, State_Name, City_Name, Doc_No, Doc_Dt, Due_Dt, Doc_Dt, Amt_Flg ,saletype_name, SalePerson.SalePerson_Name
					-- '+@CWHERE+' Order By Party_Name, Doc_Dt, Broker_Name, State_Name, City_Name'
					Set @CCMD='Select Trj_Type, Party_Name, Doc_No, Doc_Dt, Due_Dt, ISNULL(Sum(Doc_Amt),0) AS Bill_Net, ISNULL(Sum(Bal_Amt) ,0)  Bill_Bal
					,DateDiff(Day, Due_Dt, GetDate()) OD_Days, 
					DateDiff(Day, Doc_Dt, GetDate()) Os_Days, Broker_Name, State_Name, City_Name,SaleType_Name, SalePerson.SalePerson_Name
					From View_OsBills Bill Inner Join PartyDtl On Bill.PartyDtl_Id = PartyDtl.PartyDtl_Id 
					Inner Join Party On Party.Party_Key = PartyDtl.Party_Key Inner Join Broker On Bill.Broker_Key = Broker.Broker_Key 
					Inner Join City On PartyDtl.City_Key = City.City_Key Inner Join State On City.State_Key = State.State_Key 
					Inner Join SaleType On SaleType.SaleType_Id= Bill.SaleType_Id Left Join CLIENTTERMS On Party.Party_Key = CLIENTTERMS.Party_Key 
					Left Join SalePerson On CLIENTTERMS.SALEPERSON1_KEY = SalePerson.SalePerson_Key Where CoBr_Id = '''+@CCOBR_ID+'''  And FcYr_Key = '''+@CFCYR+'''
					'+@CWHERE1+' Group By Trj_Type, Party_Name, Amt_Flg, Broker_Name, State_Name, City_Name, Doc_No, Doc_Dt, Due_Dt, Doc_Dt, Amt_Flg ,saletype_name, SalePerson.SalePerson_Name
					'+@CWHERE+' Order By Party_Name, Doc_Dt, Broker_Name, State_Name, City_Name'
				
				print @CCMD
				EXECUTE SP_EXECUTESQL @CCMD   
				
		END

END 

GO---PRASYSTSQL---


IF OBJECT_ID('VIEW_ASP_OUTSTANDING','V') IS NOT NULL
	DROP VIEW VIEW_ASP_OUTSTANDING
GO---PRASYSTSQL---
CREATE VIEW VIEW_ASP_OUTSTANDING AS
SELECT TOP (100) PERCENT   A.DOC_NO, A.DOC_DT, A.TRJ_TYPE, A.DUE_DT, 
E.PARTY_NAME , G.CITY_NAME, E.CONTACT_PERSON, E.MOBILE_NO,H.STATE_NAME, K.SALEPERSON_NAME, 
A.PYT_DAYS AS CR_DAYS, DATEDIFF(DAY, A.DOC_DT, GETDATE()) AS BILL_DAYS, DATEDIFF(DAY, A.DUE_DT, GETDATE()) AS OD_DAYS
, A.DOC_AMT, A.BAL_AMT, F.CO_NAME, B.COBR_NAME, ISNULL(I.BRAND_NAME, '') AS BRAND_NAME,L.SALETYPE_NAME,M.BROKER_NAME,A.FCYR_KEY,a.CoBr_Id
FROM VIEW_OSBILLS A 
LEFT JOIN  COBR B ON A.COBR_ID = B.COBR_ID 
LEFT JOIN  PARTYDTL C ON A.PARTYDTL_ID = C.PARTYDTL_ID 
LEFT JOIN BILL D ON A.BILL_KEY = D.BILL_KEY 
LEFT JOIN  PARTY E ON C.PARTY_KEY = E.PARTY_KEY 
LEFT JOIN COMPANY F ON B.CO_ID = F.CO_ID 
LEFT JOIN CITY G ON C.CITY_KEY = G.CITY_KEY 
LEFT JOIN STATE H ON H.STATE_KEY = G.STATE_KEY 
LEFT JOIN BRAND I ON D.BRAND_KEY = I.BRAND_KEY 
LEFT JOIN CLIENTTERMS J ON E.PARTY_KEY = J.PARTY_KEY 
LEFT JOIN SALEPERSON K ON J.SALEPERSON1_KEY = K.SALEPERSON_KEY
LEFT JOIN SALETYPE L ON L.SALETYPE_ID=A.SALETYPE_ID
LEFT JOIN BROKER  M ON M.BROKER_KEY=A.BROKER_KEY
--ORDER BY E.PARTY_NAME, G.CITY_NAME, K.SALEPERSON_NAME,A.DOC_NO DESC, A.DOC_DT DESC

GO---PRASYSTSQL---