﻿using Common;

namespace Core
{
    public interface ISecurityACCService
    {
       
        bool AllowAccessToCurrentUser(out string strMassage);
        byte Delete(SecurityParam Security);
        System.Collections.Generic.IList<SecurityParam> GetAllSecurities();
       SecurityParam GetSecurityById(long SecurityId);
        string GetSecurityIdByName(string Name);
        byte InsertOrUpdate(SecurityParam Security, modMain.enmHistoryType mode);
        void SyncCheckAndUpdate();
    }
}