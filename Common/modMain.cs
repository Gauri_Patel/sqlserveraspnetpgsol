﻿using System.Data;
using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Hosting;
using System.Text;
using System.Security.Cryptography;
using Microsoft.VisualBasic;
using System.IO;
using System.Web.UI;

namespace Common
{
    public static class modMain
    {

        public static SqlTransaction g_Trans;
        public static SqlConnection g_sqlConn;

      
        public static string g_strMastConnString = "Data Source = " + SessionManager.g_strServerName + ";Initial Catalog = Master;Integrated Security = SSPI";
      
        public static string g_strQuery = "";
       
     
        
        public static string g_strFcYr_REMK { get; set; }

        public static string g_strBranchName { get; set; }
        public static string g_strFcYrName { get; set; }


        public static string g_strpartyfill = "";    //1/S=Salepersopn party    ; 2/A= All party
        public static string g_ImagePath = System.Configuration.ConfigurationManager.AppSettings["ImagePath"]?? "";
        public static string g_ImagePathMultiShade = System.Configuration.ConfigurationManager.AppSettings["MultishadeImagePath"] ?? "";

        public enum enmHistoryType
        {
            Added,
            Edited,
            Deleted
        }

        //  public static Boolean g_paramSMSExpired = false;


        private const string gc_SMSSuccessMsg = "SMS MESSAGE(S) SENT";
        private const string gc_systemUser = "PRASYST";
        private const string gc_systemPassword = "psipl21022006";
        private const string gc_PrasystPassword = "Start@316"; // Gauri
        private const string gc_strExistsMsg = " already exists !";
        private const int gc_shtCannotDeleteNumb = 547;
        private const string gc_strCannotDeleteMsg = "Cannot delete current record as it has related record(s) in other table(s)";
        private const string gc_strAppVersion = "8.1.K";
        private const string gc_strSpecUser = "User1";
        private const string gc_strSpecPwd = "User111";
        public static string g_WSServer = "www.prathamglobal.com";
        public static string g_WSServerIP = "206.183.108.164";
        public static string g_WSDatabase = "PrathamCRM";
        public static string g_WSUser = "pcrm";
        public static string g_WSPassword = "psipl$21022006";
        public static string g_GoogleSite = "www.google.com";
        public static char G_BarcodePrinter;
        public static string g_ParamQCVerify;

        //public static ContainerBuilder  builder { get; set; }

       // public static string g_strServer { get; set; }
        
        
       public static string g_strDatabaseACC { get; set; }
        
        public static string g_strConnStringMaster { get; set; }
        public static SqlConnection g_sqlConnACC { get; set; }
        public static SqlTransaction g_TransACC { get; set; }

        public static string g_strCoBranchID { get; set; } = "";

        public static string Gc_SMSSuccessMsg => gc_SMSSuccessMsg;

        public static string Gc_systemUser => gc_systemUser;

        public static string Gc_systemPassword => gc_systemPassword;

        public static string Gc_PrasystPassword => gc_PrasystPassword;

        public static bool g_blnSystemUser { get; set; }

        public static bool g_blnSuperUser { get; set; }

        public static bool g_blnAllowSpecific { get; set; }

        public static string Gc_strExistsMsg => gc_strExistsMsg;

        public static int Gc_shtCannotDeleteNumb => gc_shtCannotDeleteNumb;

        public static string Gc_strCannotDeleteMsg => Gc_strCannotDeleteMsg1;

        public static string Gc_strCannotDeleteMsg1 => gc_strCannotDeleteMsg;

        public static string Gc_strAppVersion => gc_strAppVersion;

        public static string Gc_strSpecUser => gc_strSpecUser;

        public static string Gc_strSpecPwd => gc_strSpecPwd;
        // public static USER g_User { get; set; }
        public static string g_ParamComm { get; set; }
        public static double g_ParamMRPUp { get; set; }
        public static string g_ParamCity { get; set; }
        public static char g_ParamAllowNegativeStock { get; set; }
        public static string g_ParamFabStkLoc { get; set; }
        public static string g_ParamDisplayMsg { get; set; }
        public static string g_ParamVoiceOn { get; set; }
        public static string g_ParamMarkDownSizeMRP { get; set; }
        public static string g_ParamProcRctStk { get; set; }
        public static string g_ParamDuplicateStyle { get; set; }
        public static string g_ParamShadeAllocation { get; set; }
        public static string g_ParamOrderReference { get; set; }
        public static string g_ParamSizeWiseMRP { get; set; }
        public static string g_ParamPackRateFromMst { get; set; }
        public static string g_ParamDynamicSizePrn { get; set; }
        public static string g_ParamBranchWiseSeries { get; set; }
        public static string g_ParamAllowProductEdit { get; set; }
        public static string g_ParamBranchWiseAcc { get; set; }
        public static string g_ParamOrdNoFiFoPack { get; set; }
        public static string g_ParamFranchAcc { get; set; }
        public static string g_ParamEditMRP { get; set; }
        public static string g_ParamAllowSMS { get; set; }
        public static string g_ParamSMSUser { get; set; }
        public static string g_ParamSMSPassword { get; set; }
        public static char g_ParamUniqueBarCodeGen { get; set; }
        public static string g_ParamExportDataWithoutBill { get; set; }
        public static string g_ParamStockChkOnTransaction { get; set; }
        public static string g_ParamPrintCompany { get; set; }
        public static bool g_paramShowAllPendingBills { get; set; }
        public static string g_ParamAllowRoadPermit { get; set; }
        public static string g_ParamAllowExcessQtyProcRcpt { get; set; }
        public static string g_ParamAutoBackup { get; set; }
        public static string g_ParamWgtInStyle { get; set; }
        public static string g_ParamMaintainSet { get; set; }
        public static string g_ParamJobApprvReqd { get; set; }
        public static string g_ParamSMSBillValidity { get; set; }
        public static string g_ParamBackupAt { get; set; }
        public static int g_ParamBankChargesId { get; set; }
        public static string g_ParamBankChargesNm { get; set; }
        public static double g_ParamExcisePerc { get; set; }
        public static bool g_ParamExciseAppbl { get; set; }
        public static object g_ParamIsAdminUserOne { get; set; }
        public static string g_ParamPayrollModule { get; set; }
        public static bool g_ParamWspEnable { get; set; }
        public static string g_ParamPackCaption { get; set; }
        public static bool g_ParamPartyCatBrandDiscEnable { get; set; }
        public static string g_ParamPrnDetail { get; set; }
        public static string g_paramBrand { get; set; }
        public static bool g_paramBranchWiseDocNo { get; set; }
        public static bool g_ParamProdnPlan { get; set; }
        public static bool g_ParamMRPChgOnBarCdPrn { get; set; }
        public static bool g_paramCompanyWiseProdParty { get; set; }
        public static bool g_ParamPartyApproval { get; set; }
        public static bool g_ParamMultiplyByWeightForQty { get; set; }
        public static string g_ParamMaintainStylePart { get; set; }
        public static bool g_ParamRatioBasedQty { get; set; }
        public static bool g_ParamPaperReport { get; set; }
        public static bool g_ParamLotEnable { get; set; }
        public static bool g_ParamApproval { get; set; }
        public static bool g_ParamStoreItemSizeWise { get; set; }
        public static bool g_ParamCreateWorkOrder { get; set; }
        public static bool g_ParamShadeChgProcess { get; set; }
        public static bool g_ParamIncludeStkInTallyExp { get; set; }
        public static bool g_ParamShowMachineEmployeeInProcessTransfer { get; set; }
        public static bool g_ParamAllowDetailEditInPackingBarcode { get; set; }
        public static bool g_ParamLotWisePaking { get; set; }
        public static bool g_paramTransApproval { get; set; }
        public static bool g_paramWarrantyBarcode { get; set; }
        public static string g_paramLockOnSale { get; set; }
        public static bool g_paramWebSynchronization { get; set; }
        public static bool g_paramWIPAutoCreate_SOParts { get; set; }
        public static bool g_paramGSTEnabled { get; set; }
        public static bool g_paramOrdShowStk { get; set; }
        public static bool g_paramLineDiscEnabled { get; set; }
        public static string g_ParamUS9Rate { get; set; }
        public static bool g_paramDistrubuterModule { get; set; }
        public static bool g_paramSzSetBrcodeStkEnabled { get; set; }
        public static bool g_paramAllowEditBill_Pack { get; set; }
        public static bool g_paramSMSExpired { get; set; } = false;
        public static bool g_paramCoDivisionEnabled { get; set; }
        public static bool g_paramAllowProStkInstk { get; set; }
        public static char g_paramProjSource { get; set; }
        public static bool g_paramAllowBITool { get; set; }
        public static bool g_paramBIToolExtendedRights { get; set; }
        public static bool g_paramInterBrDispAgainstord { get; set; }
        public static bool g_paramEmailonLREntry { get; set; }
        public static bool g_paramValidateCodeLength { get; set; }
        public static bool g_blnPrdSrWiseMRPchng { get; set; }
        public static int g_ParamShiftClosingTime { get; set; }
        public static int g_ParamUniversalMCEfficiency { get; set; }
        public static bool g_paramblnValidateMachUnpack { get; set; }
        public static bool g_ParamLotShadeWise_Ord { get; set; }
        public static bool g_ParamLotprocesstransfer { get; set; }
        public static bool g_ParamPGSIMGDB { get; set; }
        public static DateTime g_Currentserverdate { get; set; }
        public static bool g_AllowBkDtEnt { get; set; }
        public static bool g_AllowSeriesChnage { get; set; }
        public static bool g_AllowRateChnage { get; set; }
        public static DateTime g_BackDate { get; set; }
        public static bool g_ParamCheckLicense { get; set; }
        public static string g_ParamClientId { get; set; }
        public static string g_ProductVariant { get; set; }
        public static bool g_ParamSoftwareStatus { get; set; }
        public static bool g_ParamIsTerminated { get; set; }
        public static Int32 g_ParamAccessToUsers { get; set; }
        public static string g_ParamValidTill { get; set; }
        public static Int32 g_ParamFreePeriod { get; set; }
        public static Int32 g_ParamExtendDays { get; set; }
        public static Int32 g_ParamAfterExtensionWaitDays { get; set; }
        public static int g_ParamAfterExtensionWaitSec { get; set; }
        public static byte g_ParamUpdateInterval { get; set; }
        public static DateTime g_ParamLastUpdatedOn { get; set; }
        public static string g_ParamLastUpdatedOnEncry { get; set; }
        public static int g_ParamMaxWaitSec { get; set; }
        public static string g_ParamMsg1 { get; set; }
        public static string g_ParamMsg2 { get; set; }
        public static string g_ParamMsg3 { get; set; }
        public static string g_ParamMsg4 { get; set; }
        public static int g_SleepSeconds { get; set; }
        public static DateTime g_ParamSyncDueDt { get; set; }
        public static DateTime g_dtDateFrom { get; set; }
        public static DateTime g_dtDateTo { get; set; }
        public static DateTime? g_dtFcDateFrom { get; set; }
        public static DateTime? g_dtFcDateTo { get; set; }
     
       

        public static string g_strbinpath
        {
            get
            {
                if (HostingEnvironment.IsHosted)
                {
                    //hosted
                    return HttpRuntime.BinDirectory;
                }
                else
                {
                    //not hosted. For example, run either in unit tests
                    return AppDomain.CurrentDomain.BaseDirectory;
                }
            }

        }
        public static string setconnectstring(string dbname)
        {
            string server_name;
            server_name = SessionManager.g_strServerName;
            string sqlconn = "server=" + SessionManager.g_strServerName + ";database=" + SessionManager.g_strDatabase + SessionManager.g_strfcyr + ";User ID=" + SessionManager.g_strDBUsername + ";Password=" + SessionManager.g_strDBPassword + "";
            return sqlconn;
        }

        public static string setconnectstringAcc(string dbname)
        {
            string server_name;
            server_name = SessionManager.g_strServerName;
            string sqlconn = "server=" + SessionManager.g_strServerName + ";database=" + SessionManager.g_strDatabaseAcc + SessionManager.g_strfcyr + ";User ID=" + SessionManager.g_strDBUsername + ";Password=" + SessionManager.g_strDBPassword + "";
            return sqlconn;
        }
        public static bool setConn()
        {
            // SET DATABASE CONNECTION.
            try
            {
                SqlConnection myConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                myConn.Open();

                SqlCommand sqComm = new SqlCommand();
                sqComm.Connection = myConn;
            }
            catch (Exception ex) 
            {
                SessionManager.LogError = ex.Message;    
                return false; 
            }
            return true;
        }

        //public void LogError(string errMessage)
        //{
        //    string strErrString = "";

        //    System.IO.StreamWriter OWriter = new System.IO.StreamWriter(System.IO.File.Open(g_strErrLog, System.IO.FileMode.Append));
        //    TextWriterTraceListener OListener = new TextWriterTraceListener(OWriter);
        //    Trace.Listeners.Add(OListener);

        //    strErrString = "Error generated on " + Format(DateTime.Now.Date, "dd/MM/yyyy") + " " + Format(DateTime.Now, "hh:mm:ss") + Strings.Space(5) + errMessage;
        //    Trace.WriteLine(strErrString);
        //    Trace.Listeners.Clear();
        //    OWriter.Close(); OWriter = null;
        //}

        //public void LogAndDisplayException(Exception ExException, string strQuery = "")
        //    {
        //        LogError("Error: " + ExException.StackTrace + Constants.vbCrLf + ExException.Message + Constants.vbCrLf);
        //        Interaction.MsgBox("Error: " + ExException.StackTrace + Constants.vbCrLf + ExException.Message + Constants.vbCrLf + strQuery, MsgBoxStyle.Critical);
        //    }

        //    public void LogAndDisplayError(ErrObject Err)
        //    {
        //        LogError("Error: " + Err.Number + Constants.vbCrLf + Err.Description + Constants.vbCrLf);
        //        Interaction.MsgBox("Error: " + Err.Number + Constants.vbCrLf + Err.Description, MsgBoxStyle.Critical);
        //    }

        public static string ExecuteScalar(string strQuery, SqlTransaction Trans = null)
        {
            if (SessionManager.g_strUserId == "1")
            {
                string FILE_NAME = @"d:\PGSol\errorlog.txt";
                if (System.IO.File.Exists(FILE_NAME) == true)
                {
                    System.IO.StreamWriter objWriter = new System.IO.StreamWriter(FILE_NAME, true);
                    objWriter.Write(DateTime.Now);
                    objWriter.Write(" : ");
                    objWriter.Write(strQuery);
                    objWriter.Write("\n");
                    objWriter.Close();
                }
            }


            SqlCommand cmdCommand;
            string strValue = "";
            if (Trans == null)
                OpenCloseConnection(true);
            cmdCommand = new SqlCommand(strQuery, g_sqlConn, Trans == null ? null : Trans);// IIf(Trans == null, null, Trans));
                                                                                           // strValue = cmdCommand.ExecuteScalar().ToString
            strValue = Convert.ToString(cmdCommand.ExecuteScalar());
            if (Trans == null)
                OpenCloseConnection(false);
            cmdCommand = null;

            return strValue;
        }

        public static string ExecuteScalarACC(string strQuery, SqlTransaction Trans = null)
        {
            if (SessionManager.g_strUserId == "1")
            {
                string FILE_NAME = @"d:\PGSol\errorlog.txt";
                if (System.IO.File.Exists(FILE_NAME) == true)
                {
                    System.IO.StreamWriter objWriter = new System.IO.StreamWriter(FILE_NAME, true);
                    objWriter.Write(DateTime.Now);
                    objWriter.Write(" : ");
                    objWriter.Write(strQuery);
                    objWriter.Write("\n");
                    objWriter.Close();
                }
            }


            SqlCommand cmdCommand;
            string strValue = "";
            if (Trans == null)
                OpenCloseConnectionACC(true);
            cmdCommand = new SqlCommand(strQuery, g_sqlConnACC, Trans == null ? null : Trans);// IIf(Trans == null, null, Trans));
                                                                                           // strValue = cmdCommand.ExecuteScalar().ToString
            strValue = Convert.ToString(cmdCommand.ExecuteScalar());
            if (Trans == null)
                OpenCloseConnectionACC(false);
            cmdCommand = null;

            return strValue;
        }

        public static void OpenCloseConnection(bool blnOpen)
        {
            if ((SessionManager.g_strConnString.Trim()).Length == 0)
                SessionManager.g_strConnString = "server=" + SessionManager.g_strServerName + ";userid=" + SessionManager.g_strDBUsername + ";password=" + SessionManager.g_strDBPassword + ";database='" + SessionManager.g_strDatabase + "'";

            if (blnOpen)
            {
                g_sqlConn = new SqlConnection(SessionManager.g_strConnString);
                g_sqlConn.Open();
            }
            else
                g_sqlConn.Close();
        }
        public static void OpenCloseConnectionACC(bool blnOpen)
        {
            if ((SessionManager.g_strConnStringACC.Trim()).Length == 0)
                SessionManager.g_strConnStringACC = "server=" + SessionManager.g_strServerName + ";userid=" + SessionManager.g_strDBUsername + ";password=" + SessionManager.g_strDBPassword + ";database='" + g_strDatabaseACC + "'";

            if (blnOpen)
            {
                g_sqlConnACC = new SqlConnection(SessionManager.g_strConnStringACC);
                g_sqlConnACC.Open();
            }
            else
            {
                g_sqlConnACC.Close();
            }
        }

        public static int GetNextDtlID(string strTableName, string strFieldName, SqlTransaction Trans = null)
        {
            DataTable dtmax = new DataTable();

            g_strQuery = "SELECT MAX(" + strFieldName + ") MAXID FROM " + strTableName;
            dtmax = NewDatatable(g_strQuery, SessionManager.g_strConnString, (Trans == null ? null : Trans));
            if (dtmax.Rows[0]["MAXID"] == null)
                return 1;
            else
                return Convert.ToInt32(dtmax.Rows[0]["MAXID"]) + 1;
            //  dtmax.Dispose();
        }

        public static void ExecuteQuery(string strQuery, SqlTransaction Trans = null)
        {
            SqlCommand cmdCommand;

            if (SessionManager.g_strUserId == "1")
            {
                string FILE_NAME = @"d:\PGSol\errorlog.txt";
                if (System.IO.File.Exists(FILE_NAME) == true)
                {
                    System.IO.StreamWriter objWriter = new System.IO.StreamWriter(FILE_NAME, true);
                    objWriter.Write(DateTime.Now);
                    objWriter.Write(" : ");
                    objWriter.Write(strQuery);
                    objWriter.Write("\n");
                    objWriter.Close();
                }
            }

            if ((Trans == null)) //if ((  IsNothing(Trans)))
            {
                OpenCloseConnection(true);
                cmdCommand = new SqlCommand(strQuery, g_sqlConn);
                cmdCommand.CommandTimeout = 500;
                cmdCommand.ExecuteNonQuery();
                OpenCloseConnection(false);
            }
            else
            {
                cmdCommand = new SqlCommand(strQuery, g_sqlConn);
                cmdCommand.Transaction = Trans;
                cmdCommand.CommandTimeout = 500;
                cmdCommand.ExecuteNonQuery();
            }
            cmdCommand = null;
        }

        public static void ExecuteQueryACC(string strQuery, SqlTransaction Trans = null)
        {
            SqlCommand cmdCommand;
            Console.WriteLine(strQuery);
            if (SessionManager.g_strUserId =="1")
            {
                string FILE_NAME = @"d:\PGSol\errorlog.txt";
                if (File.Exists(FILE_NAME) == true)
                {
                    var objWriter = new StreamWriter(FILE_NAME, true);
                    objWriter.Write(DateAndTime.Now);
                    objWriter.Write(" : ");
                    objWriter.Write(strQuery);
                    objWriter.Write("\n");
                    objWriter.Close();
                }
            }

            strQuery = Convert.ToString((strQuery.ToUpper().Contains("CREATE") ? "" : "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED") + "\n" + strQuery);
            Console.WriteLine(strQuery);
            if (Information.IsNothing(Trans))
            {
                OpenCloseConnectionACC(true);
                cmdCommand = new SqlCommand(strQuery, g_sqlConnACC);
                cmdCommand.CommandTimeout = 500000;
                cmdCommand.ExecuteNonQuery();
                OpenCloseConnectionACC(false);
            }
            else
            {
                cmdCommand = new SqlCommand(strQuery, g_sqlConnACC);
                cmdCommand.Transaction = Trans;
                cmdCommand.CommandTimeout = 500000;
                cmdCommand.ExecuteNonQuery();
            }

            cmdCommand = null;
        }
        // 'function which allows to create a new datatable as per string 
        // 'which is passed as a parameter
        public static DataTable NewDatatable(string strQuery, string strConn, SqlTransaction Trans = null)
        {

            DataTable dtTable = new DataTable();
            SqlDataAdapter dbAdapt;

            if ((strConn == null))// || SessionManager.g_strConnString.Trim().Length == 0)
                strConn = "server=" + SessionManager.g_strServerName + ";userid=" + SessionManager.g_strDBUsername + ";password=" + SessionManager.g_strDBPassword + ";database='" + SessionManager.g_strDatabase + "'";
            if (SessionManager.g_strUserId == "1")
            {
                string FILE_NAME = @"d:\PGSol\errorlog.txt";
                if (System.IO.File.Exists(FILE_NAME) == true)
                {
                    System.IO.StreamWriter objWriter = new System.IO.StreamWriter(FILE_NAME, true);
                    objWriter.Write(DateTime.Now);
                    objWriter.Write(" : ");
                    objWriter.Write(strQuery);
                    objWriter.Write("\n");
                    objWriter.Close();
                }
            }

            dbAdapt = new SqlDataAdapter(strQuery, strConn);
            dbAdapt.SelectCommand.CommandTimeout = 1000;
            if ((Trans != null)) //if (!IsNothing(Trans))
            {
                dbAdapt.SelectCommand.Connection = g_sqlConn;
                dbAdapt.SelectCommand.Transaction = Trans;
            }

            dbAdapt.Fill(dtTable);
            return dtTable;
        }
        public static DataTable NewDatatableFromMasterDB(string strQuery)
        {
            DataTable dtTable = new DataTable();
            SqlDataAdapter dbAdapt;


            string strMasterSqlConnString = "server=" + SessionManager.g_strServerName + ";userid=" + SessionManager.g_strDBUsername + ";password=" + SessionManager.g_strDBPassword + ";database='prasycro_marketplus_masterdb'";

            if (SessionManager.g_strUserId =="1")
            {
                string FILE_NAME = @"d:\PGSol\errorlog.txt";
                if (System.IO.File.Exists(FILE_NAME) == true)
                {
                    System.IO.StreamWriter objWriter = new System.IO.StreamWriter(FILE_NAME, true);
                    objWriter.Write(DateTime.Now);
                    objWriter.Write(" : ");
                    objWriter.Write(strQuery);
                    objWriter.Write("\n");
                    objWriter.Close();
                }
            }

            dbAdapt = new SqlDataAdapter(strQuery, strMasterSqlConnString);

            dbAdapt.SelectCommand.CommandTimeout = 1000;
            dbAdapt.Fill(dtTable);
            return dtTable;
        }

        public static DataSet NewDataset(string strQuery, string strConn, SqlTransaction Trans = null)
        {
            if ((strConn == null))// || SessionManager.g_strConnString.Trim().Length == 0)
                strConn = "server=" + SessionManager.g_strServerName + ";userid=" + SessionManager.g_strDBUsername + ";password=" + SessionManager.g_strDBPassword + ";database='" + SessionManager.g_strDatabase + "'";
            DataSet ds = new DataSet();
            SqlDataAdapter dbAdapt;

            if (SessionManager.g_strUserId == "1")
            {
                string FILE_NAME = @"d:\PGSol\errorlog.txt";
                if (System.IO.File.Exists(FILE_NAME) == true)
                {
                    System.IO.StreamWriter objWriter = new System.IO.StreamWriter(FILE_NAME, true);
                    objWriter.Write(DateTime.Now);
                    objWriter.Write(" : ");
                    objWriter.Write(strQuery);
                    objWriter.Write("\n");
                    objWriter.Close();
                }
            }

            dbAdapt = new SqlDataAdapter(strQuery, strConn);
            dbAdapt.SelectCommand.CommandTimeout = 1000;

            if ((Trans != null)) //if (!IsNothing(Trans))
            {
                dbAdapt.SelectCommand.Connection = g_sqlConn;
                dbAdapt.SelectCommand.Transaction = Trans;
            }
            dbAdapt.Fill(ds);

            return ds;
        }
        //public static void valid_textbox(TextBox v_control, string v_id_name, string function_type)
        //{
        //    if (function_type == "D")
        //        v_control.Attributes.Add("onkeyup", "return onKeyPressBlockNumbers(event,'" + v_id_name + "');");
        //    else if (function_type == "N")
        //        v_control.Attributes.Add("onkeypress", "return onKeyPressonlynumber(event,'" + v_id_name + "');");
        //    else if (function_type == "DB")
        //        v_control.Attributes.Add("onkeypress", "return onKeyPressdateofbirth(event,'" + v_id_name + "');");
        //}

        //public static void setPageheading(Page pagename, string page_caption, int inttype)
        //{
        //    Label heading = (Label)pagename.Master.FindControl("heading");
        //    if (heading == null)
        //        heading = (Label)pagename.Master.Master.FindControl("heading");

        //    heading.Visible = true;
        //    heading.Text = "   " + page_caption + " ";

        //    ImageButton btnback = (ImageButton)pagename.Master.FindControl("btnback");
        //    btnback.Visible = false;
        //    if (inttype == 1)
        //    {
        //        btnback.Visible = true;
        //    }

        //}


        public static void gridviewbind(String strquery, string ConnectionString, GridView vgrid)
        {

            DataSet ds = NewDataset(strquery, ConnectionString);


            if (ds.Tables[0].Rows.Count == 0)
            {
                // Add a blank row to the dataset
                //ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                // Bind the DataSet to the GridView
                vgrid.DataSource = ds;
                vgrid.DataBind();
                // Get the number of columns to know what the Column Span should be
                //int columnCount = vgrid.Rows[0].Cells.Count;
                // Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
               // vgrid.Rows[0].Cells.Clear();
               // vgrid.Rows[0].Cells.Add(new TableCell());
               // vgrid.Rows[0].Cells[0].ColumnSpan = columnCount;
               // vgrid.Rows[0].Cells[0].Text = "No Items in List.";
               // vgrid.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
               // vgrid.Rows[0].Cells[0].Font.Bold = true;
               // vgrid.Rows[0].Cells[0].Font.Size = 8;
            }
            else
            {
                vgrid.DataSource = ds;
                vgrid.DataBind();
            }

        }
        //public static void set_blank_gridview(DataSet ds, GridView vgrid)
        //{
        //    // On Error Resume Next
        //    if (ds.Tables[0].Rows.Count == 0)
        //    {
        //        // Add a blank row to the dataset
        //        ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
        //        // Bind the DataSet to the GridView
        //        vgrid.DataSource = ds;
        //        vgrid.DataBind();
        //        // Get the number of columns to know what the Column Span should be
        //        int columnCount = vgrid.Rows[0].Cells.Count;
        //        // Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
        //        vgrid.Rows[0].Cells.Clear();
        //        vgrid.Rows[0].Cells.Add(new TableCell());
        //        vgrid.Rows[0].Cells[0].ColumnSpan = columnCount;
        //        vgrid.Rows[0].Cells[0].Text = "No Items in List.";
        //        vgrid.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
        //        vgrid.Rows[0].Cells[0].Font.Bold = true;
        //        vgrid.Rows[0].Cells[0].Font.Size = 8;
        //    }
        //    else
        //    {
        //        vgrid.DataSource = ds;
        //        vgrid.DataBind();
        //    }
        //}


        public static void FillInCombo(string strQuery, DropDownList drpdropDown, string strDisplayMember, string strValueMember, string strConn = "")
        {
            DataTable dt;
            if ((SessionManager.g_strConnString == null))
                SessionManager.g_strConnString = "server=" + SessionManager.g_strServerName + ";userid=" + SessionManager.g_strDBUsername + ";password=" + SessionManager.g_strDBPassword + ";database='" + SessionManager.g_strDatabase + "'";

            if (strConn.Trim().Length == 0)
                dt = NewDatatable(strQuery, SessionManager.g_strConnString, null);
            else
                dt = NewDatatable(strQuery, strConn, null);
            Console.WriteLine(strQuery);


            drpdropDown.DataSource = dt;
            drpdropDown.DataBind();
            drpdropDown.DataTextField = strDisplayMember;
            drpdropDown.DataValueField = strValueMember;
            drpdropDown.DataBind();

            dt = null;
        }
        public static void FillInComboALL(DataTable dt, DropDownList drpdropDown, string strDisplayMember, string strValueMember, string strConn = "")
        {

            drpdropDown.DataSource = dt;
            drpdropDown.DataBind();
            drpdropDown.DataTextField = strDisplayMember;
            drpdropDown.DataValueField = strValueMember;
            drpdropDown.DataBind();

            dt = null;
        }
        public static DataTable GetDataTableWithAll(string strQuery, string connstr)
        {
            DataTable dtRetTable = new DataTable();
            DataTable dtTemp = new DataTable();
            DataRow drRow;
            int i;

            dtTemp = NewDatatable(strQuery, connstr);

            dtRetTable = dtTemp.Clone();
            drRow = dtRetTable.NewRow();
            drRow[0] = "9999"; drRow[1] = "( ALL )";
            dtRetTable.Rows.Add(drRow);

            for (i = 0; i <= dtTemp.Rows.Count - 1; i++)
            {
                drRow = dtRetTable.NewRow();
                drRow[0] = dtTemp.Rows[i][0];
                drRow[1] = dtTemp.Rows[i][1];

                dtRetTable.Rows.Add(drRow);
            }
            dtTemp = null;

            return dtRetTable;
        }

        public static void CreateAndInsertDataIntoTable(DataTable dt, string strtable_name, String strordbk, SqlTransaction Trans = null)
        {
            SessionManager.g_strQuery = "IF OBJECT_ID('" + strtable_name + "','U') IS NOT NULL BEGIN DROP TABLE " + strtable_name + " END";
            modMain.ExecuteQuery(SessionManager.g_strQuery, Trans);

            string strcolumns = "";
            foreach (DataColumn item in dt.Columns)
            {
                strcolumns += (strcolumns.Length > 0 ? " , " : "") + item.ColumnName + " ";

                if (item.DataType == typeof(DateTime) || item.ColumnName.ToUpper() == "DLV_DT")
                {
                    strcolumns += " DateTime ";
                }
                else if (item.DataType == typeof(decimal))
                {
                    strcolumns += " Numeric(15,3) ";

                }
                else if (item.ColumnName.ToUpper() == "QTY" || item.ColumnName.ToUpper() == "MRP" || item.ColumnName.ToUpper() == "PENDING_ORD_QTY")
                {
                    strcolumns += " Numeric(15,3) ";

                }
                else if (item.DataType == typeof(int))
                {
                    strcolumns += " bigint ";
                }
             
                else if (item.DataType == typeof(DateTime))
                {
                    strcolumns += " DateTime ";

                }
                else
                {
                    strcolumns += " varchar(MAX) ";

                }
            }
            SessionManager.g_strQuery = " CREATE TABLE " + strtable_name + " (" + strcolumns + ")";
            modMain.ExecuteQuery(SessionManager.g_strQuery, Trans);

            if (strtable_name == "ORDBKASPDTL")
            {
                SessionManager.g_strQuery = " IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_NAME='ORDBKASPDTL' AND COLUMN_NAME='ORDBK_KEY') "
                                    + " BEGIN "
                                    + " ALTER TABLE ORDBKASPDTL ADD ORDBK_KEY VARCHAR(20) NOT NULL DEFAULT '' "
                                    + " END";
                modMain.ExecuteQuery(SessionManager.g_strQuery, Trans);

                SessionManager.g_strQuery = " IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_NAME='ORDBKASPDTL' AND COLUMN_NAME='USER_ID') "
                                     + " BEGIN "
                                     + " ALTER TABLE ORDBKASPDTL ADD USER_ID BIGINT NOT NULL DEFAULT 0 "
                                     + " END";
                modMain.ExecuteQuery(SessionManager.g_strQuery, Trans);

            }

            SqlBulkCopy objbulk = new SqlBulkCopy(SessionManager.g_strConnString);
            if (Trans != null)
            {
                 objbulk = new SqlBulkCopy(Trans.Connection, SqlBulkCopyOptions.Default, Trans);
            }
          


            objbulk.DestinationTableName = strtable_name;
            foreach (DataColumn item in dt.Columns)
            {
                objbulk.ColumnMappings.Add(item.ColumnName, item.ColumnName);
            }

           

            objbulk.WriteToServer(dt);
        
        }

        public static string EncryptValue(string plainText, string passPhrase)
        {
            if (plainText.Trim().Length == 0)
                return "";

            // 'Dim plainText As String = "Hello, World!"    ' original plaintext
            // Dim cipherText As String = ""                 ' encrypted text
            // Dim passPhrase As String = "Pas5pr@se"        ' can be any string
            // Dim initVector As String = "@1B2c3D4e5F6g7H8" ' must be 16 bytes

            // Dim rijndaelKey As RijndaelEnhanced = New RijndaelEnhanced(passPhrase, initVector)

            // Return rijndaelKey.Encrypt(plainText)

            byte[] Results;
            var UTF8 = new UTF8Encoding();
            var HashProvider = new MD5CryptoServiceProvider();
            var TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(passPhrase));
            var TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            var DataToEncrypt = UTF8.GetBytes(plainText);
            try
            {
                var Encryptor = TDESAlgorithm.CreateEncryptor();
                Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }

            return Convert.ToBase64String(Results);
        }

        public static string DecryptValue(string plainText, string passPhrase)
        {
            if (plainText.Trim().Length == 0)
                return "";

            // Dim passPhrase As String = "Pas5pr@se"        ' can be any string
            // Dim initVector As String = "@1B2c3D4e5F6g7H8" ' must be 16 bytes

            // Dim rijndaelKey As RijndaelEnhanced = New RijndaelEnhanced(passPhrase, initVector)

            // Return rijndaelKey.Decrypt(plainText)


            byte[] Results;
            var UTF8 = new UTF8Encoding();
            var HashProvider = new MD5CryptoServiceProvider();
            var TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(passPhrase));
            var TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            var DataToDecrypt = Convert.FromBase64String(plainText);
            try
            {
                var Decryptor = TDESAlgorithm.CreateDecryptor();
                Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }

            return UTF8.GetString(Results);
        }
        public static string EncryptDecryptValue(string strPassword, string strEncDec)
        {
            if (strPassword.Trim().Length == 0)
                return "";
            string strCurrChar;
            var intPos = default(byte);
            string strNewValue;
            byte i;
            strNewValue = "";
            strCurrChar = "";
            var loopTo = strPassword.Length - 1;
            for (i = 0; i <= loopTo; i++)
            {
                intPos += 1;
                strCurrChar = Strings.Mid(strPassword, intPos, 1);
                if ((strEncDec ?? "") == "1")
                {
                    if (Strings.Asc(strCurrChar) - Strings.Len(strPassword) == 39)
                    {
                        strNewValue += "''";
                    }
                    else
                    {
                        strNewValue += Convert.ToString((char)(Strings.Asc(strCurrChar) - Strings.Len(strPassword)));
                    }
                }
                else
                {
                    strNewValue += Convert.ToString((char)(Strings.Asc(strCurrChar) + Strings.Len(strPassword)));
                }
            }

            return strNewValue;
        }

        public static string GetWindowsUserName()
        {
            System.Security.Principal.WindowsIdentity win;
            win = System.Security.Principal.WindowsIdentity.GetCurrent();
            string _UserName = win.Name.Substring(win.Name.IndexOf(@"\") + 1);
            return _UserName;
        }

        public static string ReplaceStringChar(string strString)
        {
            string strTemp = "";
            foreach (char item in strString.ToCharArray())
            {
                if (!Char.IsLetterOrDigit(item))
                {
                    strTemp = Strings.Replace(strString, Convert.ToString(item), Convert.ToString(item) + Convert.ToString(item));

                }
            }
            return strTemp;
        }
       public static void ShowMessage(string msg, Page p)
        {
            p.ClientScript.RegisterStartupScript(p.GetType(), "validation", "<script language='javascript'>alert('" + msg.Replace("'","").Replace(Environment.NewLine, " ") + "');</script>");
        }
    }
}